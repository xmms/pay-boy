package com.ndood.notify.base.pojo.record;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
@TableName("r_risk_rec_channel_daily")
public class RRiskRecChannelDailyPo extends Model<RRiskRecChannelDailyPo> {

    private static final long serialVersionUID = 1L;

    private String recordNo;

    /**
     * 代理商ID
     */
    private Integer agentId;

    /**
     * 商户ID
     */
    private Integer mchId;

    /**
     * 门店openid
     */
    private Integer shopId;

    /**
     * 日期代码 20181112
     */
    private String day;

    /**
     * 本月总交易额
     */
    private BigDecimal totalAmount;

    /**
     * 本月交易次数
     */
    private Integer totalCount;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


    public String getRecordNo() {
        return recordNo;
    }

    public void setRecordNo(String recordNo) {
        this.recordNo = recordNo;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public Integer getMchId() {
        return mchId;
    }

    public void setMchId(Integer mchId) {
        this.mchId = mchId;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.recordNo;
    }

    @Override
    public String toString() {
        return "RRiskRecChannelDailyPo{" +
        ", recordNo=" + recordNo +
        ", agentId=" + agentId +
        ", mchId=" + mchId +
        ", shopId=" + shopId +
        ", day=" + day +
        ", totalAmount=" + totalAmount +
        ", totalCount=" + totalCount +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
