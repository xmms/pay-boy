package com.ndood.notify.base.pojo.record;

import com.baomidou.mybatisplus.annotation.TableName;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
@TableName("r_mch_rec_check")
public class RMchRecCheckPo extends Model<RMchRecCheckPo> {

    private static final long serialVersionUID = 1L;

    private String recordNo;

    /**
     * 代理商ID
     */
    private Integer agentId;

    /**
     * 商户ID
     */
    private Integer mchId;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


    public String getRecordNo() {
        return recordNo;
    }

    public void setRecordNo(String recordNo) {
        this.recordNo = recordNo;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public Integer getMchId() {
        return mchId;
    }

    public void setMchId(Integer mchId) {
        this.mchId = mchId;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.recordNo;
    }

    @Override
    public String toString() {
        return "RMchRecCheckPo{" +
        ", recordNo=" + recordNo +
        ", agentId=" + agentId +
        ", mchId=" + mchId +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
