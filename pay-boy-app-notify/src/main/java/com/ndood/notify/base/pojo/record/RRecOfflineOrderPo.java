package com.ndood.notify.base.pojo.record;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
@TableName("r_rec_offline_order")
public class RRecOfflineOrderPo extends Model<RRecOfflineOrderPo> {

    private static final long serialVersionUID = 1L;

    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 外部订单号
     */
    private String outOrderNo;

    /**
     * 代理商ID
     */
    private Integer agentId;

    /**
     * 代理商名称
     */
    private String agentName;

    /**
     * 商户ID
     */
    private Integer mchId;

    /**
     * 商户名称
     */
    private String mchName;

    /**
     * 门店开放ID
     */
    private Integer shopId;

    /**
     * 门店全称 店名-分店名
     */
    private String shopName;

    /**
     * 客户端IP
     */
    private String clientIp;

    /**
     * 设备编号
     */
    private String deviceNo;

    /**
     * 商品标题
     */
    private String subject;

    /**
     * 商品描述信息
     */
    private String body;

    /**
     * 特定渠道发起额外参数
     */
    private String extra;

    /**
     * 失效时间，单位分钟
     */
    private String expireTime;

    /**
     * 支付收银员ID
     */
    private Integer payCashierId;

    /**
     * 支付收银员姓名
     */
    private String payCashierName;

    /**
     * 支付备注
     */
    private String payRemark;

    /**
     * 退款人ID
     */
    private Integer refundCashierId;

    /**
     * 退款人姓名
     */
    private String refundCashierName;

    /**
     * 退款备注
     */
    private String refundRemark;

    private String lastPayTime;

    /**
     * 订单总金额 单位 元
     */
    private BigDecimal totalAmount;

    /**
     * 总实收金额
     */
    private BigDecimal receiptAmount;

    /**
     * 卖家优惠金额
     */
    private BigDecimal freeAmount;

    /**
     * 总退款金额 单位 元
     */
    private BigDecimal refundAmount;

    /**
     * 总实退金额 单位 元
     */
    private BigDecimal receiptRefundAmount;

    /**
     * 总剩余可退金额 单位 元
     */
    private BigDecimal canRefundAmount;

    /**
     * 退款时间
     */
    private String lastRefundTime;
    
    /**
     * 状态： 1 已创建 2 成功 3 失败 4 撤销 5 退款 6 部分退款
     */
    private Integer status;

    /**
     * 扩展参数1
     */
    private String param1;

    /**
     * 扩展参数2
     */
    private String param2;

    /**
     * 扩展参数3
     */
    private String param3;

    /**
     * 扩展参数4
     */
    private String param4;

    private Date createTime;

    private Date updateTime;

	public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOutOrderNo() {
        return outOrderNo;
    }

    public void setOutOrderNo(String outOrderNo) {
        this.outOrderNo = outOrderNo;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public Integer getMchId() {
        return mchId;
    }

    public void setMchId(Integer mchId) {
        this.mchId = mchId;
    }

    public String getMchName() {
        return mchName;
    }

    public void setMchName(String mchName) {
        this.mchName = mchName;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    public String getDeviceNo() {
        return deviceNo;
    }

    public void setDeviceNo(String deviceNo) {
        this.deviceNo = deviceNo;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public String getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(String expireTime) {
        this.expireTime = expireTime;
    }

    public Integer getPayCashierId() {
        return payCashierId;
    }

    public void setPayCashierId(Integer payCashierId) {
        this.payCashierId = payCashierId;
    }

    public String getPayCashierName() {
        return payCashierName;
    }

    public void setPayCashierName(String payCashierName) {
        this.payCashierName = payCashierName;
    }

    public String getPayRemark() {
        return payRemark;
    }

    public void setPayRemark(String payRemark) {
        this.payRemark = payRemark;
    }

    public Integer getRefundCashierId() {
        return refundCashierId;
    }

    public void setRefundCashierId(Integer refundCashierId) {
        this.refundCashierId = refundCashierId;
    }

    public String getRefundCashierName() {
        return refundCashierName;
    }

    public void setRefundCashierName(String refundCashierName) {
        this.refundCashierName = refundCashierName;
    }

    public String getRefundRemark() {
        return refundRemark;
    }

    public void setRefundRemark(String refundRemark) {
        this.refundRemark = refundRemark;
    }

    public String getLastPayTime() {
        return lastPayTime;
    }

    public void setLastPayTime(String lastPayTime) {
        this.lastPayTime = lastPayTime;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getReceiptAmount() {
        return receiptAmount;
    }

    public void setReceiptAmount(BigDecimal receiptAmount) {
        this.receiptAmount = receiptAmount;
    }

    public BigDecimal getFreeAmount() {
        return freeAmount;
    }

    public void setFreeAmount(BigDecimal freeAmount) {
        this.freeAmount = freeAmount;
    }

    public BigDecimal getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(BigDecimal refundAmount) {
        this.refundAmount = refundAmount;
    }

    public BigDecimal getReceiptRefundAmount() {
        return receiptRefundAmount;
    }

    public void setReceiptRefundAmount(BigDecimal receiptRefundAmount) {
        this.receiptRefundAmount = receiptRefundAmount;
    }

    public BigDecimal getCanRefundAmount() {
        return canRefundAmount;
    }

    public void setCanRefundAmount(BigDecimal canRefundAmount) {
        this.canRefundAmount = canRefundAmount;
    }

    public String getLastRefundTime() {
        return lastRefundTime;
    }

    public void setLastRefundTime(String lastRefundTime) {
        this.lastRefundTime = lastRefundTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getParam1() {
        return param1;
    }

    public void setParam1(String param1) {
        this.param1 = param1;
    }

    public String getParam2() {
        return param2;
    }

    public void setParam2(String param2) {
        this.param2 = param2;
    }

    public String getParam3() {
        return param3;
    }

    public void setParam3(String param3) {
        this.param3 = param3;
    }

    public String getParam4() {
        return param4;
    }

    public void setParam4(String param4) {
        this.param4 = param4;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.orderNo;
    }

    @Override
    public String toString() {
        return "RRecOfflineOrderPo{" +
        ", orderNo=" + orderNo +
        ", outOrderNo=" + outOrderNo +
        ", agentId=" + agentId +
        ", agentName=" + agentName +
        ", mchId=" + mchId +
        ", mchName=" + mchName +
        ", shopId=" + shopId +
        ", shopName=" + shopName +
        ", clientIp=" + clientIp +
        ", deviceNo=" + deviceNo +
        ", subject=" + subject +
        ", body=" + body +
        ", extra=" + extra +
        ", expireTime=" + expireTime +
        ", payCashierId=" + payCashierId +
        ", payCashierName=" + payCashierName +
        ", payRemark=" + payRemark +
        ", refundCashierId=" + refundCashierId +
        ", refundCashierName=" + refundCashierName +
        ", refundRemark=" + refundRemark +
        ", lastPayTime=" + lastPayTime +
        ", totalAmount=" + totalAmount +
        ", receiptAmount=" + receiptAmount +
        ", freeAmount=" + freeAmount +
        ", refundAmount=" + refundAmount +
        ", receiptRefundAmount=" + receiptRefundAmount +
        ", canRefundAmount=" + canRefundAmount +
        ", lastRefundTime=" + lastRefundTime +
        ", status=" + status +
        ", param1=" + param1 +
        ", param2=" + param2 +
        ", param3=" + param3 +
        ", param4=" + param4 +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
