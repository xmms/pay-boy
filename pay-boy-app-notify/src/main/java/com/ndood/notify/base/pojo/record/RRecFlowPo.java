package com.ndood.notify.base.pojo.record;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
@TableName("r_rec_flow")
public class RRecFlowPo extends Model<RRecFlowPo> {

    private static final long serialVersionUID = 1L;
    /**
     * 交易流水号
     */
    private String flowNo;
    /**
     * 交易流水号
     */
    private String tradeNo;

    /**
     * 交易类型：1 支付 2 退款
     */
    private Integer tradeType;

    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 代理商ID
     */
    private Integer agentId;

    /**
     * 代理商名称
     */
    private String agentName;

    /**
     * 商户ID
     */
    private Integer mchId;

    /**
     * 商户名称
     */
    private String mchName;

    /**
     * 门店开放ID
     */
    private Integer shopId;

    /**
     * 门店全称 店名-分店名
     */
    private String shopName;

    /**
     * 支付方式： 1 支付宝 2 微信
     */
    private Integer payWay;

    /**
     * 支付类型：1 扫码 2 被扫 3门店码
     */
    private Integer payType;

    /**
     * 合同ID
     */
    private Integer contractId;

    /**
     * 合同类型： 1直清 2 二清 3 子商户
     */
    private Integer contractType;

    /**
     * 合同编码
     */
    private String contractNo;

    /**
     * 合同名称
     */
    private String contractName;

    /**
     * 产品编码
     */
    private String productNo;

    /**
     * 产品名称
     */
    private String productName;

    /**
     * 渠道ID
     */
    private Integer channelId;

    /**
     * 渠道编码
     */
    private String channelNo;

    /**
     * 渠道名称
     */
    private String channelName;

    /**
     * 实际交易金额
     */
    private BigDecimal tradeAmount;

    /**
     * 交易时间
     */
    private String tradeTime;

    /**
     * 结算类型：1 费率 2 固定金额
     */
    private Integer settleType;

    /**
     * 商户风险预存期
     */
    private Integer settleRiskDay;

    /**
     * 最早应结算日
     */
    private Date settleDate;

    /**
     * 签约费率
     */
    private BigDecimal settleRate;

    /**
     * 代理商渠道费率
     */
    private BigDecimal settleAgentRate;

    /**
     * 代理商渠道单笔结算金额
     */
    private BigDecimal settleAgentFee;

    /**
     * 平台费率
     */
    private BigDecimal settlePlatformRate;

    /**
     * 平台费用
     */
    private BigDecimal settlePlatformFee;
    
    /**
     * 运营商分润收益 单位 元
     */
    private BigDecimal settlePlatformProfit;

    /**
     * 代理商分润收益 单位 元
     */
    private BigDecimal settleAgentProfit;

    /**
     * 商户收益
     */
    private BigDecimal settleMchProfit;

    /**
     * 结算标记：0 未结算 1 已结算
     */
    private Integer settleStatus;

    private Date createTime;

    private Date updateTime;

    public BigDecimal getSettlePlatformRate() {
		return settlePlatformRate;
	}

	public void setSettlePlatformRate(BigDecimal settlePlatformRate) {
		this.settlePlatformRate = settlePlatformRate;
	}

	public BigDecimal getSettlePlatformFee() {
		return settlePlatformFee;
	}

	public void setSettlePlatformFee(BigDecimal settlePlatformFee) {
		this.settlePlatformFee = settlePlatformFee;
	}

	public String getFlowNo() {
		return flowNo;
	}

	public void setFlowNo(String flowNo) {
		this.flowNo = flowNo;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}

	public Integer getChannelId() {
		return channelId;
	}

	public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;
    }

    public Integer getTradeType() {
        return tradeType;
    }

    public void setTradeType(Integer tradeType) {
        this.tradeType = tradeType;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public Integer getMchId() {
        return mchId;
    }

    public void setMchId(Integer mchId) {
        this.mchId = mchId;
    }

    public String getMchName() {
        return mchName;
    }

    public void setMchName(String mchName) {
        this.mchName = mchName;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Integer getPayWay() {
        return payWay;
    }

    public void setPayWay(Integer payWay) {
        this.payWay = payWay;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public Integer getContractType() {
        return contractType;
    }

    public void setContractType(Integer contractType) {
        this.contractType = contractType;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getContractName() {
        return contractName;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName;
    }

    public String getProductNo() {
        return productNo;
    }

    public void setProductNo(String productNo) {
        this.productNo = productNo;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getChannelNo() {
        return channelNo;
    }

    public void setChannelNo(String channelNo) {
        this.channelNo = channelNo;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public BigDecimal getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(BigDecimal tradeAmount) {
        this.tradeAmount = tradeAmount;
    }

    public String getTradeTime() {
        return tradeTime;
    }

    public void setTradeTime(String tradeTime) {
        this.tradeTime = tradeTime;
    }

    public Integer getSettleType() {
        return settleType;
    }

    public void setSettleType(Integer settleType) {
        this.settleType = settleType;
    }

    public Integer getSettleRiskDay() {
        return settleRiskDay;
    }

    public void setSettleRiskDay(Integer settleRiskDay) {
        this.settleRiskDay = settleRiskDay;
    }

    public Date getSettleDate() {
        return settleDate;
    }

    public void setSettleDate(Date settleDate) {
        this.settleDate = settleDate;
    }

    public BigDecimal getSettleRate() {
        return settleRate;
    }

    public void setSettleRate(BigDecimal settleRate) {
        this.settleRate = settleRate;
    }

    public BigDecimal getSettleAgentRate() {
        return settleAgentRate;
    }

    public void setSettleAgentRate(BigDecimal settleAgentRate) {
        this.settleAgentRate = settleAgentRate;
    }

    public BigDecimal getSettleAgentFee() {
        return settleAgentFee;
    }

    public void setSettleAgentFee(BigDecimal settleAgentFee) {
        this.settleAgentFee = settleAgentFee;
    }

    public BigDecimal getSettlePlatformProfit() {
		return settlePlatformProfit;
	}

	public void setSettlePlatformProfit(BigDecimal settlePlatformProfit) {
		this.settlePlatformProfit = settlePlatformProfit;
	}

	public BigDecimal getSettleAgentProfit() {
        return settleAgentProfit;
    }

    public void setSettleAgentProfit(BigDecimal settleAgentProfit) {
        this.settleAgentProfit = settleAgentProfit;
    }

    public BigDecimal getSettleMchProfit() {
        return settleMchProfit;
    }

    public void setSettleMchProfit(BigDecimal settleMchProfit) {
        this.settleMchProfit = settleMchProfit;
    }

    public Integer getSettleStatus() {
        return settleStatus;
    }

    public void setSettleStatus(Integer settleStatus) {
        this.settleStatus = settleStatus;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.tradeNo;
    }

    @Override
    public String toString() {
        return "RRecFlowPo{" +
        ", tradeNo=" + tradeNo +
        ", tradeType=" + tradeType +
        ", orderNo=" + orderNo +
        ", agentId=" + agentId +
        ", agentName=" + agentName +
        ", mchId=" + mchId +
        ", mchName=" + mchName +
        ", shopId=" + shopId +
        ", shopName=" + shopName +
        ", payWay=" + payWay +
        ", payType=" + payType +
        ", contractId=" + contractId +
        ", contractType=" + contractType +
        ", contractNo=" + contractNo +
        ", contractName=" + contractName +
        ", productNo=" + productNo +
        ", productName=" + productName +
        ", channelId=" + channelId +
        ", channelNo=" + channelNo +
        ", channelName=" + channelName +
        ", tradeAmount=" + tradeAmount +
        ", tradeTime=" + tradeTime +
        ", settleType=" + settleType +
        ", settleRiskDay=" + settleRiskDay +
        ", settleDate=" + settleDate +
        ", settleRate=" + settleRate +
        ", settleAgentRate=" + settleAgentRate +
        ", settleAgentFee=" + settleAgentFee +
        ", settlePlatformProfit=" + settlePlatformProfit +
        ", settleAgentProfit=" + settleAgentProfit +
        ", settleMchProfit=" + settleMchProfit +
        ", settleStatus=" + settleStatus +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
