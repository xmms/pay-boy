package com.ndood.notify.base.pojo.record;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
@TableName("r_settle_rec_agent_order")
public class RSettleRecAgentOrderPo extends Model<RSettleRecAgentOrderPo> {

    private static final long serialVersionUID = 1L;

    private String settleOrderNo;

    /**
     * 结算类型：1 二清 2 直清 3 子商户
     */
    private Integer settleType;

    /**
     * 代理商ID
     */
    private Integer agentId;

    /**
     * 代理商名称
     */
    private String agentName;

    /**
     * 日期，唯一索引，表示一天只能结算1次
     */
    private String day;

    /**
     * 结算金额
     */
    private BigDecimal amount;

    /**
     * 交易流水数量
     */
    private Integer count;

    /**
     * 银行编号
     */
    private String bankNo;

    /**
     * 银行名称；支付宝；微信
     */
    private String bankName;

    /**
     * 银行账号
     */
    private String accountNo;

    /**
     * 银行账号名称
     */
    private String accountName;

    /**
     * 普通商户模式结算类型：1 费率 2 金额
     */
    private Integer feeType;

    /**
     * 平台结算费率
     */
    private BigDecimal operatorSettleRate;

    /**
     * 运营商结算金额
     */
    private BigDecimal operatorSettleFee;

    /**
     * 平台结算金额
     */
    private BigDecimal operatorSettleAmount;

    /**
     * 代理商结算费率
     */
    private BigDecimal agentSettleRate;

    /**
     * 代理商结算费用
     */
    private BigDecimal agentSettleFee;

    /**
     * 代理商结算金额
     */
    private BigDecimal agentSettleAmount;

    /**
     * 商户返佣费率
     */
    private BigDecimal mchBackRate;

    /**
     * 商户返佣费用
     */
    private BigDecimal mchBackAmount;

    /**
     * 风险预存期
     */
    private Integer riskDay;

    /**
     * 最早可结算日
     */
    private LocalDate freeDate;

    /**
     * 结算状态： 0 待审核 1 待打款 2 打款结束
     */
    private Integer status;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


    public String getSettleOrderNo() {
        return settleOrderNo;
    }

    public void setSettleOrderNo(String settleOrderNo) {
        this.settleOrderNo = settleOrderNo;
    }

    public Integer getSettleType() {
        return settleType;
    }

    public void setSettleType(Integer settleType) {
        this.settleType = settleType;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getBankNo() {
        return bankNo;
    }

    public void setBankNo(String bankNo) {
        this.bankNo = bankNo;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public Integer getFeeType() {
        return feeType;
    }

    public void setFeeType(Integer feeType) {
        this.feeType = feeType;
    }

    public BigDecimal getOperatorSettleRate() {
        return operatorSettleRate;
    }

    public void setOperatorSettleRate(BigDecimal operatorSettleRate) {
        this.operatorSettleRate = operatorSettleRate;
    }

    public BigDecimal getOperatorSettleFee() {
        return operatorSettleFee;
    }

    public void setOperatorSettleFee(BigDecimal operatorSettleFee) {
        this.operatorSettleFee = operatorSettleFee;
    }

    public BigDecimal getOperatorSettleAmount() {
        return operatorSettleAmount;
    }

    public void setOperatorSettleAmount(BigDecimal operatorSettleAmount) {
        this.operatorSettleAmount = operatorSettleAmount;
    }

    public BigDecimal getAgentSettleRate() {
        return agentSettleRate;
    }

    public void setAgentSettleRate(BigDecimal agentSettleRate) {
        this.agentSettleRate = agentSettleRate;
    }

    public BigDecimal getAgentSettleFee() {
        return agentSettleFee;
    }

    public void setAgentSettleFee(BigDecimal agentSettleFee) {
        this.agentSettleFee = agentSettleFee;
    }

    public BigDecimal getAgentSettleAmount() {
        return agentSettleAmount;
    }

    public void setAgentSettleAmount(BigDecimal agentSettleAmount) {
        this.agentSettleAmount = agentSettleAmount;
    }

    public BigDecimal getMchBackRate() {
        return mchBackRate;
    }

    public void setMchBackRate(BigDecimal mchBackRate) {
        this.mchBackRate = mchBackRate;
    }

    public BigDecimal getMchBackAmount() {
        return mchBackAmount;
    }

    public void setMchBackAmount(BigDecimal mchBackAmount) {
        this.mchBackAmount = mchBackAmount;
    }

    public Integer getRiskDay() {
        return riskDay;
    }

    public void setRiskDay(Integer riskDay) {
        this.riskDay = riskDay;
    }

    public LocalDate getFreeDate() {
        return freeDate;
    }

    public void setFreeDate(LocalDate freeDate) {
        this.freeDate = freeDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.settleOrderNo;
    }

    @Override
    public String toString() {
        return "RSettleRecAgentOrderPo{" +
        ", settleOrderNo=" + settleOrderNo +
        ", settleType=" + settleType +
        ", agentId=" + agentId +
        ", agentName=" + agentName +
        ", day=" + day +
        ", amount=" + amount +
        ", count=" + count +
        ", bankNo=" + bankNo +
        ", bankName=" + bankName +
        ", accountNo=" + accountNo +
        ", accountName=" + accountName +
        ", feeType=" + feeType +
        ", operatorSettleRate=" + operatorSettleRate +
        ", operatorSettleFee=" + operatorSettleFee +
        ", operatorSettleAmount=" + operatorSettleAmount +
        ", agentSettleRate=" + agentSettleRate +
        ", agentSettleFee=" + agentSettleFee +
        ", agentSettleAmount=" + agentSettleAmount +
        ", mchBackRate=" + mchBackRate +
        ", mchBackAmount=" + mchBackAmount +
        ", riskDay=" + riskDay +
        ", freeDate=" + freeDate +
        ", status=" + status +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
