package com.ndood.notify.app.rabbitmq;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {
 
	/**
	 * 阶梯消息异步通知队列
	 */
    @Bean
    public Queue notifyQueue() {
        return new Queue("notify");
    }
 
}
