package com.ndood.rpc.product;

import java.util.List;

import com.googlecode.jsonrpc4j.JsonRpcService;
import com.ndood.rpc.product.entity.dto.RpcProductQuery;
import com.ndood.rpc.product.entity.query.RpcProductDto;

/**
 * 支付产品 v1
 * 跨语言rpc接口
 */
@JsonRpcService("jsonrpc/v1/product")
public interface ProductRpc {
	
	/**
	 * 1 查询多个产品
	 */
	public List<RpcProductDto> list(RpcProductQuery query);
	
	/**
	 * 2 查询单个产品
	 */
	public RpcProductDto findOne(Integer id);
}