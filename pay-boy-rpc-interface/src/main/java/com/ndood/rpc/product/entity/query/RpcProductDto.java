package com.ndood.rpc.product.entity.query;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RpcProductDto implements Serializable{
	private static final long serialVersionUID = -736208518606945293L;

	private Integer id;
	
	private String name;
	
}
