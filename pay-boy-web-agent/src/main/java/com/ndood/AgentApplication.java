package com.ndood;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Agent启动类
 * @author ndood
 */
@SpringBootApplication
@EnableWebMvc
@EnableSwagger2
@EnableCaching
@EnableTransactionManagement
@MapperScan({"com.ndood.agent.dao.*","com.ndood.agent.dao.*.extend"})  
public class AgentApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(AgentApplication.class, args);
	}
	
}