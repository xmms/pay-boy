package com.ndood.agent.service.user.impl;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ndood.agent.core.constaints.AgentErrCode;
import com.ndood.agent.core.exception.AgentException;
import com.ndood.agent.dao.system.StaffDao;
import com.ndood.agent.dao.system.extend.ExtendStaffDao;
import com.ndood.agent.pojo.system.StaffPo;
import com.ndood.agent.pojo.system.dto.StaffDto;
import com.ndood.agent.service.user.AccountInfoService;

@Service
public class AccountInfoServiceImpl implements AccountInfoService{

	@Autowired
	private ExtendStaffDao extendStaffDao;
	
	@Autowired
	private StaffDao staffDao;	
	
	@Override
	public StaffDto getAccountInfoById(String userId) throws Exception {
		
		if(StringUtils.isBlank(userId)) {
			throw new AgentException(AgentErrCode.ERR_OTHER,"用户不存在！");
		}
		
		StaffDto dto = extendStaffDao.extendFindById(Integer.parseInt(userId));
		if(dto == null) {
			throw new AgentException(AgentErrCode.ERR_OTHER,"用户不存在！");
		}
		
		// 不能暴露密码
		dto.setPassword(null);
		dto.setTradePassword(null);
		return dto;
		
	}

	@Override
	public void updateAccountInfo(StaffDto dto) throws Exception {
		
		dto.setPassword(null);
		dto.setTradePassword(null);
		dto.setMobile(null);
		dto.setEmail(null);
		dto.setUpdateTime(new Date());
		
		StaffPo po = new StaffPo();
		po.setId(dto.getId());
		po.setAgentId(dto.getAgentId());
		po.setCreateTime(dto.getCreateTime());
		po.setEmail(dto.getEmail());
		po.setHeadImgUrl(dto.getHeadImgUrl());
		po.setLastLoginIp(dto.getLastLoginIp());
		po.setLastLoginTime(dto.getLastLoginTime());
		po.setMobile(dto.getMobile());
		po.setNickName(dto.getNickName());
		po.setPassword(dto.getPassword());
		po.setStatus(dto.getStatus());
		po.setTradePassword(dto.getTradePassword());
		po.setUpdateTime(dto.getUpdateTime());
		staffDao.updateByPrimaryKeySelective(po);
		
	}
	
}
