package com.ndood.agent.service.system;

import java.util.List;

import com.ndood.agent.pojo.system.dto.RoleDto;

public interface SystemRoleService {

	/**
	 * 获取角色列表
	 */
	List<RoleDto> getRoles();

}
