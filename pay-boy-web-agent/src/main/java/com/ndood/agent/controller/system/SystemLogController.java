package com.ndood.agent.controller.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ndood.agent.pojo.comm.dto.DataTableDto;
import com.ndood.agent.pojo.system.query.AgentLogQo;
import com.ndood.agent.service.system.SystemLogService;

/**
 * 日志控制器类
 * @author ndood
 */
@Controller
public class SystemLogController {
	
	@Autowired
	private SystemLogService systemLogService;
	
	/**
	 * 显示角色页
	 */
	@GetMapping("/system/log")
	public String toRolePage(){
		return "system/log/log_page";
	}
	
	/**
	 * 日志列表
	 */
	@PostMapping("/system/log/query")
	@ResponseBody
	public DataTableDto getDectListPage(@RequestBody AgentLogQo query) throws Exception{
		DataTableDto page = systemLogService.pageLogList(query);
		return page;
	}
	
}
