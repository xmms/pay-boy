package com.ndood.agent.controller.system;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ndood.agent.pojo.comm.dto.DataTableDto;
import com.ndood.agent.pojo.comm.vo.AgentResultVo;
import com.ndood.agent.pojo.system.RolePo;
import com.ndood.agent.pojo.system.dto.RoleDto;
import com.ndood.agent.pojo.system.dto.StaffDto;
import com.ndood.agent.pojo.system.query.StaffQo;
import com.ndood.agent.service.system.SystemRoleService;
import com.ndood.agent.service.system.SystemStaffService;

/**
 * 员工控制器类
 * @author ndood
 */
@Controller
public class SystemStaffController {
	@Autowired
	private SystemStaffService systemStaffService;
	
	@Autowired
	private SystemRoleService systemRoleService;
	
	/**
	 * 显示用户页
	 * 坑，如果GetMapping不写value，有可能访问不到静态资源
	 */
	@GetMapping("/system/staff")
	public String toStaffPage(){
		return "system/staff/staff_page";
	}
	
	/**
	 * 跳转到添加页
	 * @throws Exception 
	 */
	@GetMapping("/system/staff/add")
	public String toAddStaff(Model model) throws Exception{
		// Step1: 查询出所有角色
		List<RoleDto> roles = systemRoleService.getRoles();
		model.addAttribute("roles", roles);
		return "system/staff/staff_add";
	}
	
	/**
	 * 添加用户
	 * @throws Exception 
	 */
	@PostMapping("/system/staff/add")
	@ResponseBody
	public AgentResultVo addStaff(@RequestBody StaffDto staff) throws Exception{
		systemStaffService.addStaff(staff);
		return AgentResultVo.ok().setMsg("添加用户成功！");
	}
	
	/**
	 * 删除用户
	 */
	@PostMapping("/system/staff/delete")
	@ResponseBody
	public AgentResultVo deleteStaff(Integer id){
		Integer[] ids = new Integer[]{id};
		systemStaffService.batchDeleteStaff(ids);
		return AgentResultVo.ok().setMsg("删除用户成功！");
	}

	/**
	 * 批量删除用户
	 */
	@PostMapping("/system/staff/batch_delete")
	@ResponseBody
	public AgentResultVo batchDeleteStaff(@RequestParam("ids[]") Integer[] ids){
		systemStaffService.batchDeleteStaff(ids);
		return AgentResultVo.ok().setMsg("批量删除用户成功！");
	}
	
	/**
	 * 显示修改对话框
	 */
	@GetMapping("/system/staff/update")
	public String toUpdateStaff(Integer id, Model model) throws Exception{
		StaffDto staff = systemStaffService.getStaff(id);
		List<RoleDto> roles = systemRoleService.getRoles();
		for (RoleDto role : roles) {
			for (RolePo urole : staff.getRoles()) {
				if(urole.getId()==role.getId()) {
					role.setIsInChecked(true);
					break;
				}
			}
		}
		model.addAttribute("roles", roles);
		model.addAttribute("staff", staff);
		return "system/staff/staff_update";
	}
	
	/**
	 * 修改用户
	 * @throws Exception 
	 */
	@PostMapping("/system/staff/update")
	@ResponseBody
	public AgentResultVo updateStaff(@RequestBody StaffDto staff) throws Exception{
		systemStaffService.updateStaff(staff);
		return AgentResultVo.ok().setMsg("修改用户成功！");
	}
	
	/**
	 * 用户列表
	 * @throws Exception 
	 */
	@PostMapping("/system/staff/query")
	@ResponseBody
	public DataTableDto getDectListPage(@RequestBody StaffQo query) throws Exception{
		DataTableDto page = systemStaffService.pageStaffList(query);
		return page;
	}
}