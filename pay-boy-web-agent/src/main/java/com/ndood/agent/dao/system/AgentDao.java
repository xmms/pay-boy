package com.ndood.agent.dao.system;

import com.ndood.agent.pojo.system.AgentPo;
import com.ndood.agent.pojo.system.AgentQuery;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AgentDao {
    int countByExample(AgentQuery example);

    int deleteByExample(AgentQuery example);

    int deleteByPrimaryKey(Integer id);

    int insert(AgentPo record);

    int insertSelective(AgentPo record);

    List<AgentPo> selectByExample(AgentQuery example);

    AgentPo selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") AgentPo record, @Param("example") AgentQuery example);

    int updateByExample(@Param("record") AgentPo record, @Param("example") AgentQuery example);

    int updateByPrimaryKeySelective(AgentPo record);

    int updateByPrimaryKey(AgentPo record);
}