package com.ndood.agent.dao.system;

import com.ndood.agent.pojo.system.AgentLogPo;
import com.ndood.agent.pojo.system.AgentLogQuery;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AgentLogDao {
    int countByExample(AgentLogQuery example);

    int deleteByExample(AgentLogQuery example);

    int deleteByPrimaryKey(String recordNo);

    int insert(AgentLogPo record);

    int insertSelective(AgentLogPo record);

    List<AgentLogPo> selectByExample(AgentLogQuery example);

    AgentLogPo selectByPrimaryKey(String recordNo);

    int updateByExampleSelective(@Param("record") AgentLogPo record, @Param("example") AgentLogQuery example);

    int updateByExample(@Param("record") AgentLogPo record, @Param("example") AgentLogQuery example);

    int updateByPrimaryKeySelective(AgentLogPo record);

    int updateByPrimaryKey(AgentLogPo record);
}