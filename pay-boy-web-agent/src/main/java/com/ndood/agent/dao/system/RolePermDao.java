package com.ndood.agent.dao.system;

import com.ndood.agent.pojo.system.RolePermPo;
import com.ndood.agent.pojo.system.RolePermQuery;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RolePermDao {
    int countByExample(RolePermQuery example);

    int deleteByExample(RolePermQuery example);

    int insert(RolePermPo record);

    int insertSelective(RolePermPo record);

    List<RolePermPo> selectByExample(RolePermQuery example);

    int updateByExampleSelective(@Param("record") RolePermPo record, @Param("example") RolePermQuery example);

    int updateByExample(@Param("record") RolePermPo record, @Param("example") RolePermQuery example);
}