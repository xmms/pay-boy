package com.ndood.agent.dao.system;

import com.ndood.agent.pojo.system.PermPo;
import com.ndood.agent.pojo.system.PermQuery;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PermDao {
    int countByExample(PermQuery example);

    int deleteByExample(PermQuery example);

    int deleteByPrimaryKey(Integer id);

    int insert(PermPo record);

    int insertSelective(PermPo record);

    List<PermPo> selectByExample(PermQuery example);

    PermPo selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") PermPo record, @Param("example") PermQuery example);

    int updateByExample(@Param("record") PermPo record, @Param("example") PermQuery example);

    int updateByPrimaryKeySelective(PermPo record);

    int updateByPrimaryKey(PermPo record);
}