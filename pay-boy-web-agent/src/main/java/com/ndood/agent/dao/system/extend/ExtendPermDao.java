package com.ndood.agent.dao.system.extend;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.ndood.agent.pojo.system.PermPo;

public interface ExtendPermDao {

	/**
	 * 根据roleid查询出所有perm资源
	 */
	@Select("SELECT `id`, `name`, `desc`, `status`, `icon`, `sort`, `type`, `url`, `parent_id`, `create_time`, `update_time` FROM a_perm "
			+ " WHERE `id` IN ( SELECT `perm_id` from a_role_perm WHERE `role_id` = #{roleId}) and `status` = 1")
	List<PermPo> findByRoleId(Integer roleId);

}
