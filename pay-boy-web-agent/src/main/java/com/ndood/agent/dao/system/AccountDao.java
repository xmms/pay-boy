package com.ndood.agent.dao.system;

import com.ndood.agent.pojo.system.AccountPo;
import com.ndood.agent.pojo.system.AccountQuery;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AccountDao {
    int countByExample(AccountQuery example);

    int deleteByExample(AccountQuery example);

    int deleteByPrimaryKey(Integer id);

    int insert(AccountPo record);

    int insertSelective(AccountPo record);

    List<AccountPo> selectByExample(AccountQuery example);

    AccountPo selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") AccountPo record, @Param("example") AccountQuery example);

    int updateByExample(@Param("record") AccountPo record, @Param("example") AccountQuery example);

    int updateByPrimaryKeySelective(AccountPo record);

    int updateByPrimaryKey(AccountPo record);
}