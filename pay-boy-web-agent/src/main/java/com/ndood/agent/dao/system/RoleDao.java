package com.ndood.agent.dao.system;

import com.ndood.agent.pojo.system.RolePo;
import com.ndood.agent.pojo.system.RoleQuery;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RoleDao {
    int countByExample(RoleQuery example);

    int deleteByExample(RoleQuery example);

    int deleteByPrimaryKey(Integer id);

    int insert(RolePo record);

    int insertSelective(RolePo record);

    List<RolePo> selectByExample(RoleQuery example);

    RolePo selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") RolePo record, @Param("example") RoleQuery example);

    int updateByExample(@Param("record") RolePo record, @Param("example") RoleQuery example);

    int updateByPrimaryKeySelective(RolePo record);

    int updateByPrimaryKey(RolePo record);
}