package com.ndood.agent.pojo.system;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AgentQuery {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Integer pageNo = 1;

    protected Integer startRow;

    protected Integer pageSize = 10;

    protected String fields;

    public AgentQuery() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo=pageNo;
        this.startRow = (pageNo-1)*this.pageSize;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setStartRow(Integer startRow) {
        this.startRow=startRow;
    }

    public Integer getStartRow() {
        return startRow;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize=pageSize;
        this.startRow = (pageNo-1)*this.pageSize;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setFields(String fields) {
        this.fields=fields;
    }

    public String getFields() {
        return fields;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andParentIdIsNull() {
            addCriterion("parent_id is null");
            return (Criteria) this;
        }

        public Criteria andParentIdIsNotNull() {
            addCriterion("parent_id is not null");
            return (Criteria) this;
        }

        public Criteria andParentIdEqualTo(Integer value) {
            addCriterion("parent_id =", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotEqualTo(Integer value) {
            addCriterion("parent_id <>", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdGreaterThan(Integer value) {
            addCriterion("parent_id >", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("parent_id >=", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdLessThan(Integer value) {
            addCriterion("parent_id <", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdLessThanOrEqualTo(Integer value) {
            addCriterion("parent_id <=", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdIn(List<Integer> values) {
            addCriterion("parent_id in", values, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotIn(List<Integer> values) {
            addCriterion("parent_id not in", values, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdBetween(Integer value1, Integer value2) {
            addCriterion("parent_id between", value1, value2, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotBetween(Integer value1, Integer value2) {
            addCriterion("parent_id not between", value1, value2, "parentId");
            return (Criteria) this;
        }

        public Criteria andGetRateIsNull() {
            addCriterion("get_rate is null");
            return (Criteria) this;
        }

        public Criteria andGetRateIsNotNull() {
            addCriterion("get_rate is not null");
            return (Criteria) this;
        }

        public Criteria andGetRateEqualTo(BigDecimal value) {
            addCriterion("get_rate =", value, "getRate");
            return (Criteria) this;
        }

        public Criteria andGetRateNotEqualTo(BigDecimal value) {
            addCriterion("get_rate <>", value, "getRate");
            return (Criteria) this;
        }

        public Criteria andGetRateGreaterThan(BigDecimal value) {
            addCriterion("get_rate >", value, "getRate");
            return (Criteria) this;
        }

        public Criteria andGetRateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("get_rate >=", value, "getRate");
            return (Criteria) this;
        }

        public Criteria andGetRateLessThan(BigDecimal value) {
            addCriterion("get_rate <", value, "getRate");
            return (Criteria) this;
        }

        public Criteria andGetRateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("get_rate <=", value, "getRate");
            return (Criteria) this;
        }

        public Criteria andGetRateIn(List<BigDecimal> values) {
            addCriterion("get_rate in", values, "getRate");
            return (Criteria) this;
        }

        public Criteria andGetRateNotIn(List<BigDecimal> values) {
            addCriterion("get_rate not in", values, "getRate");
            return (Criteria) this;
        }

        public Criteria andGetRateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("get_rate between", value1, value2, "getRate");
            return (Criteria) this;
        }

        public Criteria andGetRateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("get_rate not between", value1, value2, "getRate");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andMobileIsNull() {
            addCriterion("mobile is null");
            return (Criteria) this;
        }

        public Criteria andMobileIsNotNull() {
            addCriterion("mobile is not null");
            return (Criteria) this;
        }

        public Criteria andMobileEqualTo(String value) {
            addCriterion("mobile =", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotEqualTo(String value) {
            addCriterion("mobile <>", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThan(String value) {
            addCriterion("mobile >", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThanOrEqualTo(String value) {
            addCriterion("mobile >=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThan(String value) {
            addCriterion("mobile <", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThanOrEqualTo(String value) {
            addCriterion("mobile <=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLike(String value) {
            addCriterion("mobile like", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotLike(String value) {
            addCriterion("mobile not like", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileIn(List<String> values) {
            addCriterion("mobile in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotIn(List<String> values) {
            addCriterion("mobile not in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileBetween(String value1, String value2) {
            addCriterion("mobile between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotBetween(String value1, String value2) {
            addCriterion("mobile not between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andEmailIsNull() {
            addCriterion("email is null");
            return (Criteria) this;
        }

        public Criteria andEmailIsNotNull() {
            addCriterion("email is not null");
            return (Criteria) this;
        }

        public Criteria andEmailEqualTo(String value) {
            addCriterion("email =", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotEqualTo(String value) {
            addCriterion("email <>", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThan(String value) {
            addCriterion("email >", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThanOrEqualTo(String value) {
            addCriterion("email >=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThan(String value) {
            addCriterion("email <", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThanOrEqualTo(String value) {
            addCriterion("email <=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLike(String value) {
            addCriterion("email like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotLike(String value) {
            addCriterion("email not like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailIn(List<String> values) {
            addCriterion("email in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotIn(List<String> values) {
            addCriterion("email not in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailBetween(String value1, String value2) {
            addCriterion("email between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotBetween(String value1, String value2) {
            addCriterion("email not between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andQqIsNull() {
            addCriterion("qq is null");
            return (Criteria) this;
        }

        public Criteria andQqIsNotNull() {
            addCriterion("qq is not null");
            return (Criteria) this;
        }

        public Criteria andQqEqualTo(String value) {
            addCriterion("qq =", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqNotEqualTo(String value) {
            addCriterion("qq <>", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqGreaterThan(String value) {
            addCriterion("qq >", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqGreaterThanOrEqualTo(String value) {
            addCriterion("qq >=", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqLessThan(String value) {
            addCriterion("qq <", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqLessThanOrEqualTo(String value) {
            addCriterion("qq <=", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqLike(String value) {
            addCriterion("qq like", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqNotLike(String value) {
            addCriterion("qq not like", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqIn(List<String> values) {
            addCriterion("qq in", values, "qq");
            return (Criteria) this;
        }

        public Criteria andQqNotIn(List<String> values) {
            addCriterion("qq not in", values, "qq");
            return (Criteria) this;
        }

        public Criteria andQqBetween(String value1, String value2) {
            addCriterion("qq between", value1, value2, "qq");
            return (Criteria) this;
        }

        public Criteria andQqNotBetween(String value1, String value2) {
            addCriterion("qq not between", value1, value2, "qq");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("remark is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("remark is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("remark =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("remark <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("remark >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("remark >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("remark <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("remark <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("remark like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("remark not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("remark in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("remark not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("remark between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("remark not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRiskDayIsNull() {
            addCriterion("risk_day is null");
            return (Criteria) this;
        }

        public Criteria andRiskDayIsNotNull() {
            addCriterion("risk_day is not null");
            return (Criteria) this;
        }

        public Criteria andRiskDayEqualTo(Integer value) {
            addCriterion("risk_day =", value, "riskDay");
            return (Criteria) this;
        }

        public Criteria andRiskDayNotEqualTo(Integer value) {
            addCriterion("risk_day <>", value, "riskDay");
            return (Criteria) this;
        }

        public Criteria andRiskDayGreaterThan(Integer value) {
            addCriterion("risk_day >", value, "riskDay");
            return (Criteria) this;
        }

        public Criteria andRiskDayGreaterThanOrEqualTo(Integer value) {
            addCriterion("risk_day >=", value, "riskDay");
            return (Criteria) this;
        }

        public Criteria andRiskDayLessThan(Integer value) {
            addCriterion("risk_day <", value, "riskDay");
            return (Criteria) this;
        }

        public Criteria andRiskDayLessThanOrEqualTo(Integer value) {
            addCriterion("risk_day <=", value, "riskDay");
            return (Criteria) this;
        }

        public Criteria andRiskDayIn(List<Integer> values) {
            addCriterion("risk_day in", values, "riskDay");
            return (Criteria) this;
        }

        public Criteria andRiskDayNotIn(List<Integer> values) {
            addCriterion("risk_day not in", values, "riskDay");
            return (Criteria) this;
        }

        public Criteria andRiskDayBetween(Integer value1, Integer value2) {
            addCriterion("risk_day between", value1, value2, "riskDay");
            return (Criteria) this;
        }

        public Criteria andRiskDayNotBetween(Integer value1, Integer value2) {
            addCriterion("risk_day not between", value1, value2, "riskDay");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMaxIsNull() {
            addCriterion("risk_withdraw_max is null");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMaxIsNotNull() {
            addCriterion("risk_withdraw_max is not null");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMaxEqualTo(BigDecimal value) {
            addCriterion("risk_withdraw_max =", value, "riskWithdrawMax");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMaxNotEqualTo(BigDecimal value) {
            addCriterion("risk_withdraw_max <>", value, "riskWithdrawMax");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMaxGreaterThan(BigDecimal value) {
            addCriterion("risk_withdraw_max >", value, "riskWithdrawMax");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMaxGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("risk_withdraw_max >=", value, "riskWithdrawMax");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMaxLessThan(BigDecimal value) {
            addCriterion("risk_withdraw_max <", value, "riskWithdrawMax");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMaxLessThanOrEqualTo(BigDecimal value) {
            addCriterion("risk_withdraw_max <=", value, "riskWithdrawMax");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMaxIn(List<BigDecimal> values) {
            addCriterion("risk_withdraw_max in", values, "riskWithdrawMax");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMaxNotIn(List<BigDecimal> values) {
            addCriterion("risk_withdraw_max not in", values, "riskWithdrawMax");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMaxBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("risk_withdraw_max between", value1, value2, "riskWithdrawMax");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMaxNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("risk_withdraw_max not between", value1, value2, "riskWithdrawMax");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMinIsNull() {
            addCriterion("risk_withdraw_min is null");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMinIsNotNull() {
            addCriterion("risk_withdraw_min is not null");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMinEqualTo(BigDecimal value) {
            addCriterion("risk_withdraw_min =", value, "riskWithdrawMin");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMinNotEqualTo(BigDecimal value) {
            addCriterion("risk_withdraw_min <>", value, "riskWithdrawMin");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMinGreaterThan(BigDecimal value) {
            addCriterion("risk_withdraw_min >", value, "riskWithdrawMin");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMinGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("risk_withdraw_min >=", value, "riskWithdrawMin");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMinLessThan(BigDecimal value) {
            addCriterion("risk_withdraw_min <", value, "riskWithdrawMin");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMinLessThanOrEqualTo(BigDecimal value) {
            addCriterion("risk_withdraw_min <=", value, "riskWithdrawMin");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMinIn(List<BigDecimal> values) {
            addCriterion("risk_withdraw_min in", values, "riskWithdrawMin");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMinNotIn(List<BigDecimal> values) {
            addCriterion("risk_withdraw_min not in", values, "riskWithdrawMin");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMinBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("risk_withdraw_min between", value1, value2, "riskWithdrawMin");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMinNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("risk_withdraw_min not between", value1, value2, "riskWithdrawMin");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMonthAmountIsNull() {
            addCriterion("risk_withdraw_month_amount is null");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMonthAmountIsNotNull() {
            addCriterion("risk_withdraw_month_amount is not null");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMonthAmountEqualTo(BigDecimal value) {
            addCriterion("risk_withdraw_month_amount =", value, "riskWithdrawMonthAmount");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMonthAmountNotEqualTo(BigDecimal value) {
            addCriterion("risk_withdraw_month_amount <>", value, "riskWithdrawMonthAmount");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMonthAmountGreaterThan(BigDecimal value) {
            addCriterion("risk_withdraw_month_amount >", value, "riskWithdrawMonthAmount");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMonthAmountGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("risk_withdraw_month_amount >=", value, "riskWithdrawMonthAmount");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMonthAmountLessThan(BigDecimal value) {
            addCriterion("risk_withdraw_month_amount <", value, "riskWithdrawMonthAmount");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMonthAmountLessThanOrEqualTo(BigDecimal value) {
            addCriterion("risk_withdraw_month_amount <=", value, "riskWithdrawMonthAmount");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMonthAmountIn(List<BigDecimal> values) {
            addCriterion("risk_withdraw_month_amount in", values, "riskWithdrawMonthAmount");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMonthAmountNotIn(List<BigDecimal> values) {
            addCriterion("risk_withdraw_month_amount not in", values, "riskWithdrawMonthAmount");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMonthAmountBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("risk_withdraw_month_amount between", value1, value2, "riskWithdrawMonthAmount");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMonthAmountNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("risk_withdraw_month_amount not between", value1, value2, "riskWithdrawMonthAmount");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMonthCountIsNull() {
            addCriterion("risk_withdraw_month_count is null");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMonthCountIsNotNull() {
            addCriterion("risk_withdraw_month_count is not null");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMonthCountEqualTo(Integer value) {
            addCriterion("risk_withdraw_month_count =", value, "riskWithdrawMonthCount");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMonthCountNotEqualTo(Integer value) {
            addCriterion("risk_withdraw_month_count <>", value, "riskWithdrawMonthCount");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMonthCountGreaterThan(Integer value) {
            addCriterion("risk_withdraw_month_count >", value, "riskWithdrawMonthCount");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMonthCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("risk_withdraw_month_count >=", value, "riskWithdrawMonthCount");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMonthCountLessThan(Integer value) {
            addCriterion("risk_withdraw_month_count <", value, "riskWithdrawMonthCount");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMonthCountLessThanOrEqualTo(Integer value) {
            addCriterion("risk_withdraw_month_count <=", value, "riskWithdrawMonthCount");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMonthCountIn(List<Integer> values) {
            addCriterion("risk_withdraw_month_count in", values, "riskWithdrawMonthCount");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMonthCountNotIn(List<Integer> values) {
            addCriterion("risk_withdraw_month_count not in", values, "riskWithdrawMonthCount");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMonthCountBetween(Integer value1, Integer value2) {
            addCriterion("risk_withdraw_month_count between", value1, value2, "riskWithdrawMonthCount");
            return (Criteria) this;
        }

        public Criteria andRiskWithdrawMonthCountNotBetween(Integer value1, Integer value2) {
            addCriterion("risk_withdraw_month_count not between", value1, value2, "riskWithdrawMonthCount");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Byte value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Byte value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Byte value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Byte value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Byte value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Byte> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Byte> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Byte value1, Byte value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andLogicDeleteIsNull() {
            addCriterion("logic_delete is null");
            return (Criteria) this;
        }

        public Criteria andLogicDeleteIsNotNull() {
            addCriterion("logic_delete is not null");
            return (Criteria) this;
        }

        public Criteria andLogicDeleteEqualTo(Byte value) {
            addCriterion("logic_delete =", value, "logicDelete");
            return (Criteria) this;
        }

        public Criteria andLogicDeleteNotEqualTo(Byte value) {
            addCriterion("logic_delete <>", value, "logicDelete");
            return (Criteria) this;
        }

        public Criteria andLogicDeleteGreaterThan(Byte value) {
            addCriterion("logic_delete >", value, "logicDelete");
            return (Criteria) this;
        }

        public Criteria andLogicDeleteGreaterThanOrEqualTo(Byte value) {
            addCriterion("logic_delete >=", value, "logicDelete");
            return (Criteria) this;
        }

        public Criteria andLogicDeleteLessThan(Byte value) {
            addCriterion("logic_delete <", value, "logicDelete");
            return (Criteria) this;
        }

        public Criteria andLogicDeleteLessThanOrEqualTo(Byte value) {
            addCriterion("logic_delete <=", value, "logicDelete");
            return (Criteria) this;
        }

        public Criteria andLogicDeleteIn(List<Byte> values) {
            addCriterion("logic_delete in", values, "logicDelete");
            return (Criteria) this;
        }

        public Criteria andLogicDeleteNotIn(List<Byte> values) {
            addCriterion("logic_delete not in", values, "logicDelete");
            return (Criteria) this;
        }

        public Criteria andLogicDeleteBetween(Byte value1, Byte value2) {
            addCriterion("logic_delete between", value1, value2, "logicDelete");
            return (Criteria) this;
        }

        public Criteria andLogicDeleteNotBetween(Byte value1, Byte value2) {
            addCriterion("logic_delete not between", value1, value2, "logicDelete");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}