package com.ndood.agent.pojo.system.query;

import java.util.Date;

import com.ndood.agent.pojo.system.StaffPo;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class StaffQo extends StaffPo{
	private static final long serialVersionUID = 2551531117012681679L;
	private Integer offset;
	private Integer limit;
	private String keywords;
	private Date startTime;
	private Date endTime;

	public int getPageNo() {
		return offset / limit;
	}
}
