package com.ndood.agent.pojo.system;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class AgentPo implements Serializable {
    /**
     * 代理商ID
     */
    private Integer id;

    /**
     * 父级代理商ID
     */
    private Integer parentId;

    /**
     * 代理商提成费率
     */
    private BigDecimal getRate;

    /**
     * 商户名称
     */
    private String name;

    /**
     * 手机
     */
    private String mobile;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 企业QQ
     */
    private String qq;

    /**
     * 商户备注
     */
    private String remark;

    /**
     * 风险预存期 单位天
     */
    private Integer riskDay;

    /**
     * 单笔最大允许提现额度(提现，结算)
     */
    private BigDecimal riskWithdrawMax;

    /**
     * 单笔最小允许提现额度
     */
    private BigDecimal riskWithdrawMin;

    /**
     * 每月允许最大提现额度
     */
    private BigDecimal riskWithdrawMonthAmount;

    /**
     * 每月允许最大提现次数
     */
    private Integer riskWithdrawMonthCount;

    /**
     * 状态: 1启用 2 禁用
     */
    private Byte status;

    /**
     * 逻辑删除: 0 未删除 1 删除
     */
    private Byte logicDelete;

    private Date createTime;

    private Date updateTime;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public BigDecimal getGetRate() {
        return getRate;
    }

    public void setGetRate(BigDecimal getRate) {
        this.getRate = getRate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq == null ? null : qq.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Integer getRiskDay() {
        return riskDay;
    }

    public void setRiskDay(Integer riskDay) {
        this.riskDay = riskDay;
    }

    public BigDecimal getRiskWithdrawMax() {
        return riskWithdrawMax;
    }

    public void setRiskWithdrawMax(BigDecimal riskWithdrawMax) {
        this.riskWithdrawMax = riskWithdrawMax;
    }

    public BigDecimal getRiskWithdrawMin() {
        return riskWithdrawMin;
    }

    public void setRiskWithdrawMin(BigDecimal riskWithdrawMin) {
        this.riskWithdrawMin = riskWithdrawMin;
    }

    public BigDecimal getRiskWithdrawMonthAmount() {
        return riskWithdrawMonthAmount;
    }

    public void setRiskWithdrawMonthAmount(BigDecimal riskWithdrawMonthAmount) {
        this.riskWithdrawMonthAmount = riskWithdrawMonthAmount;
    }

    public Integer getRiskWithdrawMonthCount() {
        return riskWithdrawMonthCount;
    }

    public void setRiskWithdrawMonthCount(Integer riskWithdrawMonthCount) {
        this.riskWithdrawMonthCount = riskWithdrawMonthCount;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Byte getLogicDelete() {
        return logicDelete;
    }

    public void setLogicDelete(Byte logicDelete) {
        this.logicDelete = logicDelete;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", parentId=").append(parentId);
        sb.append(", getRate=").append(getRate);
        sb.append(", name=").append(name);
        sb.append(", mobile=").append(mobile);
        sb.append(", email=").append(email);
        sb.append(", qq=").append(qq);
        sb.append(", remark=").append(remark);
        sb.append(", riskDay=").append(riskDay);
        sb.append(", riskWithdrawMax=").append(riskWithdrawMax);
        sb.append(", riskWithdrawMin=").append(riskWithdrawMin);
        sb.append(", riskWithdrawMonthAmount=").append(riskWithdrawMonthAmount);
        sb.append(", riskWithdrawMonthCount=").append(riskWithdrawMonthCount);
        sb.append(", status=").append(status);
        sb.append(", logicDelete=").append(logicDelete);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}