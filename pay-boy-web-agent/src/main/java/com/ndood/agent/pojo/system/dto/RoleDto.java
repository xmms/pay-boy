package com.ndood.agent.pojo.system.dto;

import java.util.List;

import com.google.common.collect.Lists;
import com.ndood.agent.pojo.system.RolePo;

public class RoleDto extends RolePo {

	private static final long serialVersionUID = 7789112041250164652L;

	private List<PermDto> perms = Lists.newArrayList();

	private Boolean isInChecked;
	
	public List<PermDto> getPerms() {
		return perms;
	}

	public void setPerms(List<PermDto> perms) {
		this.perms = perms;
	}

	public Boolean getIsInChecked() {
		return isInChecked;
	}

	public void setIsInChecked(Boolean isInChecked) {
		this.isInChecked = isInChecked;
	}

}