package com.ndood.agent.pojo.system.dto;

import com.ndood.agent.pojo.system.AgentLogPo;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class AgentLogDto extends AgentLogPo{
	private static final long serialVersionUID = 3759807312319273284L;
}
