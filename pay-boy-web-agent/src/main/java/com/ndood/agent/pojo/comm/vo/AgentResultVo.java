package com.ndood.agent.pojo.comm.vo;

import java.io.Serializable;

import com.alibaba.fastjson.annotation.JSONField;
import com.ndood.agent.core.constaints.AgentErrCode;

/**
 * 普通对象返回数据
 */
public class AgentResultVo implements Serializable {
	private static final long serialVersionUID = 3765308055181715685L;

	public final static AgentResultVo ok() {
		return new AgentResultVo(AgentErrCode.SUCCESS);
	}
	
	public final static AgentResultVo error() {
		return new AgentResultVo(AgentErrCode.ERR_OTHER);
	}
	
	private AgentErrCode code;
	private String msg;
	private Object data;
	
	private AgentResultVo(AgentErrCode code) {
		this.code = code;
		this.msg = code.getValue();
	}

	@JSONField(name = "code")
	public String getCode() {
		return code.getCode();
	}
	
	@JSONField(name = "code")
	public AgentResultVo setCode(String code) {
		this.code = AgentErrCode.getEnum(code);
		return this;
	}

	public String getMsg() {
		return msg;
	}
	
	public AgentResultVo setMsg(String msg) {
		this.msg = msg;
		return this;
	}

	public Object getData() {
		return data;
	}

	public AgentResultVo setData(Object data) {
		this.data = data;
		return this;
	}
}
