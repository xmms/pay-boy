package com.ndood.agent.pojo.system.query;

import java.util.Date;

import com.ndood.agent.pojo.system.AgentLogPo;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class AgentLogQo extends AgentLogPo {
	private static final long serialVersionUID = -6651166712501998424L;
	private Integer offset;
	private Integer limit;
	private String keywords;
	private Date startTime;
	private Date endTime;

	public int getPageNo() {
		return offset / limit;
	}
	
}
