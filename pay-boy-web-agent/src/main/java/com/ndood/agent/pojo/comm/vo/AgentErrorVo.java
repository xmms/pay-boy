package com.ndood.agent.pojo.comm.vo;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class AgentErrorVo implements Serializable {
	private static final long serialVersionUID = 4948544702730668719L;

	private String code;
	private String msg;

	public AgentErrorVo() {
		super();
	}

	public AgentErrorVo(String code, String msg) {
		super();
		this.code = code;
		this.msg = msg;
	}

}
