package com.ndood.agent.core.rpc;

import java.net.MalformedURLException;
import java.net.URL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.googlecode.jsonrpc4j.spring.AutoJsonRpcClientProxyCreator;
import com.ndood.agent.core.properties.AgentProperties;
import com.ndood.rpc.product.ProductRpc;

import lombok.extern.slf4j.Slf4j;

/**
 * rpc客户端配置
 */
@Configuration
@ComponentScan(basePackageClasses={ProductRpc.class})
@Slf4j
public class RpcClientConfiguration {

	@Autowired
	private AgentProperties agentProperties;
	
	@Bean
	public AutoJsonRpcClientProxyCreator rpcClientProxyCreator() {
		AutoJsonRpcClientProxyCreator creator = new AutoJsonRpcClientProxyCreator();
		try {
//			creator.setBaseUrl(new URL(agentProperties.getRpc().getBaseUrl()));
			creator.setBaseUrl(new URL("http://localhost:83/"));
		} catch (MalformedURLException e) {
			log.error("创建rpc服务地址错误",e);
		}
		// 测试扫描所在的包
		creator.setScanPackage(ProductRpc.class.getPackage().getName());
		return creator;
	}
}
