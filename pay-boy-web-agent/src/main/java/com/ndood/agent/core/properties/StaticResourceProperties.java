package com.ndood.agent.core.properties;

import lombok.Data;

/**
 * 静态资源相关配置
 */
@Data
public class StaticResourceProperties {

	/**
	 * 默认是开发环境配置
	 */
	private boolean isDevelop = true;

}
