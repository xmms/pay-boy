package com.ndood.agent.core.constaints;

public enum AgentErrCode {

	SUCCESS("10000", "请求成功"),
	
	ERR_PARAM("10100", "参数错误"),
	
	ERR_OTHER("10101", "(*^__^*)系统开小差了,请稍后重试"), 
	
	ERR_PASSWORD("10102", "密码不正确");
	
    private String code;
    private String value;
    
    private AgentErrCode(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
    
	public static AgentErrCode getEnum(String code) {
		for (AgentErrCode rs : AgentErrCode.values()) {
			if (code==rs.getCode()) {
				return rs;
			}
		}
		return AgentErrCode.ERR_OTHER;
	}
    
}
