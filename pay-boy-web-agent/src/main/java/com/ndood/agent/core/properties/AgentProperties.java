package com.ndood.agent.core.properties;
/**
 * 自定义属性配置类SecurityProperties
 */
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.annotation.Order;

import lombok.Data;

/**
 * ndood agent配置主类
 * @author ndood
 */
@Data
@Order(1)
@ConfigurationProperties(prefix = "ndood.agent")
public class AgentProperties {
	/**
	 * 系统默认域名
	 */
	private String domain = "http://pay.jeepupil.top";
	/**
	 * 默认的sessionid名称
	 */
	private String DefaultCookieName = "JSESSIONID";
	/**
	 * 阿里云通信相关配置
	 */
	private AliyunSmsProperties dayu = new AliyunSmsProperties();
	/**
	 * 阿里云OSS相关配置
	 */
	private AliyunOssProperties oss = new AliyunOssProperties();
	/**
	 * 静态资源相关配置
	 */
	private StaticResourceProperties stat = new StaticResourceProperties();
	/**
	 * 邮件相关配置
	 */
	private AgentEmailProperties email = new AgentEmailProperties();
	/**
	 * 主键生成器
	 */
	private IdWorkerProperties id = new IdWorkerProperties();
	/**
	 * jsonRpc基本路径
	 */
	private RpcProperties rpc = new RpcProperties();
}