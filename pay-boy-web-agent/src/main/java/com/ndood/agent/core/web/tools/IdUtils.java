package com.ndood.agent.core.web.tools;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ndood.agent.core.id.SnowflakeIdWorker;
import com.ndood.common.base.util.DateUtil;

@Component
public class IdUtils {

	@Autowired
	private SnowflakeIdWorker idWorker;
	
	public String getNextId() {
		String id = DateUtil.getNatureDateTimeSequence() + idWorker.nextId();
		return id;
	}
	
}
