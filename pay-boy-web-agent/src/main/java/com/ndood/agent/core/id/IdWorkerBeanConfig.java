package com.ndood.agent.core.id;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ndood.agent.core.properties.AgentProperties;
 
/**
 * @author : zhangxin
 * @version : V1.0
 * @since : 2018/09/15
 */
@Configuration
public class IdWorkerBeanConfig {
	
	@Autowired
	private AgentProperties agentProperties;
	
    /**
     * 配置全局id生成器
     */
    @Bean
    public SnowflakeIdWorker getIdWorker() {
        return new SnowflakeIdWorker(agentProperties.getId().getWorkspaceId(), agentProperties.getId().getMachineId());
    }
    
}
