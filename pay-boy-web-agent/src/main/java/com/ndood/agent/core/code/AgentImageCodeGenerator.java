package com.ndood.agent.core.code;

import org.springframework.web.context.request.ServletWebRequest;

import com.ndood.authenticate.core.validate.code.ValidateCodeGenerator;
import com.ndood.authenticate.core.validate.code.image.ImageCode;

/**
 * 新建验证码生成器，覆盖core ValidateCodeBeanConfig中默认的
 * @author ndood
 */
/*@Component("imageCodeGenerator")*/
public class AgentImageCodeGenerator implements ValidateCodeGenerator{

	/**
	 * 添加生成验证码方法
	 */
	@Override
	public ImageCode generate(ServletWebRequest request) {
		System.out.println("更高级的图形验证码生成代码");
		return null;
	}
	
}