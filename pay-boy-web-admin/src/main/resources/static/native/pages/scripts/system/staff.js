define([
	'jquery',
	'jquery-validation',
	'jquery-extension',
	'layui',
	'bootstrap-datetimepicker',
	'bootstrap-datetimepicker-zh-CN',
	'bootstrap-datepicker',
	'bootstrap-datepicker-zh-CN',
	'bootstrap-table',
	'bootstrap-table-zh-CN',
	'bootstrap-table-mobile',
	'jquery-jstree',
	'jquery-combotree',
	'bootstrap-imguploader',
	'jquery-distselector'
], function() {
	return {
		initUpdateDialog: function(){
			// 出生日期
			$('#staff_update_birthday').datepicker({
				language: 'zh-CN',
				format: 'yyyy-mm-dd',
				autoclose: true
			});
			
			// 图片上传插件
			$("#staff_update_head_image").ndoodImgUploader({
				url: '/oss/aliyun/upload',
				fileType: ["jpg","png","bmp","jpeg"], 
				fileSize: 1024 * 1024 * 10,
				imgNum: 1,
				imgName: 'headImgUrl',
				errorTips: function(msg){
					layer.alert(msg);
				}
			});
			
			// 表单验证
			$("#staff_update_form").validate({
				rules: {
					nickName:{
						required: true,
						rangelength:[2,18],
						isNickName:true,
					},
					mobile:{
						required: true,
						isMobile: true,
					},
					email:{
						required: true,
						isEmail: true,
					},
					roleIds:{
						required: true,
					},
				},
				messages:{
					nickName:{
						required: "用户昵称不能为空.",
						rangelength: $.validator.format("昵称最小长度:{0}, 最大长度:{1}。"),
						isNickName:"用户名必须由字母、数字或特殊符号(@_.)组成.",
					},
					mobile:{
						required: "手机号不能为空.",
						isMobile: "请正确填写您的手机号码.",
					},
					email:{
						required: "邮箱不能为空.",
						isEmail: "请正确填写你的邮箱.",
					},
					roleIds:{
						required: '角色不能为空.',
					},
				},
				errorPlacement: function(error,element) {
					if(element.attr("name")=="deptId"){
						$(element).parent().after(error);
						return;
					}
					if(element.is(":checkbox")){
						$(element).parent().parent().parent();
						$(element).parent().parent().after(error);
						return;
					}
					$(element).parent().addClass('has-error');
					$(element).after(error);
				},
				success:function(element) {
					$(element).parent().removeClass('has-error');
					$(element).parent().find('label.error').remove();
				},
				// 开启隐藏域验证
				ignore: [],
			    // 是否在提交表单时验证
				onsubmit: false,
				// 是否在获取焦点时验证
				onfocusout:false,
				// 是否在敲击键盘时验证
				onkeyup:false,
				// 是否在点击时验证
				onclick: false,
				// 提交表单后，（第一个）未通过验证的表单获得焦点
				focusInvalid:true,
				// 当未通过验证的元素获得焦点时，移除错误提示
				focusCleanup:false,
			});
		},
		initAddDialog: function(){
			// 图片上传插件
			$("#staff_add_head_image").ndoodImgUploader({
				url: '/oss/aliyun/upload',
				fileType: ["jpg","png","bmp","jpeg"], 
				fileSize: 1024 * 1024 * 10,
				imgNum: 1,
				imgName: 'headImgUrl',
				errorTips: function(msg){
					layer.alert(msg);
				}
			});
			
			// 表单验证
			$("#staff_add_form").validate({
				rules: {
					nickName:{
						required: true,
						rangelength:[2,18],
						isNickName:true,
					},
					mobile:{
						required: true,
						isMobile: true,
					},
					email:{
						required: true,
						isEmail: true,
					},
					roleIds:{
						required: true,
					},
				},
				messages:{
					nickName:{
						required: "用户昵称不能为空.",
						rangelength: $.validator.format("昵称最小长度:{0}, 最大长度:{1}。"),
						isNickName:"用户名必须由字母、数字或特殊符号(@_.)组成.",
					},
					mobile:{
						required: "手机号不能为空.",
						isMobile: "请正确填写您的手机号码.",
					},
					email:{
						required: "邮箱不能为空.",
						isEmail: "请正确填写你的邮箱.",
					},
					roleIds:{
						required: '角色不能为空.',
					},
				},
				errorPlacement: function(error,element) {
					if(element.attr("name")=="deptId"){
						$(element).parent().after(error);
						return;
					}
					if(element.is(":checkbox")){
						$(element).parent().parent().parent();
						$(element).parent().parent().after(error);
						return;
					}
					$(element).parent().addClass('has-error');
					$(element).after(error);
				},
				success:function(element) {
					$(element).parent().removeClass('has-error');
					$(element).parent().find('label.error').remove();
				},
				// 开启隐藏域验证
				ignore: [],
			    // 是否在提交表单时验证
				onsubmit: false,
				// 是否在获取焦点时验证
				onfocusout:false,
				// 是否在敲击键盘时验证
				onkeyup:false,
				// 是否在点击时验证
				onclick: false,
				// 提交表单后，（第一个）未通过验证的表单获得焦点
				focusInvalid:true,
				// 当未通过验证的元素获得焦点时，移除错误提示
				focusCleanup:false,
			});
		},
		initPage: function(){
			// 1 初始化日期控件
			$('#staff_start_date').datetimepicker({
				language: 'zh-CN',
				format: 'yyyy-mm-dd hh:ii:00',
				autoclose: true
			});
			
			$('#staff_end_date').datetimepicker({
				language: 'zh-CN',
				format: 'yyyy-mm-dd hh:ii:00',
				autoclose: true
			});	
			
			// 2 初始化表单控件
			$('#staff_datatable').bootstrapTable({
				url : "/system/staff/query", 
				method : 'post', 
				toolbar : '#staff_toolbar',
				uniqueId: "id",
				striped: true,
				cache: false,
				pagination : true, 
				sortable: false,
				singleSelect : false,
				search: true,
				showColumns: true,
				showToggle:false,
				cardView: false,
				showRefresh: true,
				clickToSelect: true,
				minimumCountColumns: 5,
				queryParams : function(params) {
					return {
						limit : params.limit,
						offset : params.offset,
						keywords: params.search,
						sortName:'sort',
						nickName: $("#staff_content").find("form").find("input[name='nickName']").val(),
						startTime: $("#staff_content").find("form").find("input[name='startTime']").val(),
						endTime: $("#staff_content").find("form").find("input[name='endTime']").val()
					};
				},
				sidePagination : "server",
				pageNumber:1, 
				pageSize : 10,
				pageList: [10, 20, 50],
				columns : [
					{
						checkbox : true
					},
					{
						field : 'id',
						title : '编号'
					},
					{
						field : 'nickName',
						title : '昵称',
					},
					{
						field : 'email',
						title : '邮箱'
					},
					{
						field : 'mobile',
						title : '手机'
					},
					{
						field : 'status',
						title : '状态',
						formatter: function(value, row, index){
							if(value==0){
								return '<span class="text-danger"><i class="fa fa-circle"></i> 禁用</span>';
							}else{
								return '<span class="text-success"><i class="fa fa-circle"></i> 启用</span>';
							}
						}
					},
					{
						field : 'lastLoginIp',
						title : '最后登录IP'
					},
					{
						field : 'lastLoginTime',
						title : '最后登录时间'
					},
					{
						visible : false,
						field : 'updateTime',
						title : '最后修改时间'
					},
					{
						title : '操作',
						field : '',
						align : 'center',
						formatter : function(value, row, index) {
							var b1 = '<a href="javascript:void(0);" class="btn btn-xs btn-success btn-editone" onclick="staff_update('+row['id']+')"><i class="fa fa-pencil"></i></a>';
							var b2 = '<a href="javascript:;" class="btn btn-xs btn-success btn-delone" onclick="staff_delete('+row['id']+')"><i class="fa fa-trash"></i></a>';
							return b1 + b2;
						}
					} 
				]
			});
			
			// 3初始化手机响应式
			var docW = window.innerWidth;
			if (docW < 768) {
				$("#staff_search_plus").show();
				$("#staff_search_plus_btn").hide();
				$("#staff_content .bootstrap-table .fixed-table-toolbar input[type='text']").hide();
			}else{
				$("#staff_search_plus").hide();
			}
		}
	}
});

// 添加用户
function staff_add() {
	Layout.reloadAjaxContent('/system/staff/add', $(".page-sidebar").find("a[href='/system/staff']"));
}

//删除用户
function staff_delete(id){
	layer.confirm('确定要删除吗？', {
		btn : [ '确定', '取消' ]
	}, function() {
		$.ajax({
			url : '/system/staff/delete',
			type : "post",
			data : {'id' : id},
			success : function(data) {
				if (data.code != '10000') {
					layer.alert(data.msg,{icon: 2});
					return;
				}
				layer.alert(data.msg, {icon: 1});
				staff_reload();
			},
	        error: function (response, ajaxOptions, thrownError) {
	        	errorCallBack(response, ajaxOptions, thrownError);                
	        }
		});
	})
}

// 批量删除
function staff_batch_delete(){
	var rows = $('#staff_datatable').bootstrapTable('getSelections'); 
	if (rows.length == 0) {
		layer.alert("请至少选择一条记录!",{icon: 7});
		return;
	}
	layer.confirm("确定要删除选中的'" + rows.length + "'条记录吗?", {
		btn : [ '确定', '取消' ]
	}, function() {
		var ids = [];
		$.each(rows, function(i, row) {
			ids.push(row['id']);
		});
		$.ajax({
			type : 'post',
			data : {ids:ids},
			dataType : 'json',
			url : '/system/staff/batch_delete',
			success : function(data) {
				if (data.code != '10000') {
					layer.alert(data.msg,{icon: 2});
					return;
				}
				layer.alert(data.msg, {icon: 1});
				staff_reload();
			},
	        error: function (response, ajaxOptions, thrownError) {
	        	errorCallBack(response, ajaxOptions, thrownError);                
	        }
		});
	}, function() {});
}

// 修改用户
function staff_update(id){
	Layout.reloadAjaxContent('/system/staff/update?id='+id, $(".page-sidebar").find("a[href='/system/staff']"));
}

//高级查询
function staff_search_submit(){
	$('#staff_datatable').bootstrapTable('refresh', null);
}

//显示高级搜索
function staff_search_plus(){
	var search = $('#staff_datatable').parents('.bootstrap-table').find('.fixed-table-toolbar').find("input[type='text']");
	if(!$(search).is(":hidden")){
		// 隐藏普通搜索
		$(search).val('').hide();
		// 显示高级搜索
		$("#staff_search_plus").show();
	}else{
		$("#staff_search_plus").find("form")[0].reset();
		$("#staff_search_plus").hide();
		$(search).show();
	}
}

// 重新加载datatable
function staff_reload(){
	$('#staff_datatable').bootstrapTable('refresh', null);
}

//保存用户
function add_staff(callback){
	// Step1: 表单验证
	// 校验基本表单
	var valid = $("#staff_add_form").valid();
	if(!valid ){
		return;
	}
	
	// Step2: 提交表单
	var data = $('#staff_add_form').serializeJson();
	var roleIds = data['roleIds'];
	if(!(roleIds instanceof Array)){
		var arr = [];
		arr.push(roleIds);
		data['roleIds'] = arr;
	}
	
    $.ajax({
    	url:"/system/staff/add",
        type:"post",
        dataType:'json',
        contentType:"application/json;charset=utf-8",
        data:JSON.stringify(data),
        async:false,
        success:function(data){
        	if(data.code!='10000'){
        		parent.layer.alert(data.msg, {icon: 1});
        		return;
        	}
        	//layer.alert("添加用户成功.");
        	//Layout.reloadAjaxContent('/system/staff', $(".page-sidebar").find("a[href='/system/staff']"));
        	Layout.reloadAjaxContent('/jump_success?url=/system/staff');
        	return;
        },
        error: function (response, ajaxOptions, thrownError) {
        	errorCallBack(response, ajaxOptions, thrownError);                
        }
    });
}

// 保存并添加下一个用户
function add_next(callback){
	// Step1: 表单验证
	var valid = $("#staff_add_form").valid();
	if(!valid){
		return;
	}
	var data = $('#staff_add_form').serializeJson();
	var roleIds = data['roleIds'];
	if(!(roleIds instanceof Array)){
		var arr = [];
		arr.push(roleIds);
		data['roleIds'] = arr;
	}
	
	// Step2: 提交表单
    $.ajax({
    	url:"/system/staff/add",
        type:"post",
        dataType:'json',
        contentType:"application/json;charset=utf-8",
        data:JSON.stringify(data),
        async:false,
        success:function(data){
        	if(data.code!='10000'){
        		parent.layer.alert(data.msg, {icon: 1});
        		return;
        	}
        	Layout.reloadAjaxContent('/system/staff/add', $(".page-sidebar").find("a[href='/system/staff']"));
        	return;
        },
        error: function (response, ajaxOptions, thrownError) {
        	errorCallBack(response, ajaxOptions, thrownError);                
        }
    });
}

function update_staff(callback){
	// Step1: 表单验证
	// 校验基本表单
	var valid = $("#staff_update_form").valid();
	if(!valid){
		return;
	}
	var data = $('#staff_update_form').serializeJson();
	var roleIds = data['roleIds'];
	if(!(roleIds instanceof Array)){
		var arr = [];
		arr.push(roleIds);
		data['roleIds'] = arr;
	}
	console.log(data);
	
	// Step2: 提交表单
    $.ajax({
    	url:"/system/staff/update",
        type:"post",
        dataType:'json',
        contentType:"application/json;charset=utf-8",
        data:JSON.stringify(data),
        async:true,
        success:function(data){
        	if(data.code!='10000'){
        		parent.layer.alert(data.msg, {icon: 1});
        		return;
        	}
        	Layout.reloadAjaxContent('/jump_success?url=/system/staff');
        	return;
        },
        error: function (response, ajaxOptions, thrownError) {
        	errorCallBack(response, ajaxOptions, thrownError);                
        }
    });
}