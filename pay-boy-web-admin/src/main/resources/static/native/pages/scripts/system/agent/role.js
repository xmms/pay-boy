define([
	'jquery',
	'jquery-validation',
	'jquery-extension',
	'layui',
	'jquery-jstree',
	'bootstrap-table',
	'bootstrap-table-zh-CN',
	'bootstrap-table-mobile',
], function() {
	return {
		initUpdateDialog: function(){
			// 表单验证
			$("#agent_role_update_form").validate({
				rules: {
				},
				messages:{
				},
			    errorPlacement: function(error,element) {
			    	$(element).parent().updateClass('has-error');
			        $(element).after(error);
			    },
				success:function(element) {
			        $(element).parent().removeClass('has-error');
			        $(element).parent().find('label').remove();
			    },
			    // 是否在提交表单时验证
				onsubmit: false,
				// 是否在获取焦点时验证
				onfocusout:false,
				// 是否在敲击键盘时验证
				onkeyup:false,
				// 是否在点击时验证
				onclick: false,
				// 提交表单后，（第一个）未通过验证的表单获得焦点
				focusInvalid:true,
				// 当未通过验证的元素获得焦点时，移除错误提示
				focusCleanup:false,
			});
			// 初始化角色资源树
			$("#agent_role_tree").jstree("destroy");
			var roleId = $("input[name='id']").val();
			$.ajax({
				url: "/system/agent/role/role_permission_tree",
				type: 'post',
				dataType: 'json',
				data: {roleId:roleId},
				success: function (res) {
					if(res.code!='10000'){
						return;
					}
					// 渲染jstree
					$("#agent_role_tree").on('redraw.jstree', function (e) {
						$(".layer-footer").attr("domrefresh", Math.random());
					}).jstree({
						"themes": {"stripes": true},
						"checkbox": {
							"keep_selected_style": false,
						},
						"plugins": ["checkbox"],
						"core": {
							'check_callback': true,
							"data": res.data
						}
					});
				},
		        error: function (response, ajaxOptions, thrownError) {
		        	errorCallBack(response, ajaxOptions, thrownError);                
		        }
			});
		},
		initAddDialog: function(){
			// 表单验证
			$("#agent_role_add_form").validate({
				rules: {
				},
				messages:{
				},
			    errorPlacement: function(error,element) {
			    	$(element).parent().addClass('has-error');
			        $(element).after(error);
			    },
				success:function(element) {
			        $(element).parent().removeClass('has-error');
			        $(element).parent().find('label').remove();
			    }
			});
			// 初始化角色资源树
			$("#agent_role_tree").jstree("destroy");
			$.ajax({
				url: "/system/agent/role/role_permission_tree",
				type: 'post',
				dataType: 'json',
				data: {},
				success: function (res) {
					if(res.code!='10000'){
						return;
					}
					// 渲染jstree
					$("#agent_role_tree").on('redraw.jstree', function (e) {
						$(".layer-footer").attr("domrefresh", Math.random());
					}).jstree({
						"themes": {"stripes": true},
						"checkbox": {
							"keep_selected_style": false,
						},
						"types": {
							/*"root": {
								"icon": "fa fa-folder-open",
							},
							"menu": {
								"icon": "fa fa-folder-open",
							},
							"file": {
								"icon": "fa fa-file-o",
							}*/
						},
						"plugins": ["checkbox", "types"],
						"core": {
							'check_callback': true,
							"data": res.data
						}
					});
				},
		        error: function (response, ajaxOptions, thrownError) {
		        	errorCallBack(response, ajaxOptions, thrownError);                
		        },
			    // 是否在提交表单时验证
				onsubmit: false,
				// 是否在获取焦点时验证
				onfocusout:false,
				// 是否在敲击键盘时验证
				onkeyup:false,
				// 是否在点击时验证
				onclick: false,
				// 提交表单后，（第一个）未通过验证的表单获得焦点
				focusInvalid:true,
				// 当未通过验证的元素获得焦点时，移除错误提示
				focusCleanup:false,
			});
		},
		initPage: function(){
			// 1 初始化表单控件
			$('#agent_role_datatable').bootstrapTable({
				url : "/system/agent/role/query", 
				method : 'post', 
				toolbar : '#agent_role_toolbar',
				uniqueId: "id",
				striped: true,
				cache: false,
				pagination : true, 
				sortable: false,
				singleSelect : false,
				search: true,
				showColumns: true,
				showToggle:false,
				cardView: false,
				showRefresh: true,
				clickToSelect: true,
				minimumCountColumns: 5,
				queryParams : function(params) {
					return {
						limit : params.limit,
						offset : params.offset,
						keywords: params.search,
						sortName:'sort'
					};
				},
				sidePagination : "server",
				pageNumber:1, 
				pageSize : 10,
				pageList: [10, 20, 30],
				columns : [
					{
						checkbox : true
					},
					{
						field : 'id',
						title : '编号'
					},
					{
						field : 'name',
						title : '角色名称'
					},
					{
						field : 'desc',
						title : '描述',
					},
					{
						field : 'sort',
						title : '排序',
					},
					{
						field : 'status',
						title : '状态',
						formatter: function(value, row, index){
							if(value==0){
								return '<span class="text-danger"><i class="fa fa-circle"></i> 禁用</span>';
							}else{
								return '<span class="text-success"><i class="fa fa-circle"></i> 启用</span>';
							}
						}
					},
					{
						field : 'createTime',
						width : '150px',
						title : '创建时间'
					},
					{
						field : 'updateTime',
						width : '150px',
						title : '最后修改时间'
					},
					{
						title : '操作',
						field : '',
						align : 'center',
						formatter : function(value, row, index) {
							var b1 = '<a href="javascript:void(0);" class="btn btn-xs btn-success btn-editone" onclick="agent_role_update('+row['id']+')"><i class="fa fa-pencil"></i></a>';
							var b2 = '<a href="javascript:;" class="btn btn-xs btn-success btn-delone" onclick="agent_role_delete('+row['id']+')"><i class="fa fa-trash"></i></a>';
							return b1 + b2;
						}
					} 
				]
			});
			
			// 初始化手机响应式
			var docW = window.innerWidth;
			if (docW < 768) {
				$("#agent_role_search_plus").show();
				$("#agent_role_search_plus_btn").hide();
				$("#agent_role_content .bootstrap-table .fixed-table-toolbar input[type='text']").hide();
			}else{
				$("#agent_role_search_plus").hide();
			}
		}
	}
});

// 添加角色
function agent_role_add() {
	var docW = window.innerWidth;
	var area = [ '100%', '100%' ];
	var maxmin = false;
	if (docW >= 768) {
		area = [ '800px', '600px' ];
		maxmin = true;
	}
	var i = layer.open({
		type : 2,
		title : '添加角色',
		maxmin : maxmin,
		shadeClose : false,
		area : area,
		content : '/system/agent/role/add',
		btn : [ '保存+btn-success+fa-check', '取消+btn-success+fa-close' ],
		btn1 : function(index) {
			var loadindex = layer.load(1, {time: 5*1000});	
			$("#layui-layer-iframe" + i)[0].contentWindow.add_role();
			layer.close(loadindex);
		},
		btn2 : function(index) {
			layer.close(i);
		}
	});
	if (docW >= 768) {
		layer.restore(i);
	}else{
		layer.full(i);
	}
}

//删除角色
function agent_role_delete(id){
	layer.confirm('确定要删除吗？', {
		btn : [ '确定', '取消' ]
	}, function() {
		$.ajax({
			url : '/system/agent/role/delete',
			type : "post",
			data : {'id' : id},
			success : function(data) {
				if (data.code != '10000') {
					layer.alert(data.msg,{icon: 2});
					return;
				}
				layer.alert(data.msg, {icon: 1});
				agent_role_reload();
			},
	        error: function (response, ajaxOptions, thrownError) {
	        	errorCallBack(response, ajaxOptions, thrownError);                
	        }
		});
	})
}

// 批量删除
function agent_role_batch_delete(){
	var rows = $('#agent_role_datatable').bootstrapTable('getSelections'); 
	if (rows.length == 0) {
		layer.alert("请至少选择一条记录!",{icon: 7});
		return;
	}
	layer.confirm("确定要删除选中的'" + rows.length + "'条记录吗?", {
		btn : [ '确定', '取消' ]
	}, function() {
		var ids = [];
		$.each(rows, function(i, row) {
			ids.push(row['id']);
		});
		$.ajax({
			type : 'post',
			data : {ids:ids},
			dataType : 'json',
			url : '/system/agent/role/batch_delete',
			success : function(data) {
				if (data.code != '10000') {
					layer.alert(data.msg,{icon: 2});
					return;
				}
				layer.alert(data.msg, {icon: 1});
				agent_role_reload();
			},
	        error: function (response, ajaxOptions, thrownError) {
	        	errorCallBack(response, ajaxOptions, thrownError);                
	        }
		});
	}, function() {});
}

// 修改角色
function agent_role_update(id){
	var docW = window.innerWidth;
	var area = [ '100%', '100%' ];
	var maxmin = false;
	if (docW >= 768) {
		area = [ '800px', '600px' ];
		maxmin = true;
	}
	var i = layer.open({
		type : 2,
		title : '修改角色',
		maxmin : maxmin,
		shadeClose : false,
		area : area,
		content : '/system/agent/role/update?id='+id,
		btn : [ '保存+btn-success+fa-check', '取消+btn-success+fa-close' ],
		btn1 : function(index) {
			var loadindex = layer.load(1, {time: 5*1000});	
			$("#layui-layer-iframe" + i)[0].contentWindow.agent_update_role();
			layer.close(loadindex);
		},
		btn2 : function(index) {
			layer.close(i);
		}
	});
	if (docW >= 768) {
		layer.restore(i);
	}else{
		layer.full(i);
	}
}

// 重新加载datatable
function agent_role_reload(){
	$('#agent_role_datatable').bootstrapTable('refresh', null);
}

//保存地区
function add_role(callback){
	// Step1: 表单验证
	var valid = $("#agent_role_add_form").valid();
	if(!valid){
		return;
	}
	
	// Step2: 获取输入数据，拼装成请求数据
	var data = $('#agent_role_add_form').serializeJson();
	var arr =  $("#agent_role_tree").jstree(true).get_selected();
	data['resourceIds'] = arr;
	
	// Step3: 提交表单
    $.ajax({
    	url:"/system/agent/role/add",
        type:"post",
        dataType:'json',
        contentType:"application/json;charset=utf-8",
        data:JSON.stringify(data),
        async:false,
        success:function(data){
        	if(data.code!='10000'){
        		parent.layer.alert(data.msg, {icon: 2});
        		var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
    			parent.layer.close(index);
        		return;
        	}
        	parent.layer.alert(data.msg, {icon: 1});
        	parent.agent_role_reload();
        	var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
			parent.layer.close(index);
        },
        error: function (response, ajaxOptions, thrownError) {
        	errorCallBack(response, ajaxOptions, thrownError);                
        }
    });
}

//保存角色
function agent_update_role(callback){
	// Step1: 表单验证
	var valid = $("#agent_role_update_form").valid();
	if(!valid){
		return;
	}
	
	// Step2: 获取输入数据，拼装成请求数据
	var data = $('#agent_role_update_form').serializeJson();
	var arr =  $("#agent_role_tree").jstree(true).get_selected();
	data['resourceIds'] = arr;
	
	// Step3: 提交表单
    $.ajax({
    	url:"/system/agent/role/update",
        type:"post",
        dataType:'json',
        contentType:"application/json;charset=utf-8",
        data:JSON.stringify(data),
        async:false,
        success:function(data){
        	if(data.code!='10000'){
        		parent.layer.alert(data.msg, {icon: 2});
        		var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
    			parent.layer.close(index);
        		return;
        	}
        	parent.layer.alert(data.msg, {icon: 1});
        	parent.agent_role_reload();
        	var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
			parent.layer.close(index);
        },
        error: function (response, ajaxOptions, thrownError) {
        	errorCallBack(response, ajaxOptions, thrownError);                
        }
    });
}