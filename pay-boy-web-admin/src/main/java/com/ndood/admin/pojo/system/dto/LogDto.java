package com.ndood.admin.pojo.system.dto;

import java.util.Date;

import com.ndood.admin.pojo.system.LogPo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 日志DTO类
 */
@Getter @Setter
@EqualsAndHashCode(callSuper=false)
public class LogDto extends LogPo {
	private static final long serialVersionUID = 2000231857406108177L;

	public LogDto() {
		super();
	}
	
	public LogDto(String recordNo, String ip, String ipInfo, String requestType, String requestUrl, String requestParam,
			Integer staffId, String staffName, Date createTime, Date updateTime) {
		super();
		super.setRecordNo(recordNo);
		super.setIp(ip);
		super.setIp(ipInfo);
		super.setRequestType(requestType);
		super.setRequestUrl(requestUrl);
		super.setRequestParam(requestParam);
		super.setStaffId(staffId);
		super.setStaffName(staffName);
		super.setCreateTime(createTime);
		super.setUpdateTime(updateTime);
	}
	
}
