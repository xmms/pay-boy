package com.ndood.admin.pojo.system.agent.dto;

import java.io.Serializable;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

/**
 * 权限DTO类
 */
@Getter @Setter
public class AgentTreeDto implements Serializable {
	private static final long serialVersionUID = 6650285921381454775L;
	private Integer id;
	private String parent;
	private String text;
	private String type;
	private Map<String, Object> state;
}
