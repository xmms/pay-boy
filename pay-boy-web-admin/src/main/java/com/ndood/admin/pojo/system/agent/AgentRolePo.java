package com.ndood.admin.pojo.system.agent;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

/**
 * 角色bean
 * @author ndood
 */
@Entity
@Table(name="a_role")
@Cacheable(true)
@Getter @Setter
@JsonIgnoreProperties(value={"handler","fieldHandler"})
public class AgentRolePo implements Serializable{
	private static final long serialVersionUID = 7148771564181945122L;

	/**
	 * ID
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	/**
	 * 名称
	 */
	@Column(length = 32, nullable = false, unique = true)
	private String name;
	
	/**
	 * 描述
	 */
	@Column(name="`desc`",length = 32)
	private String desc;

	/**
	 * 删除标记
	 */
	@Column(nullable = false)
	private Integer status;
	
	/**
	 * 排序
	 */
	private Integer sort;
	
	/**
	 * 创建时间
	 */
	@Column(name="create_time", nullable = false, columnDefinition="datetime", updatable=false)
	private Date createTime;
	
	/**
	 * 修改时间
	 */
	@Column(name="update_time", nullable = false, columnDefinition="datetime")
	private Date updateTime;
	
	/**
	 * 角色拥有的资源
	 */
	@ManyToMany
	@JoinTable(name = "a_role_perm", joinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"), 
	inverseJoinColumns = @JoinColumn(name = "perm_id", referencedColumnName = "id"))
	private List<AgentPermPo> perms;

}
