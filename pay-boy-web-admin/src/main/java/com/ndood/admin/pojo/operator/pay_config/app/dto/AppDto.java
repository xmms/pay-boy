package com.ndood.admin.pojo.operator.pay_config.app.dto;

import java.util.Date;

import com.ndood.admin.pojo.operator.pay_config.app.AppPo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 应用DTO类
 */
@Getter @Setter
@EqualsAndHashCode(callSuper=false)
public class AppDto extends AppPo {
	private static final long serialVersionUID = 6650285921381454775L;
	
	public AppDto(Integer id, String appNo, String name, Integer limit, Integer status, Integer sort,
			Date createTime, Date updateTime) {
		super();
		super.setId(id);
		super.setAppNo(appNo);
		super.setName(name);
		super.setLimit(limit);
		super.setStatus(status);
		super.setSort(sort);
		super.setCreateTime(createTime);
		super.setUpdateTime(updateTime);
	}

	public AppDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
