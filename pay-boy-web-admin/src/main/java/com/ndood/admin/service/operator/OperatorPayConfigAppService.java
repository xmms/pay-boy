package com.ndood.admin.service.operator;

import com.ndood.admin.pojo.comm.dto.DataTableDto;
import com.ndood.admin.pojo.operator.pay_config.app.AppPo;
import com.ndood.admin.pojo.operator.pay_config.app.dto.AppDto;
import com.ndood.admin.pojo.operator.pay_config.app.query.AppQuery;

/**
 * 运营子系统 支付设置 app
 */
public interface OperatorPayConfigAppService {

	/**
	 * 添加app
	 */
	void addApp(AppDto dto) throws Exception;

	/**
	 * 批量删除
	 */
	void batchDeleteApp(Integer[] ids);

	/**
	 * 更新
	 */
	void updateApp(AppDto dto) throws Exception;

	/**
	 * 获取app
	 */
	AppPo getApp(Integer id);

	/**
	 * 查询app列表
	 */
	DataTableDto pageAppList(AppQuery query) throws Exception;

	
}
