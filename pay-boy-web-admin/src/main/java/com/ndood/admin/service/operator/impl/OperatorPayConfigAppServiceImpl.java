package com.ndood.admin.service.operator.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.ndood.admin.pojo.comm.dto.DataTableDto;
import com.ndood.admin.pojo.operator.pay_config.app.AppPo;
import com.ndood.admin.pojo.operator.pay_config.app.dto.AppDto;
import com.ndood.admin.pojo.operator.pay_config.app.query.AppQuery;
import com.ndood.admin.repository.operator.pay_config.app.AppRepository;
import com.ndood.admin.repository.operator.pay_config.app.manager.AppRepositoryManager;
import com.ndood.admin.service.operator.OperatorPayConfigAppService;
import com.ndood.common.base.util.JPAUtil;

@Service
public class OperatorPayConfigAppServiceImpl implements OperatorPayConfigAppService{

	@Autowired
	private AppRepository appDao;
	
	@Autowired
	private AppRepositoryManager appRepositoryManager;
	
	@Override
	public void addApp(AppDto dto) throws Exception {
		// Step1: 补充相关默认字段
		dto.setCreateTime(new Date());
		dto.setUpdateTime(new Date());
		
		// Step2: 保存角色
		AppPo po = new AppPo();
		JPAUtil.childToFather(dto, po);
		// 设置复杂属性
		appDao.save(po);
	}

	@Override
	public void batchDeleteApp(Integer[] ids) {
		// appDao.deleteByIdByIds(Arrays.asList(ids));
		for (Integer id : ids) {
			appDao.deleteById(id);
		}
	}

	@Override
	public void updateApp(AppDto dto) throws Exception {
		// Step1: 删除旧角色值
		AppPo po = appDao.findById(dto.getId()).get();
		
		// Step2: 保存新的值
		JPAUtil.childToFather(dto, po);
		// 设置特殊属性
		appDao.save(po);
	}
	
	@Override
	public AppPo getApp(Integer id) {
		return appDao.findById(id).get();
	}

	@Override
	public DataTableDto pageAppList(AppQuery query) throws Exception {
		// Step1: 封装查询参数
		Integer pageSize = query.getLimit();
		String keywords = query.getKeywords();
		Integer pageNo = query.getPageNo();
		
		// Step2: 进行复杂查询
		Page<AppDto> page = appRepositoryManager.pageAppList(keywords, pageNo, pageSize);
		return new DataTableDto(page.getContent(), page.getTotalElements());
	}

}
