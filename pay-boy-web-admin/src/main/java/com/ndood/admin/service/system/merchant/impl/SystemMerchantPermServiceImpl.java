package com.ndood.admin.service.system.merchant.impl;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ndood.admin.pojo.system.merchant.MerchantPermPo;
import com.ndood.admin.pojo.system.merchant.dto.MerchantPermDto;
import com.ndood.admin.repository.system.merchant.MerchantPermRepository;
import com.ndood.admin.repository.system.merchant.manager.MerchantPermRepositoryManager;
import com.ndood.admin.service.system.merchant.SystemMerchantPermService;
import com.ndood.common.base.util.JPAUtil;

/**
 * 资源模块业务类
 * @author ndood
 */
@Transactional
@Service
public class SystemMerchantPermServiceImpl implements SystemMerchantPermService {
	
	@Autowired
	private MerchantPermRepository agemtPermDao;

	@Autowired
	private MerchantPermRepositoryManager merchantPermRepositoryManager;
	
	@Override
	public List<MerchantPermDto> getPermList() throws Exception {
		return merchantPermRepositoryManager.getPermList();
	}

	@Override
	public void batchDeletePerm(Integer[] ids) {
		for (Integer id : ids) {
			agemtPermDao.deleteById(id);
		}
	}

	@Override
	public MerchantPermDto addPerm(MerchantPermDto dto) throws Exception {
		// Step1: 补充相关字段
		dto.setCreateTime(new Date());
		dto.setUpdateTime(new Date());
		MerchantPermPo po = new MerchantPermPo();
		
		// Step2: 处理上级资源
		Integer parentId = dto.getParentId();
		if (parentId != null) {
			MerchantPermPo parent = agemtPermDao.findById(parentId).get();
			po.setParent(parent);
		}

		// Step3: 保存并返回
		JPAUtil.childToFather(dto, po);
		po = agemtPermDao.save(po);
		dto.setId(po.getId());
		return dto;
	}

	@Override
	public MerchantPermDto getPerm(Integer id) throws Exception {
		// Step1: 获取资源信息
		MerchantPermDto dto = new MerchantPermDto();
		MerchantPermPo po = agemtPermDao.findById(id).get();
		JPAUtil.fatherToChild(po, dto);
		dto.setParent(po.getParent());
		return dto;
	}

	@Override
	public MerchantPermDto updatePerm(MerchantPermDto dto) throws Exception {
		// Step1: 更新资源信息
		dto.setUpdateTime(new Date());
		MerchantPermPo po = agemtPermDao.findById(dto.getId()).get();
		JPAUtil.childToFather(dto, po);
		agemtPermDao.save(po);
		
		// Step2: 获取隐含属性用于异步回显
		JPAUtil.fatherToChild(po, dto);
		return dto;
	}
}
