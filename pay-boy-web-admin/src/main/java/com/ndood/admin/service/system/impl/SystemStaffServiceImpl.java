package com.ndood.admin.service.system.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.ndood.admin.pojo.comm.dto.DataTableDto;
import com.ndood.admin.pojo.system.RolePo;
import com.ndood.admin.pojo.system.StaffPo;
import com.ndood.admin.pojo.system.dto.StaffDto;
import com.ndood.admin.pojo.system.query.StaffQuery;
import com.ndood.admin.repository.system.StaffRepository;
import com.ndood.admin.repository.system.manager.StaffRepositoryManager;
import com.ndood.admin.service.system.SystemStaffService;
import com.ndood.common.base.util.JPAUtil;

/**
 * 用户模块业务类
 * @author ndood
 */
@Service
public class SystemStaffServiceImpl implements SystemStaffService{
	
	@Autowired
	private StaffRepository staffDao;
	
	@Autowired
	private StaffRepositoryManager staffRepositoryManager;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Override
	public String addStaff(StaffDto dto) throws Exception {
		// Step1: 补充相关默认字段
		dto.setCreateTime(new Date());
		dto.setUpdateTime(new Date());
		if(dto.getPassword()==null) {
			dto.setPassword(passwordEncoder.encode("123456"));
		}else {
			dto.setPassword(passwordEncoder.encode(dto.getPassword()));
		}

		StaffPo po = new StaffPo();
		if(dto.getRoleIds()!=null) {
			List<RolePo> roles = new ArrayList<RolePo>();
			for (Integer roleId : dto.getRoleIds()) {
				RolePo role = new RolePo();
				role.setId(roleId);
				roles.add(role);
			}
			po.setRoles(roles);
		}
		
		JPAUtil.childToFather(dto, po);
		
		// Step2: 保存用户信息
		StaffPo staffPo = staffDao.save(po);
		return String.valueOf(staffPo.getId());
	}

	@Override
	public void batchDeleteStaff(Integer[] ids) {
		// staffDao.deleteByIdByIds(Arrays.asList(ids));
		for (Integer id : ids) {
			staffDao.deleteById(id);
		}
	}

	@Override
	public void updateStaff(StaffDto dto) throws Exception {
		// Step1: 判断是否更新密码
		StaffPo po = staffDao.findById(dto.getId()).get();
		String newPassword = dto.getPassword();
		if(!StringUtils.isEmpty(newPassword)){
			if(!passwordEncoder.matches(po.getPassword(), newPassword)){
				dto.setPassword(passwordEncoder.encode(newPassword));
			}
		}
		dto.setUpdateTime(new Date());
		JPAUtil.childToFather(dto, po);
		
		List<RolePo> roles = new ArrayList<RolePo>();
		for (Integer roleId : dto.getRoleIds()) {
			RolePo role = new RolePo();
			role.setId(roleId);
			roles.add(role);
		}
		po.setRoles(roles);
		
		// Step2: 更新时间
		staffDao.save(po);
	}
	
	@Override
	public StaffDto getStaff(Integer id) throws Exception {
		// Step1: 获取用户信息时不能暴露密码
		StaffPo po = staffDao.findById(id).get();
		po.setPassword(null);
		
		StaffDto dto = new StaffDto();
		JPAUtil.fatherToChild(po, dto);
		
		dto.setRoles(po.getRoles());
		return dto;
	}

	/**
	 * 分页查询用户信息，采用jpa的dto形式
	 */
	public DataTableDto pageStaffList(StaffQuery query) throws Exception{
		Page<StaffDto> page = staffRepositoryManager.pageStaffList(query);
		return new DataTableDto(page.getContent(), page.getTotalElements());
	}

}