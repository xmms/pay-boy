package com.ndood.admin.service.system.agent;

import java.util.List;

import com.ndood.admin.pojo.comm.dto.DataTableDto;
import com.ndood.admin.pojo.system.agent.AgentRolePo;
import com.ndood.admin.pojo.system.agent.dto.AgentRoleDto;
import com.ndood.admin.pojo.system.agent.dto.AgentTreeDto;
import com.ndood.admin.pojo.system.agent.query.AgentRoleQuery;

/**
 * 角色管理业接口
 * @author ndood
 */
public interface SystemAgentRoleService {

	/**
	 * 角色列表
	 * @param query
	 * @return
	 * @throws Exception 
	 */
	DataTableDto pageRoleList(AgentRoleQuery query) throws Exception;

	/**
	 * 添加角色
	 * @param role
	 * @throws Exception 
	 */
	void addRole(AgentRoleDto role) throws Exception;

	/**
	 * 批量删除角色
	 * @param ids
	 */
	void batchDeleteRole(Integer[] ids);

	/**
	 * 更新角色
	 * @param role
	 * @throws Exception 
	 */
	void updateRole(AgentRoleDto dto) throws Exception;

	/**
	 * 获取单个角色
	 * @param id
	 * @return
	 */
	AgentRolePo getRole(Integer id);

	/**
	 * 获取角色资源树
	 * @param id
	 * @param pid
	 * @return
	 */
	List<AgentTreeDto> getPermissionTree(Integer roleId);

	/**
	 * 获取所有角色
	 * @throws Exception 
	 */
	List<AgentRoleDto> getRoles() throws Exception;

}
