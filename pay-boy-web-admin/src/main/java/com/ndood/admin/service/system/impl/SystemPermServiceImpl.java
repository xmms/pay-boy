package com.ndood.admin.service.system.impl;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ndood.admin.pojo.system.PermPo;
import com.ndood.admin.pojo.system.dto.PermDto;
import com.ndood.admin.repository.system.PermRepository;
import com.ndood.admin.repository.system.manager.PermRepositoryManager;
import com.ndood.admin.service.system.SystemPermService;
import com.ndood.common.base.util.JPAUtil;

/**
 * 资源模块业务类
 * @author ndood
 */
@Transactional
@Service
public class SystemPermServiceImpl implements SystemPermService {
	
	@Autowired
	private PermRepository permDao;

	@Autowired
	private PermRepositoryManager permRepositoryManager;
	
	@Override
	public List<PermDto> getPermList() throws Exception {
		return permRepositoryManager.getPermList();
	}

	@Override
	public void batchDeletePerm(Integer[] ids) {
		for (Integer id : ids) {
			permDao.deleteById(id);
		}
	}

	@Override
	public PermDto addPerm(PermDto dto) throws Exception {
		// Step1: 补充相关字段
		dto.setCreateTime(new Date());
		dto.setUpdateTime(new Date());
		PermPo po = new PermPo();
		
		// Step2: 处理上级资源
		Integer parentId = dto.getParentId();
		if (parentId != null) {
			PermPo parent = permDao.findById(parentId).get();
			po.setParent(parent);
		}

		// Step3: 保存并返回
		JPAUtil.childToFather(dto, po);
		po = permDao.save(po);
		dto.setId(po.getId());
		return dto;
	}

	@Override
	public PermDto getPerm(Integer id) throws Exception {
		// Step1: 获取资源信息
		PermDto dto = new PermDto();
		PermPo po = permDao.findById(id).get();
		JPAUtil.fatherToChild(po, dto);
		dto.setParent(po.getParent());
		return dto;
	}

	@Override
	public PermDto updatePerm(PermDto dto) throws Exception {
		// Step1: 更新资源信息
		dto.setUpdateTime(new Date());
		PermPo po = permDao.findById(dto.getId()).get();
		JPAUtil.childToFather(dto, po);
		permDao.save(po);
		
		// Step2: 获取隐含属性用于异步回显
		JPAUtil.fatherToChild(po, dto);
		return dto;
	}
}
