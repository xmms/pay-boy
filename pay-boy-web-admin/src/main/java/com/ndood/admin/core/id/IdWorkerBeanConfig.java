package com.ndood.admin.core.id;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ndood.admin.core.properties.AdminProperties;
 
/**
 * @author : zhangxin
 * @version : V1.0
 * @since : 2018/09/15
 */
@Configuration
public class IdWorkerBeanConfig {
	
	@Autowired
	private AdminProperties adminProperties;
	
    /**
     * 配置全局id生成器
     */
    @Bean
    public SnowflakeIdWorker getIdWorker() {
        return new SnowflakeIdWorker(adminProperties.getId().getWorkspaceId(), adminProperties.getId().getMachineId());
    }
    
}
