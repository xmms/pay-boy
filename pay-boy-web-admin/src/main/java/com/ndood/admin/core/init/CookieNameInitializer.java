package com.ndood.admin.core.init;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.ndood.admin.core.properties.AdminProperties;

/**
 * 初始化的时候配置默认的sessionid名称
 */
@Component
public class CookieNameInitializer {
	
	@Autowired
	private AdminProperties adminProperties;
	
	@Bean
	public ServletContextInitializer servletContextInitializer() {
	    return new ServletContextInitializer() {
	        @Override
	        public void onStartup(ServletContext servletContext) throws ServletException {
	            servletContext.getSessionCookieConfig().setName(adminProperties.getDefaultCookieName());
	        }
	    };
	}
}

