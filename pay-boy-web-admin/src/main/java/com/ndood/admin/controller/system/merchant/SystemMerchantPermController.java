package com.ndood.admin.controller.system.merchant;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ndood.admin.pojo.comm.vo.AdminResultVo;
import com.ndood.admin.pojo.system.merchant.MerchantPermPo;
import com.ndood.admin.pojo.system.merchant.dto.MerchantPermDto;
import com.ndood.admin.service.system.merchant.SystemMerchantPermService;

/**
 * 资源控制器类
 * @author ndood
 */
@Controller
public class SystemMerchantPermController {
	
	@Autowired
	private SystemMerchantPermService systemMerchantPermService;

	/**
	 * 显示资源页
	 */
	@GetMapping("/system/merchant/perm")
	public String toPermPage(){
		return "system/merchant/perm/perm_page";
	}
	
	/**
	 * 显示添加对话框
	 */
	@GetMapping("/system/merchant/perm/add")
	public String toAddPerm(Integer parentId,String parentName, Model model){
		if(!StringUtils.isEmpty(parentId)&&!StringUtils.isEmpty(parentName)){
			model.addAttribute("parentId", parentId);
			model.addAttribute("parentName", parentName);
		}
		return "system/merchant/perm/perm_add";
	}
	
	/**
	 * 添加资源
	 */
	@PostMapping("/system/merchant/perm/add")
	@ResponseBody
	public AdminResultVo addPerm(@RequestBody MerchantPermDto perm) throws Exception{
		MerchantPermDto obj = systemMerchantPermService.addPerm(perm);
		return AdminResultVo.ok().setData(obj).setMsg("添加资源成功！");
	}
	
	/**
	 * 展示所有资源
	 */
	@GetMapping("/system/merchant/perm/treeview")
	@ResponseBody
	public List<MerchantPermDto> getPermTreeview() throws Exception{
		List<MerchantPermDto> permList = systemMerchantPermService.getPermList();
		return permList;
	}
	
	/**
	 * 删除资源
	 */
	@PostMapping("/system/merchant/perm/delete")
	@ResponseBody
	public AdminResultVo deletePerm(Integer id){
		Integer[] ids = new Integer[]{id};
		systemMerchantPermService.batchDeletePerm(ids);
		return AdminResultVo.ok().setMsg("删除资源成功！");
	}
	
	/**
	 * 批量删除资源
	 */
	@PostMapping("/system/merchant/perm/batch_delete")
	@ResponseBody
	public AdminResultVo batchDeletePerm(@RequestParam("ids[]") Integer[] ids){
		systemMerchantPermService.batchDeletePerm(ids);
		return AdminResultVo.ok().setMsg("批量删除资源成功！");
	}
	
	/**
	 * 显示修改对话框
	 */
	@GetMapping("/system/merchant/perm/update")
	public String toUpdatePerm(Integer id, Model model) throws Exception{
		MerchantPermPo perm = systemMerchantPermService.getPerm(id);
		model.addAttribute("perm", perm);
		return "system/merchant/perm/perm_update";
	}
	
	/**
	 * 修改资源
	 */
	@PostMapping("/system/merchant/perm/update")
	@ResponseBody
	public AdminResultVo updatePerm(@RequestBody MerchantPermDto perm) throws Exception{
		MerchantPermDto obj = systemMerchantPermService.updatePerm(perm);
		return AdminResultVo.ok().setData(obj).setMsg("修改资源成功！");
	}
}
