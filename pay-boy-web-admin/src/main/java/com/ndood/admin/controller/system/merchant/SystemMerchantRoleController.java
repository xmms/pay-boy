package com.ndood.admin.controller.system.merchant;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ndood.admin.pojo.comm.dto.DataTableDto;
import com.ndood.admin.pojo.comm.vo.AdminResultVo;
import com.ndood.admin.pojo.system.merchant.MerchantRolePo;
import com.ndood.admin.pojo.system.merchant.dto.MerchantRoleDto;
import com.ndood.admin.pojo.system.merchant.dto.MerchantTreeDto;
import com.ndood.admin.pojo.system.merchant.query.MerchantRoleQuery;
import com.ndood.admin.service.system.merchant.SystemMerchantRoleService;

/**
 * 角色控制器类
 * @author ndood
 */
@Controller
public class SystemMerchantRoleController {
	
	@Autowired
	private SystemMerchantRoleService systemMerchantRoleService;
	
	/**
	 * 显示角色页
	 */
	@GetMapping("/system/merchant/role")
	public String toRolePage(){
		return "system/merchant/role/role_page";
	}
	
	/**
	 * 显示添加对话框
	 */
	@GetMapping("/system/merchant/role/add")
	public String toAddRole(){
		return "system/merchant/role/role_add";
	}
	
	/**
	 * 添加角色
	 */
	@PostMapping("/system/merchant/role/add")
	@ResponseBody
	public AdminResultVo addRole(@RequestBody MerchantRoleDto role) throws Exception{
		systemMerchantRoleService.addRole(role);
		return AdminResultVo.ok().setMsg("添加角色成功！");
	}
	
	/**
	 * 删除角色
	 */
	@PostMapping("/system/merchant/role/delete")
	@ResponseBody
	public AdminResultVo deleteRole(Integer id){
		Integer[] ids = new Integer[]{id};
		systemMerchantRoleService.batchDeleteRole(ids);
		return AdminResultVo.ok().setMsg("删除角色成功！");
	}

	/**
	 * 添加角色
	 */
	@PostMapping("/system/merchant/role/batch_delete")
	@ResponseBody
	public AdminResultVo batchDeleteRole(@RequestParam("ids[]") Integer[] ids){
		systemMerchantRoleService.batchDeleteRole(ids);
		return AdminResultVo.ok().setMsg("批量删除角色成功！");
	}
	
	/**
	 * 显示修改对话框
	 */
	@GetMapping("/system/merchant/role/update")
	public String toUpdateRole(Integer id, Model model){
		MerchantRolePo role = systemMerchantRoleService.getRole(id);
		model.addAttribute("role", role);
		return "system/merchant/role/role_update";
	}
	
	/**
	 * 修改角色
	 * @throws Exception 
	 */
	@PostMapping("/system/merchant/role/update")
	@ResponseBody
	public AdminResultVo updateRole(@RequestBody MerchantRoleDto role) throws Exception{
		systemMerchantRoleService.updateRole(role);
		return AdminResultVo.ok().setMsg("修改角色成功！");
	}
	
	/**
	 * 角色列表
	 * @throws Exception 
	 */
	@PostMapping("/system/merchant/role/query")
	@ResponseBody
	public DataTableDto getDectListPage(@RequestBody MerchantRoleQuery query) throws Exception{
		DataTableDto page = systemMerchantRoleService.pageRoleList(query);
		return page;
	}
	
	/**
	 * 资源树
	 * @param roleId
	 * @return
	 */
	@PostMapping("/system/merchant/role/role_permission_tree")
	@ResponseBody
	public AdminResultVo getPermissionTree(Integer roleId){
		List<MerchantTreeDto> list = systemMerchantRoleService.getPermissionTree(roleId);
		return AdminResultVo.ok().setData(list).setMsg("获取角色权限树成功！");
	}
}
