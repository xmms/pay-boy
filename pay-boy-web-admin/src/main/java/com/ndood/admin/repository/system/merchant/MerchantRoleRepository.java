package com.ndood.admin.repository.system.merchant;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ndood.admin.pojo.system.merchant.MerchantRolePo;

/**
 * 角色dao
 */
public interface MerchantRoleRepository extends JpaRepository<MerchantRolePo, Integer>{

	Page<MerchantRolePo> findAll(Specification<MerchantRolePo> specification, Pageable pageable);

	List<MerchantRolePo> findByStatusEqualsOrderBySort(Integer status);
	
}
