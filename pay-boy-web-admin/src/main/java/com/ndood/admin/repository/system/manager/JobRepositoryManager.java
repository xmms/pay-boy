package com.ndood.admin.repository.system.manager;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import com.ndood.admin.core.jpa.entitymanager.EntityManagerModel;
import com.ndood.admin.pojo.system.dto.JobDto;
/**
 * 定时任务持久化管理类
 */
@Repository
@SuppressWarnings("unchecked")
public class JobRepositoryManager {
	
	@Autowired
	@PersistenceContext
	private EntityManager em;
	
	/**
	 * 查询出日志分页信息
	 */
	public Page<JobDto> pageJobList(String keywords, Date startTime, Date endTime, Integer pageNo, Integer pageSize) {
		// Step1: 动态拼接jpql
		EntityManagerModel model = EntityManagerModel.newModel();
		if(!StringUtils.isBlank(keywords)) {
			model.plusSQL("AND s.jobName LIKE :jobName ").plusParam("username", "%"+keywords+"%");
		}
		
		// Step2: 获取list
		Query query = em.createQuery("SELECT new com.ndood.admin.pojo.system.dto.JobDto(id, cronExpression, methodName, isConcurrent, description," + 
				" updateBy, beanClass, jobStatus, jobGroup, createBy, springBean, jobName, createTime, updateTime) "
				+ "FROM JobPo s "
				+ "WHERE 1 = 1 " + model.getSQLPlus()
				+ "ORDER BY s.createTime DESC ");
		
		model.setQueryParam(query);
		query.setFirstResult(pageNo * pageSize).setMaxResults(pageSize);
		query.setHint("org.hibernate.cacheable", true);
		List<JobDto> resultList = query.getResultList();
		
		// Step3: 获取count
		Query queryCount = em.createQuery("SELECT COUNT(0) "
				+ "FROM JobPo s "
				+ "WHERE 1 = 1 " + model.getSQLPlus());
		model.setQueryParam(queryCount);
		queryCount.setHint("org.hibernate.cacheable", true);
		Long count = (Long)queryCount.getSingleResult();
		
		// Step4: 封装page
		Page<JobDto> page = new PageImpl<>(resultList, PageRequest.of(pageNo, pageSize), count);
		return page;
	
	}
	
}