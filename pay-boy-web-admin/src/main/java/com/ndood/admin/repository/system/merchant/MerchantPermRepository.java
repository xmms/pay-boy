package com.ndood.admin.repository.system.merchant;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ndood.admin.pojo.system.merchant.MerchantPermPo;

/**
 * https://blog.jooq.org/tag/sqlresultsetmapping/
 */
public interface MerchantPermRepository extends JpaRepository<MerchantPermPo, Integer> {

	List<MerchantPermPo> findByOrderBySort();
	
	List<MerchantPermPo> findByParent(MerchantPermPo po);
}
