package com.ndood.admin.repository.system;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ndood.admin.pojo.system.PermPo;

/**
 * https://blog.jooq.org/tag/sqlresultsetmapping/
 */
public interface PermRepository extends JpaRepository<PermPo, Integer> {

	List<PermPo> findByOrderBySort();
	
	List<PermPo> findByParent(PermPo po);
}
