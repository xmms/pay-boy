package com.ndood.authenticate.core.properties;

/**
 * 添加短信验证码属性配置类
 * @author ndood
 */
public class SmsCodeProperties {

	private int length = 6;
	private int expireIn = 60;
	private String url = "";

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getExpireIn() {
		return expireIn;
	}

	public void setExpireIn(int expireIn) {
		this.expireIn = expireIn;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
