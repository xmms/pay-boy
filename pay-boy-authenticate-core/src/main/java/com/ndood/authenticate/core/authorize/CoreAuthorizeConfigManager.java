package com.ndood.authenticate.core.authorize;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.stereotype.Component;

/**
 * 实现AuthorizeConfigManager，作用是将系统里所有的authorizeConfigProvider收集起来
 */
@Component
@Order(Integer.MAX_VALUE)
public class CoreAuthorizeConfigManager implements AuthorizeConfigManager{

	@Autowired
	private List<AuthorizeConfigProvider> authorizeConfigProviders;
	
	/**
	 * 循环读取授权配置
	 */
	@Override
	public void config(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry config) {
		for (AuthorizeConfigProvider authorizeConfigProvider : authorizeConfigProviders) {
			authorizeConfigProvider.config(config);
		}
		// 其它所有的资源都必须认证
		// 注释掉该行，方式在rbac判断之前就通过
		// config.anyRequest().authenticated();
	}

}