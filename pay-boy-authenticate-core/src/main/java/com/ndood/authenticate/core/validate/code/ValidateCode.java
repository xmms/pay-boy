package com.ndood.authenticate.core.validate.code;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
/**
 * 创建短信验证码类
 * 序列化以支持redis持久化存储
 * @author 908304389@qq.com
 */
public class ValidateCode implements Serializable{
	
	private static final long serialVersionUID = -8469071750306217985L;
	private String code;
	private Date expireTime;

	public ValidateCode() {
		super();
	}

	/**
	 * 构造方法，过期时间点
	 */
	public ValidateCode(String code, int expireIn) {
		this.code = code;
		Date d = new Date();
		d.setTime(d.getTime() + expireIn * 1000);
		this.expireTime = d;
	}
	
	public ValidateCode(String code, Date expireTime) {
		this.code = code;
		this.expireTime = expireTime;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(Date expireTime) {
		this.expireTime = expireTime;
	}
	
	@JsonIgnore
	public boolean isExpired(){
		return new Date().getTime() > expireTime.getTime();
	}
}