package com.ndood.authenticate.core;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import com.ndood.authenticate.core.properties.SecurityProperties;

/**
 * 自定义属性配置入口 
 * 让SecurityProperties生效
 * @author ndood
 */
@Configuration
@EnableConfigurationProperties(SecurityProperties.class)
public class CoreConfig {
	
}
