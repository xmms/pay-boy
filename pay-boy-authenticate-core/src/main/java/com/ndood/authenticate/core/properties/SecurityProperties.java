package com.ndood.authenticate.core.properties;
/**
 * 自定义属性配置类SecurityProperties
 */
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * ndood配置主类
 * @author ndood
 */
@ConfigurationProperties(prefix = "ndood.security")
public class SecurityProperties {
	/**
	 * 未登陆跳转
	 */
	private String loginPage = "/authentication/require";
	/**
	 * 登陆入口
	 */
	private String loginProcessUrl = "/authentication/form";
	/**
	 * 验证码配置
	 */
	private ValidateCodeProperties code = new ValidateCodeProperties(); 
	
	/**
	 * 社交登录相关配置
	 */
	private SocialProperties social = new SocialProperties();
	
	/**
	 * 浏览器配置
	 */
	private BrowserProperties browser = new BrowserProperties();
	
	/**
	 * Oauth相关配置
	 */
	private Oauth2Properties oauth2 = new Oauth2Properties(); 

	public String getLoginPage() {
		return loginPage;
	}

	public void setLoginPage(String loginPage) {
		this.loginPage = loginPage;
	}

	public String getLoginProcessUrl() {
		return loginProcessUrl;
	}

	public void setLoginProcessUrl(String loginProcessUrl) {
		this.loginProcessUrl = loginProcessUrl;
	}

	public BrowserProperties getBrowser() {
		return browser;
	}

	public void setBrowser(BrowserProperties browser) {
		this.browser = browser;
	}

	public ValidateCodeProperties getCode() {
		return code;
	}

	public void setCode(ValidateCodeProperties code) {
		this.code = code;
	}

	public SocialProperties getSocial() {
		return social;
	}

	public void setSocial(SocialProperties social) {
		this.social = social;
	}

	public Oauth2Properties getOauth2() {
		return oauth2;
	}

	public void setOauth2(Oauth2Properties oauth2) {
		this.oauth2 = oauth2;
	}
}