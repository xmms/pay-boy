package com.ndood.authenticate.core.validate.code;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ndood.authenticate.core.properties.SecurityProperties;
import com.ndood.authenticate.core.validate.code.image.ImageCodeGenerator;
import com.ndood.authenticate.core.validate.code.sms.DefaultSmsCodeSender;
import com.ndood.authenticate.core.validate.code.sms.SmsCodeGenerator;
import com.ndood.authenticate.core.validate.code.sms.SmsCodeSender;

/**
 * 添加验证码config配置类
 * @author ndood
 */
@Configuration
public class ValidateCodeBeanConfig {

	@Autowired
	private SecurityProperties securityProperties;
	
	/**
	 * 配置默认imageCodeGenerator
	 * 当demo里imageCodeGenerator bean不存在时，才用这个配置
	 */
	@Bean
	@ConditionalOnMissingBean(name = "imageValidateCodeGenerator")
	public ValidateCodeGenerator imageValidateCodeGenerator(){
		ImageCodeGenerator codeGenerator = new ImageCodeGenerator();
		codeGenerator.setPaymentProperties(securityProperties);
		return codeGenerator;
	}
	
	/**
	 * 配置默认smsCodeGenerator
	 * 当smsCodeGenerator bean不存在时，才用这个配置
	 */
	@Bean
	@ConditionalOnMissingBean(name = "smsValidateCodeGenerator")
	public  ValidateCodeGenerator smsValidateCodeGenerator(){
		SmsCodeGenerator codeGenerator = new SmsCodeGenerator();
		codeGenerator.setPaymentProperties(securityProperties);
		return codeGenerator;
	}
	
	/**
	 * 配置默认smsCodeSender
	 * 当smsCodeSender bean不存在时，才用这个配置
	 */
	@Bean
	@ConditionalOnMissingBean(SmsCodeSender.class)
	public SmsCodeSender smsCodeSender(){
		return new DefaultSmsCodeSender();
	}
}