package com.ndood.authenticate.core.properties;

/**
 * 添加验证码属性配置类
 * @author ndood
 */
public class ImageCodeProperties {
	/**
	 * 验证码宽度
	 */
	private int width = 40;
	/**
	 * 验证码高度
	 */
	private int height = 23;
	/**
	 * 长度
	 */
	private int length = 4;
	/**
	 * 过期时间
	 */
	private int expireIn = 60;
	/**
	 * 生成验证码的url
	 */
	private String url = "";

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getLength() {
		length = 4;
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getExpireIn() {
		return expireIn;
	}

	public void setExpireIn(int expireIn) {
		this.expireIn = expireIn;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
