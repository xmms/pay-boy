package com.ndood.authenticate.app.validate.code;

import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletWebRequest;

import com.ndood.authenticate.core.validate.code.ValidateCode;
import com.ndood.authenticate.core.validate.code.ValidateCodeException;
import com.ndood.authenticate.core.validate.code.ValidateCodeRepository;
import com.ndood.authenticate.core.validate.code.ValidateCodeType;

/**
 * 基于redis的验证码存取器，解决手机端无法使用session的问题
 */
@Component
public class RedisValidateCodeRepository implements ValidateCodeRepository {

	@Autowired
	private RedisTemplate<Object, Object> redisTemplate;

	
	@Override
	public void save(ServletWebRequest request, ValidateCode code, ValidateCodeType type) {
		redisTemplate.opsForValue().set(buildKey(request, type), code, 30, TimeUnit.MINUTES);
	}

	@Override
	public ValidateCode get(ServletWebRequest request, ValidateCodeType type) {
		Object value = redisTemplate.opsForValue().get(buildKey(request, type));
		if (value == null) {
			return null;
		}
		return (ValidateCode) value;
	}

	@Override
	public void remove(ServletWebRequest request, ValidateCodeType type) {
		redisTemplate.delete(buildKey(request, type));
	}

	private String buildKey(ServletWebRequest request, ValidateCodeType type) {
		String userId = request.getHeader("userId");
		if (StringUtils.isBlank(userId)) {
			throw new ValidateCodeException("请在请求头中携带userId参数");
		}
		return "code:" + type.toString().toLowerCase() + ":" + userId;
	}
}