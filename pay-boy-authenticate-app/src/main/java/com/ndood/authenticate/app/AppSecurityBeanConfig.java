package com.ndood.authenticate.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.provider.expression.OAuth2WebSecurityExpressionHandler;

/**
 * Unsatisfied dependency expressed through field 'authenticationManager'; 
 * nested exception is org.springframework.beans.factory.NoSuchBeanDefinitionException: No qualifying bean of type 
 * 'org.springframework.security.authentication.AuthenticationManager' 
 * available: expected at least 1 bean which qualifies as autowire candidate. 
 * Dependency annotations: {@org.springframework.beans.factory.annotation.Autowired(required=true)}
 */
@Configuration
public class AppSecurityBeanConfig extends WebSecurityConfigurerAdapter {

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        AuthenticationManager manager = super.authenticationManagerBean();
        return manager;
    }
    
    /**
     * https://blog.csdn.net/liu_zhaoming/article/details/79411021
     * 解决oauth不支持expression handler问题
     */
    @Bean
    public OAuth2WebSecurityExpressionHandler oAuth2WebSecurityExpressionHandler(ApplicationContext applicationContext) {
        OAuth2WebSecurityExpressionHandler expressionHandler = new OAuth2WebSecurityExpressionHandler();
        expressionHandler.setApplicationContext(applicationContext);
        return expressionHandler;
    }

}