package com.ndood.reconciliation.app.redis;

import com.ndood.reconciliation.base.enums.EnumPayway;
import com.ndood.reconciliation.domain.recon.entity.dto.ReconInfoDto;

public class RedisReconKeyHelper {
	
	public static final String KEY_STORE_SNAPCHAT = "recon-snapchat";
	
	public static final String KEY_STORE = "recon";
	
	public static final String PERFIX_ALIPAY = "alipay-reconciliation";
	
	public static final String PERFIX_WECHAT = "wechat-reconciliation";
	
	public static final String PERFIX_UNION = "union-reconciliation";
	
	public static String alipayReconKey(String date) {
		return String.format("%s:%s", PERFIX_ALIPAY, date);
	}

	public static String wechatReconKey(String date) {
		return String.format("%s:%s", PERFIX_WECHAT, date);
	}
	
	public static String unionReconKey(String date) {
		return String.format("%s:%s", PERFIX_WECHAT, date);
	}

	/**
	 * 根据key获取对账记录
	 */
	public static ReconInfoDto getReconInfo(String reconKey) {
		ReconInfoDto reconInfo = new ReconInfoDto();
		EnumPayway payway;
		String date;
		if (reconKey.startsWith(PERFIX_ALIPAY)) {
			payway = EnumPayway.ALIPAY;
			date = reconKey.substring(PERFIX_ALIPAY.length());
		} else if (reconKey.startsWith(PERFIX_WECHAT)) {
			payway = EnumPayway.WECHAT;
			date = reconKey.substring(PERFIX_WECHAT.length());
		} else if (reconKey.startsWith(PERFIX_UNION)) {
			payway = EnumPayway.UNION;
			date = reconKey.substring(PERFIX_UNION.length());
		}
		else {
			payway = EnumPayway.INVALID;
			date = null;
		}
		reconInfo.setPayway(payway);
		reconInfo.setDate(date);
		return reconInfo;
	}
}