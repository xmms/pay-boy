package com.ndood.reconciliation.app.task;

import java.text.ParseException;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.ndood.common.base.util.DateUtil;
import com.ndood.reconciliation.app.redis.ReconRedisTool;
import com.ndood.reconciliation.app.redis.RedisReconKeyHelper;
import com.ndood.reconciliation.domain.recon.service.ReconCommonService;

@Component
public class ReconScheduleTask {

	@Autowired
	private ReconRedisTool reconRedisTool;
	
	@Autowired
	private ReconCommonService reconCommonService;
	
	@Scheduled(cron = "0 0 15 * * ?")
	public void taskRun() throws ParseException {
		String date = DateUtil.format(new Date(), "yyyyMMdd");
		
		// Step1 官方支付宝对账
		String alipayBatchNo = RedisReconKeyHelper.alipayReconKey(date);
		reconRedisTool.saddKey(alipayBatchNo);
			
		// Step2 官方微信对账
		String wechatBatchNo = RedisReconKeyHelper.wechatReconKey(date);
		reconRedisTool.saddKey(wechatBatchNo);
		
		// Step3 银联对账
		String unionBatchNo = RedisReconKeyHelper.unionReconKey(date);
		reconRedisTool.saddKey(unionBatchNo);
		
		// Step4 将缓冲池中超过3天的订单放入差错队列，然后并删除
		reconCommonService.processOvertimeMistakes();
	}
	

}
