package com.ndood.reconciliation.base.constaints;

public interface ReconConstaints {

	/**
	 * 对账批次状态
	 */
	Integer RECON_BATCH_STATUS_INIT = 1;
	Integer RECON_BATCH_STATUS_OK = 2;
	Integer RECON_BATCH_STATUS_FAILED = 3;
	
	/**
	 * 本地金额多
	 */
	String PLATFORM_OVER_CASH_MISMATCH = "001";
	String PLATFORM_SHORT_CASH_MISMATCH = "002";
}
