package com.ndood.reconciliation.base.dao.recon;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.ndood.reconciliation.domain.recon.entity.dto.ReconScratchDto;

public interface ExtendReconScratchDao {

	/**
	 * 1 根据时间区间和支付方式查询出所有缓冲池记录
	 */
	@Select("SELECT id, record_no, trade_order_no, order_no, agent_id, agent_name, mch_id, mch_name, pay_way, pay_type, contract_id, contract_type, contract_no, contract_name, product_no, product_name, "
			+ "channel_id, channel_no, channel_name, app_id, trade_type, trade_time, total_amount, receipt_amount, profit, create_time, update_time "
			+ "FROM r_mch_rec_recon_scratch_pool "
			+ "WHERE gmt_created >= #{startTime} AND gmt_created < #{endTime} AND pay_way= #{payWay} ")
	public List<ReconScratchDto> extendFindScratchListByTimeZoneAndPayway(@Param("startTime") Date startTime, @Param("endTime") Date endTime,
			@Param("payWay") Integer payWay);

	/**
	 * 2 查询缓冲池中超过3天的记录
	 */
	@Select("SELECT id, record_no, trade_order_no, order_no, agent_id, agent_name, mch_id, mch_name, pay_way, pay_type, contract_id, contract_type, contract_no, contract_name, product_no, product_name, "
			+ "channel_id, channel_no, channel_name, app_id, trade_type, trade_time, total_amount, receipt_amount, profit, create_time, update_time "
			+ "FROM r_mch_rec_recon_scratch_pool "
			+ "WHERE gmt_created < #{beforeDate} ")
	public List<ReconScratchDto> extendFindScratchListBeforeDate(@Param("beforeDate") Date beforeDate);


}
