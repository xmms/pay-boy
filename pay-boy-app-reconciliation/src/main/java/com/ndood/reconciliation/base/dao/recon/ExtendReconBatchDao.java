package com.ndood.reconciliation.base.dao.recon;


import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.ndood.reconciliation.domain.recon.entity.dto.ReconBatchDto;

public interface ExtendReconBatchDao {

	/**
	 * 根据日期，payway appid 查询出对账批次
	 */
	@Select("SELECT * FROM r_mch_rec_recon_batch WHERE date = #{date} AND pay_way = #{payWay} LIMIT 1 ")
	public ReconBatchDto extendFindReconBatchByDateAndPayWay(@Param("date") String date, @Param("payWay") Integer payWay);

}
