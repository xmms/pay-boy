package com.ndood.reconciliation.base.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CsvUtil {

	/**
	 * 解析订单流水数据
	 */
	public static List<HashMap<String, String>> parseOrderList(String filePath) {
		List<HashMap<String,String>> mapList = new ArrayList<>();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "GBK"));
			reader.read();// 第一行信息，为标题信息，不用,如果需要，注释掉
			String line = null;
			while ((line = reader.readLine()) != null) {
				String item[] = line.split("\r\n");// CSV格式文件为逗号分隔符文件，这里根据逗号切分
				String orderLine = item[item.length - 1];// 这就是你要的数据了
				if (orderLine.contains(")")) {
					String[] fields = orderLine.split(",");
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("payOrderNo", fields[1]);
					map.put("tradeType", "交易".equals(fields[2])?"1":"2");
					map.put("totalAmount", fields[11]);
					map.put("receiptAmount", fields[12]);
					map.put("profit", fields[22]);
					mapList.add(map);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
				}
			}
		}
		return mapList;

	}

	/**
	 * 解析合计数据
	 */
	public static Double parseTotalAmount(String type, String filePath) {
		Double money = null;
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "GBK"));
			reader.read();//
			String line = null;
			while ((line = reader.readLine()) != null) {
				String item[] = line.split("\r\n");
				String last = item[item.length - 1];
				if (last.contains("合计")) {
					String[] strs = last.split(",");
					money = Double.valueOf(strs[strs.length - 1]);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
				}
			}
		}
		return money;
	}

}
