package com.ndood.reconciliation.domain.recon.entity.dto;

import com.ndood.reconciliation.domain.recon.entity.ReconBatchDo;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ReconBatchDto extends ReconBatchDo {

}
