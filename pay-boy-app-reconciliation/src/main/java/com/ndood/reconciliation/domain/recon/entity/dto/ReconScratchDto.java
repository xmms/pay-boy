package com.ndood.reconciliation.domain.recon.entity.dto;

import com.ndood.reconciliation.domain.recon.entity.ReconScratchDo;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ReconScratchDto extends ReconScratchDo {

}
