package com.ndood.reconciliation.domain.recon.entity.dto;

import com.ndood.reconciliation.domain.recon.entity.ReconTradeRecordDo;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ReconTradeRecordDto extends ReconTradeRecordDo {

}
