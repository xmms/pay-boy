package com.ndood.reconciliation.domain.recon.entity;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ReconTradeRecordDo {
	
	/**
	 * 支付方式
	 */
	private Integer payWay;
	/**
	 * 商户ID
	 */
	private Integer mchId;
	/**
	 * 代理ID
	 */
	private Integer agentId;
	/**
	 * 订单编号
	 */
	private String orderNo;
	/**
	 * 支付订单号或者退款订单号
	 */
	private String tradeOrderNo;
	/**
	 * 交易类型
	 */
	private Integer tradeType;
	/**
	 * appid
	 */
	private String appId;
	/**
	 * 金额
	 */
	private BigDecimal amount;
	/**
	 * 实收支付金额，退款金额
	 */
	private BigDecimal realAmount;
	/**
	 * 交易时间，退款时间
	 */
	private String tradeTime;
	/**
	 * 交易状态： 支付和退款不同
	 */
	private Integer tradeStatus;
}
