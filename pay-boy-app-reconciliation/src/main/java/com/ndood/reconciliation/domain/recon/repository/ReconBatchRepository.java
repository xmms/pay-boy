package com.ndood.reconciliation.domain.recon.repository;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.ndood.common.base.dao.record.RMchRecReconBatchMapper;
import com.ndood.common.base.pojo.record.RMchRecReconBatchPo;
import com.ndood.reconciliation.domain.recon.entity.dto.ReconBatchDto;

@Repository
public class ReconBatchRepository {
	
	@Autowired
	private RMchRecReconBatchMapper rMchRecReconBatchMapper;

	public void insertReconBatch(ReconBatchDto dto) {
		RMchRecReconBatchPo po = new RMchRecReconBatchPo();
		po.setRemoteTradeAmount(dto.getRemoteTradeAmount());
		po.setRemoteRefundCount(dto.getRemoteRefundCount());
		po.setRemoteTradeCount(dto.getRemoteTradeCount());
		po.setUnhandleMistakeCount(dto.getUnhandleMistakeCount());
		po.setRemoteRefundAmount(dto.getRemoteRefundAmount());
		po.setOrgCheckFilePath(dto.getOrgCheckFilePath());
		po.setReleaseCheckFilePath(dto.getReleaseCheckFilePath());
		po.setBatchNo(dto.getBatchNo());
		po.setReconDay(dto.getReconDay());
		po.setReconType(dto.getReconType());
		po.setPayWay(dto.getPayWay());
		po.setMistakeCount(dto.getMistakeCount());
		po.setTradeCount(dto.getTradeCount());
		po.setTradeAmount(dto.getTradeAmount());
		po.setRefundCount(dto.getRefundCount());
		po.setErrorCode(dto.getErrorCode());
		po.setStatus(dto.getStatus());
		po.setReleaseStatus(dto.getReleaseStatus());
		po.setRefundAmount(dto.getRefundAmount());
		po.setErrorMsg(dto.getErrorMsg());
		po.setRemark(dto.getRemark());

		po.setCreateTime(new Date());
		po.setUpdateTime(new Date());
		rMchRecReconBatchMapper.insert(po);
	}
	
	public void updateReconBatch(ReconBatchDto dto) {
		RMchRecReconBatchPo po = new RMchRecReconBatchPo();
		po.setId(dto.getId());
		po.setRemoteTradeAmount(dto.getRemoteTradeAmount());
		po.setRemoteRefundCount(dto.getRemoteRefundCount());
		po.setRemoteTradeCount(dto.getRemoteTradeCount());
		po.setUnhandleMistakeCount(dto.getUnhandleMistakeCount());
		po.setRemoteRefundAmount(dto.getRemoteRefundAmount());
		po.setOrgCheckFilePath(dto.getOrgCheckFilePath());
		po.setReleaseCheckFilePath(dto.getReleaseCheckFilePath());
		po.setBatchNo(dto.getBatchNo());
		po.setReconDay(dto.getReconDay());
		po.setReconType(dto.getReconType());
		po.setPayWay(dto.getPayWay());
		po.setMistakeCount(dto.getMistakeCount());
		po.setTradeCount(dto.getTradeCount());
		po.setTradeAmount(dto.getTradeAmount());
		po.setRefundCount(dto.getRefundCount());
		po.setErrorCode(dto.getErrorCode());
		po.setStatus(dto.getStatus());
		po.setReleaseStatus(dto.getReleaseStatus());
		po.setRefundAmount(dto.getRefundAmount());
		po.setErrorMsg(dto.getErrorMsg());
		po.setRemark(dto.getRemark());
		
		po.setCreateTime(dto.getCreateTime());
		po.setUpdateTime(new Date());
		if(po.getId()!=null) {
			rMchRecReconBatchMapper.insert(po);
			return;
		}
		UpdateWrapper<RMchRecReconBatchPo> updateWrapper = new UpdateWrapper<>();
		updateWrapper.eq("batch_no", po.getBatchNo());
		rMchRecReconBatchMapper.update(po, updateWrapper);
	}


}
