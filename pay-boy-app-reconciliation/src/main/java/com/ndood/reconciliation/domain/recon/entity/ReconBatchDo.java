package com.ndood.reconciliation.domain.recon.entity;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ReconBatchDo {
    private Long id;

    /**
     * 对账批次号
     */
    private String batchNo;

    /**
     * 对账日
     */
    private String reconDay;

    /**
     * 对账类型：1 离线对账 2 在线对账
     */
    private Integer reconType;

    /**
     * 支付方式
     */
    private Integer payWay;

    /**
     * 错误数
     */
    private Integer mistakeCount;

    /**
     * 未处理错误数
     */
    private Integer unhandleMistakeCount;

    /**
     * 交易数量
     */
    private Integer tradeCount;

    /**
     * 交易金额
     */
    private BigDecimal tradeAmount;

    /**
     * 退款数量
     */
    private Integer refundCount;

    /**
     * 退款金额
     */
    private BigDecimal refundAmount;

    /**
     * 远程交易数量
     */
    private Integer remoteTradeCount;

    /**
     * 远程交易额
     */
    private BigDecimal remoteTradeAmount;

    /**
     * 远程退款数量
     */
    private Integer remoteRefundCount;

    /**
     * 远程退款金额
     */
    private BigDecimal remoteRefundAmount;

    /**
     * 对账单地址
     */
    private String orgCheckFilePath;

    /**
     * 对账文件存放地址
     */
    private String releaseCheckFilePath;

    /**
     * 文件解析状态： 1 待解析 2 已解析
     */
    private Integer releaseStatus;

    /**
     * 错误码
     */
    private String errorCode;

    /**
     * 错误描述
     */
    private String errorMsg;

    /**
     * 备注
     */
    private String remark;

    /**
     * 状态：1 待对账 2 已对账 3 对账失败
     */
    private Integer status;

    private Date createTime;

    private Date updateTime;
}
