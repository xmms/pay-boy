package com.ndood.reconciliation.domain.recon.entity;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ReconScratchDo {
    private Long id;

    private String recordNo;

    /**
     * 交易订单流水号
     */
    private String tradeOrderNo;

    /**
     * 订单流水号
     */
    private String orderNo;

    /**
     * 代理商ID
     */
    private Integer agentId;

    private String agentName;

    /**
     * 商户ID
     */
    private Integer mchId;

    private String mchName;

    /**
     * 支付方式
     */
    private Integer payWay;

    /**
     * 支付类型
     */
    private Integer payType;

    private Integer contractId;

    private Integer contractType;

    private String contractNo;

    private String contractName;

    private String productNo;

    private String productName;

    private Integer channelId;

    private String channelNo;

    private String channelName;

    private String appId;

    /**
     * 交易类型：1 支付 2 退款
     */
    private Integer tradeType;

    /**
     * 交易时间
     */
    private String tradeTime;

    /**
     * 交易金额
     */
    private BigDecimal totalAmount;

    /**
     * 实际交易金额
     */
    private BigDecimal receiptAmount;

    /**
     * 服务费
     */
    private BigDecimal profit;
}
