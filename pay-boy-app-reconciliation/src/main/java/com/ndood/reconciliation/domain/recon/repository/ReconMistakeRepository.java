package com.ndood.reconciliation.domain.recon.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ndood.common.base.dao.record.RMchRecReconMistakeMapper;
import com.ndood.common.base.pojo.record.RMchRecReconMistakePo;
import com.ndood.reconciliation.domain.recon.entity.dto.ReconMistakeDto;

@Repository
public class ReconMistakeRepository {

	@Autowired
	private RMchRecReconMistakeMapper rMchRecReconMistakeMapper;
	
	/**
	 * 1 添加差错记录
	 */
	public void addMistake(ReconMistakeDto dto) {
		RMchRecReconMistakePo po = new RMchRecReconMistakePo();
		po.setContractType(dto.getContractType());
		po.setTradeType(dto.getTradeType());
		po.setRecordNo(dto.getRecordNo());
		po.setChannelNo(dto.getChannelNo());
		po.setTradeOrderNo(dto.getTradeOrderNo());
		po.setMchName(dto.getMchName());
		po.setChannelId(dto.getChannelId());
		po.setTradeTime(dto.getTradeTime());
		po.setAgentId(dto.getAgentId());
		po.setTotalAmount(dto.getTotalAmount());
		po.setContractId(dto.getContractId());
		po.setReceiptAmount(dto.getReceiptAmount());
		po.setProfit(dto.getProfit());
		po.setOrderNo(dto.getOrderNo());
		po.setAgentName(dto.getAgentName());
		po.setChannelName(dto.getChannelName());
		po.setPayType(dto.getPayType());
		po.setContractNo(dto.getContractNo());
		po.setProductName(dto.getProductName());
		po.setContractName(dto.getContractName());
		po.setProductNo(dto.getProductNo());
		po.setPayWay(dto.getPayWay());
		po.setMchId(dto.getMchId());
		po.setAppId(dto.getAppId());
		rMchRecReconMistakeMapper.insert(po);
	}

	public void batchAddMistakes(List<ReconMistakeDto> mistakeList) {
		for (ReconMistakeDto dto : mistakeList) {
			addMistake(dto);
		}
	}
	
}
