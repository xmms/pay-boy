package com.ndood.common.base.pojo.record;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-04-13
 */
@TableName("r_mch_rec_recon_scratch_pool")
public class RMchRecReconScratchPoolPo extends Model<RMchRecReconScratchPoolPo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private String recordNo;

    /**
     * 交易订单流水号
     */
    private String tradeOrderNo;

    /**
     * 订单流水号
     */
    private String orderNo;

    /**
     * 代理商ID
     */
    private Integer agentId;

    private String agentName;

    /**
     * 商户ID
     */
    private Integer mchId;

    private String mchName;

    /**
     * 支付方式
     */
    private Integer payWay;

    /**
     * 支付类型
     */
    private Integer payType;

    private Integer contractId;

    private Integer contractType;

    private String contractNo;

    private String contractName;

    private String productNo;

    private String productName;

    private Integer channelId;

    private String channelNo;

    private String channelName;

    private String appId;

    /**
     * 交易类型：1 支付 2 退款
     */
    private Integer tradeType;

    /**
     * 交易时间
     */
    private String tradeTime;

    /**
     * 交易金额
     */
    private BigDecimal totalAmount;

    /**
     * 实际交易金额
     */
    private BigDecimal receiptAmount;

    /**
     * 服务费
     */
    private BigDecimal profit;

    private Date createTime;

    private Date updateTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRecordNo() {
        return recordNo;
    }

    public void setRecordNo(String recordNo) {
        this.recordNo = recordNo;
    }

    public String getTradeOrderNo() {
        return tradeOrderNo;
    }

    public void setTradeOrderNo(String tradeOrderNo) {
        this.tradeOrderNo = tradeOrderNo;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public Integer getMchId() {
        return mchId;
    }

    public void setMchId(Integer mchId) {
        this.mchId = mchId;
    }

    public String getMchName() {
        return mchName;
    }

    public void setMchName(String mchName) {
        this.mchName = mchName;
    }

    public Integer getPayWay() {
        return payWay;
    }

    public void setPayWay(Integer payWay) {
        this.payWay = payWay;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public Integer getContractType() {
        return contractType;
    }

    public void setContractType(Integer contractType) {
        this.contractType = contractType;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getContractName() {
        return contractName;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName;
    }

    public String getProductNo() {
        return productNo;
    }

    public void setProductNo(String productNo) {
        this.productNo = productNo;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getChannelId() {
        return channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    public String getChannelNo() {
        return channelNo;
    }

    public void setChannelNo(String channelNo) {
        this.channelNo = channelNo;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public Integer getTradeType() {
        return tradeType;
    }

    public void setTradeType(Integer tradeType) {
        this.tradeType = tradeType;
    }

    public String getTradeTime() {
        return tradeTime;
    }

    public void setTradeTime(String tradeTime) {
        this.tradeTime = tradeTime;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getReceiptAmount() {
        return receiptAmount;
    }

    public void setReceiptAmount(BigDecimal receiptAmount) {
        this.receiptAmount = receiptAmount;
    }

    public BigDecimal getProfit() {
        return profit;
    }

    public void setProfit(BigDecimal profit) {
        this.profit = profit;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "RMchRecReconScratchPoolPo{" +
        "id=" + id +
        ", recordNo=" + recordNo +
        ", tradeOrderNo=" + tradeOrderNo +
        ", orderNo=" + orderNo +
        ", agentId=" + agentId +
        ", agentName=" + agentName +
        ", mchId=" + mchId +
        ", mchName=" + mchName +
        ", payWay=" + payWay +
        ", payType=" + payType +
        ", contractId=" + contractId +
        ", contractType=" + contractType +
        ", contractNo=" + contractNo +
        ", contractName=" + contractName +
        ", productNo=" + productNo +
        ", productName=" + productName +
        ", channelId=" + channelId +
        ", channelNo=" + channelNo +
        ", channelName=" + channelName +
        ", appId=" + appId +
        ", tradeType=" + tradeType +
        ", tradeTime=" + tradeTime +
        ", totalAmount=" + totalAmount +
        ", receiptAmount=" + receiptAmount +
        ", profit=" + profit +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
