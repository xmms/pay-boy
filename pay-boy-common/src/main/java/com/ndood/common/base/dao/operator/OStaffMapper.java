package com.ndood.common.base.dao.operator;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ndood.common.base.pojo.operator.OStaffPo;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
public interface OStaffMapper extends BaseMapper<OStaffPo> {

}
