package com.ndood.common.base.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 简单复制一些基础字段
 */
public class JPAUtil {
	/*
	 * 将父类所有的属性COPY到子类中。 类定义中child一定要extends father；
	 * 而且child和father一定为严格javabean写法，属性为deleteDate，方法为getDeleteDate
	 */
	public static void fatherToChild(Object father, Object child) throws Exception {
		if (!(child.getClass().getSuperclass() == father.getClass())) {
			throw new Exception("child不是father的子类");
		}
		Class<? extends Object> fatherClazz = father.getClass();
		Field fields[] = fatherClazz.getDeclaredFields();
		for (Field field : fields) {
			field.setAccessible(true);
			if(Modifier.isStatic(field.getModifiers()) || Modifier.isFinal(field.getModifiers())){
				continue;
			}
			Object obj = field.get(father);
			if(obj==null){
				continue;
			}
			// 对于简单数据类型的才做处理
			Class<?> fieldClazz = field.getType();
			if (fieldClazz == int.class || fieldClazz == Integer.class
					||fieldClazz == boolean.class || fieldClazz == Boolean.class
					||fieldClazz == float.class || fieldClazz == Float.class
					||fieldClazz == double.class || fieldClazz == Double.class
					||fieldClazz == short.class || fieldClazz == Short.class
					||fieldClazz == BigDecimal.class
					||fieldClazz == Date.class
					||fieldClazz == String.class
					||fieldClazz == Timestamp.class
					||fieldClazz == long.class || fieldClazz == Long.class) {
				field.set(child, obj);
			}
			field.setAccessible(false);
		}
	}
	
	public static void childToFather(Object child, Object father) throws Exception {
		if (!(child.getClass().getSuperclass() == father.getClass())) {
			throw new Exception("child不是father的子类");
		}
		Class<? extends Object> fatherClazz = father.getClass();
		Field fields[] = fatherClazz.getDeclaredFields();
		for (Field field : fields) {
			field.setAccessible(true);
			if(Modifier.isStatic(field.getModifiers()) || Modifier.isFinal(field.getModifiers())){
				continue;
			}
			Object obj = field.get(child);
			if(obj==null){
				continue;
			}
			Class<?> fieldClazz = field.getType();
			if (fieldClazz == int.class || fieldClazz == Integer.class
					||fieldClazz == boolean.class || fieldClazz == Boolean.class
					||fieldClazz == float.class || fieldClazz == Float.class
					||fieldClazz == double.class || fieldClazz == Double.class
					||fieldClazz == short.class || fieldClazz == Short.class
					||fieldClazz == BigDecimal.class
					||fieldClazz == Date.class
					||fieldClazz == String.class
					||fieldClazz == Timestamp.class
					||fieldClazz == long.class || fieldClazz == Long.class) {
				field.set(father, obj);
			}
			field.setAccessible(false);
		}
	}
	
	/**
	 * 转换实体类  
	 * @param list
	 * @param clazz
	 * @return
	 * @throws Exception
	 */
    @SuppressWarnings("rawtypes")
	public static <T> List<T> castEntity(List<Object[]> list, Class<T> clazz) throws Exception {    
        List<T> returnList = new ArrayList<T>();    
        Object[] co = list.get(0);    
        Class[] c2 = new Class[co.length];    
            
        //确定构造方法    
        for(int i = 0; i < co.length; i++){    
            c2[i] = co[i].getClass();    
        }    
            
        for(Object[] o : list){    
            Constructor<T> constructor = clazz.getConstructor(c2);    
            returnList.add(constructor.newInstance(o));    
        }    
            
        return returnList;    
    } 
}
