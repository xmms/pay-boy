package com.ndood.common.base.pojo.record;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-04-13
 */
@TableName("r_mch_rec_recon_batch")
public class RMchRecReconBatchPo extends Model<RMchRecReconBatchPo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 对账批次号
     */
    private String batchNo;

    /**
     * 对账日
     */
    private String reconDay;

    /**
     * 对账类型：1 离线对账 2 在线对账
     */
    private Integer reconType;

    /**
     * 支付方式
     */
    private Integer payWay;

    /**
     * 错误数
     */
    private Integer mistakeCount;

    /**
     * 未处理错误数
     */
    private Integer unhandleMistakeCount;

    /**
     * 交易数量
     */
    private Integer tradeCount;

    /**
     * 交易金额
     */
    private BigDecimal tradeAmount;

    /**
     * 退款数量
     */
    private Integer refundCount;

    /**
     * 退款金额
     */
    private BigDecimal refundAmount;

    /**
     * 远程交易数量
     */
    private Integer remoteTradeCount;

    /**
     * 远程交易额
     */
    private BigDecimal remoteTradeAmount;

    /**
     * 远程退款数量
     */
    private Integer remoteRefundCount;

    /**
     * 远程退款金额
     */
    private BigDecimal remoteRefundAmount;

    /**
     * 对账单地址
     */
    private String orgCheckFilePath;

    /**
     * 对账文件存放地址
     */
    private String releaseCheckFilePath;

    /**
     * 文件解析状态： 1 待解析 2 已解析
     */
    private Integer releaseStatus;

    /**
     * 错误码
     */
    private String errorCode;

    /**
     * 错误描述
     */
    private String errorMsg;

    /**
     * 备注
     */
    private String remark;

    /**
     * 状态：1 待对账 2 已对账 3 对账失败
     */
    private Integer status;

    private Date createTime;

    private Date updateTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public String getReconDay() {
        return reconDay;
    }

    public void setReconDay(String reconDay) {
        this.reconDay = reconDay;
    }

    public Integer getReconType() {
        return reconType;
    }

    public void setReconType(Integer reconType) {
        this.reconType = reconType;
    }

    public Integer getPayWay() {
        return payWay;
    }

    public void setPayWay(Integer payWay) {
        this.payWay = payWay;
    }

    public Integer getMistakeCount() {
        return mistakeCount;
    }

    public void setMistakeCount(Integer mistakeCount) {
        this.mistakeCount = mistakeCount;
    }

    public Integer getUnhandleMistakeCount() {
        return unhandleMistakeCount;
    }

    public void setUnhandleMistakeCount(Integer unhandleMistakeCount) {
        this.unhandleMistakeCount = unhandleMistakeCount;
    }

    public Integer getTradeCount() {
        return tradeCount;
    }

    public void setTradeCount(Integer tradeCount) {
        this.tradeCount = tradeCount;
    }

    public BigDecimal getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(BigDecimal tradeAmount) {
        this.tradeAmount = tradeAmount;
    }

    public Integer getRefundCount() {
        return refundCount;
    }

    public void setRefundCount(Integer refundCount) {
        this.refundCount = refundCount;
    }

    public BigDecimal getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(BigDecimal refundAmount) {
        this.refundAmount = refundAmount;
    }

    public Integer getRemoteTradeCount() {
        return remoteTradeCount;
    }

    public void setRemoteTradeCount(Integer remoteTradeCount) {
        this.remoteTradeCount = remoteTradeCount;
    }

    public BigDecimal getRemoteTradeAmount() {
        return remoteTradeAmount;
    }

    public void setRemoteTradeAmount(BigDecimal remoteTradeAmount) {
        this.remoteTradeAmount = remoteTradeAmount;
    }

    public Integer getRemoteRefundCount() {
        return remoteRefundCount;
    }

    public void setRemoteRefundCount(Integer remoteRefundCount) {
        this.remoteRefundCount = remoteRefundCount;
    }

    public BigDecimal getRemoteRefundAmount() {
        return remoteRefundAmount;
    }

    public void setRemoteRefundAmount(BigDecimal remoteRefundAmount) {
        this.remoteRefundAmount = remoteRefundAmount;
    }

    public String getOrgCheckFilePath() {
        return orgCheckFilePath;
    }

    public void setOrgCheckFilePath(String orgCheckFilePath) {
        this.orgCheckFilePath = orgCheckFilePath;
    }

    public String getReleaseCheckFilePath() {
        return releaseCheckFilePath;
    }

    public void setReleaseCheckFilePath(String releaseCheckFilePath) {
        this.releaseCheckFilePath = releaseCheckFilePath;
    }

    public Integer getReleaseStatus() {
        return releaseStatus;
    }

    public void setReleaseStatus(Integer releaseStatus) {
        this.releaseStatus = releaseStatus;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "RMchRecReconBatchPo{" +
        "id=" + id +
        ", batchNo=" + batchNo +
        ", reconDay=" + reconDay +
        ", reconType=" + reconType +
        ", payWay=" + payWay +
        ", mistakeCount=" + mistakeCount +
        ", unhandleMistakeCount=" + unhandleMistakeCount +
        ", tradeCount=" + tradeCount +
        ", tradeAmount=" + tradeAmount +
        ", refundCount=" + refundCount +
        ", refundAmount=" + refundAmount +
        ", remoteTradeCount=" + remoteTradeCount +
        ", remoteTradeAmount=" + remoteTradeAmount +
        ", remoteRefundCount=" + remoteRefundCount +
        ", remoteRefundAmount=" + remoteRefundAmount +
        ", orgCheckFilePath=" + orgCheckFilePath +
        ", releaseCheckFilePath=" + releaseCheckFilePath +
        ", releaseStatus=" + releaseStatus +
        ", errorCode=" + errorCode +
        ", errorMsg=" + errorMsg +
        ", remark=" + remark +
        ", status=" + status +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
