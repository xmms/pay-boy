package com.ndood.common.base.pojo.record;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
@TableName("r_mch_account")
public class RMchAccountPo extends Model<RMchAccountPo> {

    private static final long serialVersionUID = 1L;

    private String recordNo;

    /**
     * 交易类型：1 充值 2 转账 3 消费 4 提现 5 结算到余额
     */
    private Integer type;

    /**
     * 代理商ID
     */
    private Integer agentId;

    /**
     * 代理商名称
     */
    private String agentName;

    /**
     * 商户ID
     */
    private Integer mchId;

    /**
     * 商户名称
     */
    private String mchName;

    /**
     * 旧余额
     */
    private BigDecimal oldBalance;

    /**
     * 新余额
     */
    private BigDecimal newBalance;

    /**
     * 变动金额
     */
    private BigDecimal amount;

    /**
     * 资金订单原始报文，来自多种订单表
     */
    private String extra;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


    public String getRecordNo() {
        return recordNo;
    }

    public void setRecordNo(String recordNo) {
        this.recordNo = recordNo;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public Integer getMchId() {
        return mchId;
    }

    public void setMchId(Integer mchId) {
        this.mchId = mchId;
    }

    public String getMchName() {
        return mchName;
    }

    public void setMchName(String mchName) {
        this.mchName = mchName;
    }

    public BigDecimal getOldBalance() {
        return oldBalance;
    }

    public void setOldBalance(BigDecimal oldBalance) {
        this.oldBalance = oldBalance;
    }

    public BigDecimal getNewBalance() {
        return newBalance;
    }

    public void setNewBalance(BigDecimal newBalance) {
        this.newBalance = newBalance;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.recordNo;
    }

    @Override
    public String toString() {
        return "RMchAccountPo{" +
        ", recordNo=" + recordNo +
        ", type=" + type +
        ", agentId=" + agentId +
        ", agentName=" + agentName +
        ", mchId=" + mchId +
        ", mchName=" + mchName +
        ", oldBalance=" + oldBalance +
        ", newBalance=" + newBalance +
        ", amount=" + amount +
        ", extra=" + extra +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
