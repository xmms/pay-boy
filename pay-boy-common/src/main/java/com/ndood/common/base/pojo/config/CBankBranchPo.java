package com.ndood.common.base.pojo.config;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
@TableName("c_bank_branch")
public class CBankBranchPo extends Model<CBankBranchPo> {

    private static final long serialVersionUID = 1L;

    private Long code;

    private String name;

    private String province;

    private String city;

    private String bank;


    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    @Override
    protected Serializable pkVal() {
        return this.code;
    }

    @Override
    public String toString() {
        return "CBankBranchPo{" +
        ", code=" + code +
        ", name=" + name +
        ", province=" + province +
        ", city=" + city +
        ", bank=" + bank +
        "}";
    }
}
