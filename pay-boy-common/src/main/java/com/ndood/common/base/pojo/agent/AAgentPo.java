package com.ndood.common.base.pojo.agent;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
@TableName("a_agent")
public class AAgentPo extends Model<AAgentPo> {

    private static final long serialVersionUID = 1L;

    /**
     * 代理商ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 父级代理商ID
     */
    private Integer parentId;

    /**
     * 代理商提成费率
     */
    private BigDecimal getRate;

    /**
     * 代理商名称
     */
    private String name;

    /**
     * 手机
     */
    private String mobile;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 企业QQ
     */
    private String qq;

    /**
     * 代理商证件类型
     */
    private Integer certType;

    /**
     * 代理商证件编码
     */
    private String certNo;

    /**
     * 证件人名称
     */
    private String certName;

    /**
     * 代理商证件生效时间
     */
    private LocalDateTime certStart;

    /**
     * 代理商证件失效时间
     */
    private LocalDateTime certEnd;

    /**
     * 商户备注
     */
    private String remark;

    /**
     * 状态: 1 待审核 2 审核通过 3 审核失败
     */
    private Integer status;

    /**
     * 逻辑删除: 0 未删除 1 删除
     */
    private Integer logicDelete;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public BigDecimal getGetRate() {
        return getRate;
    }

    public void setGetRate(BigDecimal getRate) {
        this.getRate = getRate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public Integer getCertType() {
        return certType;
    }

    public void setCertType(Integer certType) {
        this.certType = certType;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getCertName() {
        return certName;
    }

    public void setCertName(String certName) {
        this.certName = certName;
    }

    public LocalDateTime getCertStart() {
        return certStart;
    }

    public void setCertStart(LocalDateTime certStart) {
        this.certStart = certStart;
    }

    public LocalDateTime getCertEnd() {
        return certEnd;
    }

    public void setCertEnd(LocalDateTime certEnd) {
        this.certEnd = certEnd;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getLogicDelete() {
        return logicDelete;
    }

    public void setLogicDelete(Integer logicDelete) {
        this.logicDelete = logicDelete;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "AAgentPo{" +
        ", id=" + id +
        ", parentId=" + parentId +
        ", getRate=" + getRate +
        ", name=" + name +
        ", mobile=" + mobile +
        ", email=" + email +
        ", qq=" + qq +
        ", certType=" + certType +
        ", certNo=" + certNo +
        ", certName=" + certName +
        ", certStart=" + certStart +
        ", certEnd=" + certEnd +
        ", remark=" + remark +
        ", status=" + status +
        ", logicDelete=" + logicDelete +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
