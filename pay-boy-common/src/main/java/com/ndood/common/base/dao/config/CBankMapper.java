package com.ndood.common.base.dao.config;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ndood.common.base.pojo.config.CBankPo;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
public interface CBankMapper extends BaseMapper<CBankPo> {

}
