package com.ndood.common.base.pojo.record;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
@TableName("r_rec_refund")
public class RRecRefundPo extends Model<RRecRefundPo> {

    private static final long serialVersionUID = 1L;

    private Long id;
    
    private String refundOrderNo;
	/**
	 * 商户ID
	 */
	private Integer mchId;
	/**
	 * 商户名称
	 */
	private String mchName;
	/**
	 * 代理ID
	 */
	private Integer agentId;
	/**
	 * 代理名称
	 */
	private String agentName;
    
    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 支付方式： 1 支付宝 2 微信
     */
    private Integer payWay;

    /**
     * 支付类型：1 扫码 2 被扫 3门店码
     */
    private Integer payType;

    /**
     * 合同ID
     */
    private Integer contractId;

    /**
     * 合同编码
     */
    private String contractNo;

    /**
     * 合同类型： 1 直清 2 二清 3子商户
     */
    private Integer contractType;

    /**
     * 合同名称
     */
    private String contractName;

    /**
     * 支付产品编码
     */
    private String productNo;

    /**
     * 支付产品名称
     */
    private String productName;

    /**
     * 支付渠道编码
     */
    private Integer channelId;

    /**
     * 渠道编码
     */
    private String channelNo;

    /**
     * 支付渠道名称
     */
    private String channelName;

    private BigDecimal refundAmount;

    private BigDecimal refundFreeAmount;

    private BigDecimal refundReceiptAmount;

    private BigDecimal refundThirdFreeAmount;

    private BigDecimal refundBuyerPayAmount;

    private String refundTime;

    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
     * 状态：1 创建 2 成功 3 失败
     */
    private Integer status;

    /**
     * 渠道应用appid
     */
    private String param1;

    /**
     * 跨公众号支付所需的sub_appid，来自商户合同
     */
    private String param2;

    /**
     * 渠道商户号
     */
    private String param3;

    /**
     * 跨公众号支付所需的sub_mch_id，来自商户合同
     */
    private String param4;

    /**
     * 跨公众号支付sub_open_id
     */
    private String param5;

    /**
     * 渠道返回报文
     */
    private String param6;

    private Date createTime;

    private Date updateTime;

    public Integer getMchId() {
		return mchId;
	}

	public void setMchId(Integer mchId) {
		this.mchId = mchId;
	}

	public String getMchName() {
		return mchName;
	}

	public void setMchName(String mchName) {
		this.mchName = mchName;
	}

	public Integer getAgentId() {
		return agentId;
	}

	public void setAgentId(Integer agentId) {
		this.agentId = agentId;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getRefundOrderNo() {
        return refundOrderNo;
    }

    public void setRefundOrderNo(String refundOrderNo) {
        this.refundOrderNo = refundOrderNo;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getPayWay() {
        return payWay;
    }

    public void setPayWay(Integer payWay) {
        this.payWay = payWay;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public Integer getContractType() {
        return contractType;
    }

    public void setContractType(Integer contractType) {
        this.contractType = contractType;
    }

    public String getContractName() {
        return contractName;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName;
    }

    public String getProductNo() {
        return productNo;
    }

    public void setProductNo(String productNo) {
        this.productNo = productNo;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getChannelId() {
        return channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    public String getChannelNo() {
        return channelNo;
    }

    public void setChannelNo(String channelNo) {
        this.channelNo = channelNo;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public BigDecimal getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(BigDecimal refundAmount) {
        this.refundAmount = refundAmount;
    }

    public BigDecimal getRefundFreeAmount() {
        return refundFreeAmount;
    }

    public void setRefundFreeAmount(BigDecimal refundFreeAmount) {
        this.refundFreeAmount = refundFreeAmount;
    }

    public BigDecimal getRefundReceiptAmount() {
        return refundReceiptAmount;
    }

    public void setRefundReceiptAmount(BigDecimal refundReceiptAmount) {
        this.refundReceiptAmount = refundReceiptAmount;
    }

    public BigDecimal getRefundThirdFreeAmount() {
        return refundThirdFreeAmount;
    }

    public void setRefundThirdFreeAmount(BigDecimal refundThirdFreeAmount) {
        this.refundThirdFreeAmount = refundThirdFreeAmount;
    }

    public BigDecimal getRefundBuyerPayAmount() {
        return refundBuyerPayAmount;
    }

    public void setRefundBuyerPayAmount(BigDecimal refundBuyerPayAmount) {
        this.refundBuyerPayAmount = refundBuyerPayAmount;
    }

    public String getRefundTime() {
        return refundTime;
    }

    public void setRefundTime(String refundTime) {
        this.refundTime = refundTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getParam1() {
        return param1;
    }

    public void setParam1(String param1) {
        this.param1 = param1;
    }

    public String getParam2() {
        return param2;
    }

    public void setParam2(String param2) {
        this.param2 = param2;
    }

    public String getParam3() {
        return param3;
    }

    public void setParam3(String param3) {
        this.param3 = param3;
    }

    public String getParam4() {
        return param4;
    }

    public void setParam4(String param4) {
        this.param4 = param4;
    }

    public String getParam5() {
        return param5;
    }

    public void setParam5(String param5) {
        this.param5 = param5;
    }

    public String getParam6() {
        return param6;
    }

    public void setParam6(String param6) {
        this.param6 = param6;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.refundOrderNo;
    }

    @Override
    public String toString() {
        return "RRecRefundPo{" +
        ", refundOrderNo=" + refundOrderNo +
        ", orderNo=" + orderNo +
        ", payWay=" + payWay +
        ", payType=" + payType +
        ", contractId=" + contractId +
        ", contractNo=" + contractNo +
        ", contractType=" + contractType +
        ", contractName=" + contractName +
        ", productNo=" + productNo +
        ", productName=" + productName +
        ", channelId=" + channelId +
        ", channelNo=" + channelNo +
        ", channelName=" + channelName +
        ", refundAmount=" + refundAmount +
        ", refundFreeAmount=" + refundFreeAmount +
        ", refundReceiptAmount=" + refundReceiptAmount +
        ", refundThirdFreeAmount=" + refundThirdFreeAmount +
        ", refundBuyerPayAmount=" + refundBuyerPayAmount +
        ", refundTime=" + refundTime +
        ", status=" + status +
        ", param1=" + param1 +
        ", param2=" + param2 +
        ", param3=" + param3 +
        ", param4=" + param4 +
        ", param5=" + param5 +
        ", param6=" + param6 +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
