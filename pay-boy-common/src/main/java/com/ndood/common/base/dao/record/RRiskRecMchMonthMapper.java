package com.ndood.common.base.dao.record;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ndood.common.base.pojo.record.RRiskRecMchMonthPo;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
public interface RRiskRecMchMonthMapper extends BaseMapper<RRiskRecMchMonthPo> {

}
