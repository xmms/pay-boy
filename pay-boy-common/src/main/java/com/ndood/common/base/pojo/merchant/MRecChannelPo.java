package com.ndood.common.base.pojo.merchant;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
@TableName("m_rec_channel")
public class MRecChannelPo extends Model<MRecChannelPo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 代理商ID
     */
    private Integer agentId;

    /**
     * 商户ID
     */
    private Integer mchId;

    /**
     * 支付合同ID
     */
    private Integer contractId;

    /**
     * 支付合同编码
     */
    private String contractNo;

    /**
     * 合同类型: 1 直清 2 二清 3 子商户
     */
    private Integer contractType;

    /**
     * 支付合同名称
     */
    private String contractName;

    /**
     * 产品类型编码
     */
    private String productNo;

    /**
     * 产品名称
     */
    private String productName;

    /**
     * 渠道类型编码
     */
    private String channelNo;

    /**
     * 渠道名称
     */
    private String channelName;

    /**
     * 支付方式
     */
    private Integer payWay;

    /**
     * 支付类型
     */
    private Integer payType;

    /**
     * 状态：0 禁用 1 启用
     */
    private Integer status;

    /**
     * 优先级，用来控制渠道路由
     */
    private Integer priority;

    private Date createTime;

    private Date updateTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public Integer getMchId() {
        return mchId;
    }

    public void setMchId(Integer mchId) {
        this.mchId = mchId;
    }

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public Integer getContractType() {
        return contractType;
    }

    public void setContractType(Integer contractType) {
        this.contractType = contractType;
    }

    public String getContractName() {
        return contractName;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName;
    }

    public String getProductNo() {
        return productNo;
    }

    public void setProductNo(String productNo) {
        this.productNo = productNo;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getChannelNo() {
        return channelNo;
    }

    public void setChannelNo(String channelNo) {
        this.channelNo = channelNo;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public Integer getPayWay() {
        return payWay;
    }

    public void setPayWay(Integer payWay) {
        this.payWay = payWay;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "MRecChannelPo{" +
        ", id=" + id +
        ", agentId=" + agentId +
        ", mchId=" + mchId +
        ", contractId=" + contractId +
        ", contractNo=" + contractNo +
        ", contractType=" + contractType +
        ", contractName=" + contractName +
        ", productNo=" + productNo +
        ", productName=" + productName +
        ", channelNo=" + channelNo +
        ", channelName=" + channelName +
        ", payWay=" + payWay +
        ", payType=" + payType +
        ", status=" + status +
        ", priority=" + priority +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
