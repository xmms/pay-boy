package com.ndood.common.base.pojo.merchant;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
@TableName("m_shop")
public class MShopPo extends Model<MShopPo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 代理商ID
     */
    private Integer agentId;

    /**
     * 商户ID
     */
    private Integer mchId;

    /**
     * 门店名称
     */
    private String name;

    /**
     * 所在省
     */
    private String provinceId;

    /**
     * 省
     */
    private String province;

    /**
     * 市
     */
    private String cityId;

    /**
     * 市
     */
    private String city;

    /**
     * 所在区
     */
    private String regionId;

    /**
     * 区
     */
    private String region;

    /**
     * 门店详细地址
     */
    private String address;

    /**
     * 门店负责人
     */
    private String linkMan;

    /**
     * 门店负责人手机号
     */
    private String linkMobile;

    /**
     * 门店备注
     */
    private String remark;

    /**
     * 经度
     */
    private String latitude;

    /**
     * 纬度
     */
    private String longtitude;

    /**
     * 营业时间
     */
    private String serviceTime;

    /**
     * 店面门牌照
     */
    private String outterImgUrl;

    /**
     * 门店内部照
     */
    private String innerImgUrl;

    /**
     * 其它照片1
     */
    private String otherImg1Url;

    /**
     * 其它照片2
     */
    private String otherImg2Url;

    /**
     * 门店状态：0 待审核 1 启用 2 禁用
     */
    private Integer status;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public Integer getMchId() {
        return mchId;
    }

    public void setMchId(Integer mchId) {
        this.mchId = mchId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLinkMan() {
        return linkMan;
    }

    public void setLinkMan(String linkMan) {
        this.linkMan = linkMan;
    }

    public String getLinkMobile() {
        return linkMobile;
    }

    public void setLinkMobile(String linkMobile) {
        this.linkMobile = linkMobile;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(String longtitude) {
        this.longtitude = longtitude;
    }

    public String getServiceTime() {
        return serviceTime;
    }

    public void setServiceTime(String serviceTime) {
        this.serviceTime = serviceTime;
    }

    public String getOutterImgUrl() {
        return outterImgUrl;
    }

    public void setOutterImgUrl(String outterImgUrl) {
        this.outterImgUrl = outterImgUrl;
    }

    public String getInnerImgUrl() {
        return innerImgUrl;
    }

    public void setInnerImgUrl(String innerImgUrl) {
        this.innerImgUrl = innerImgUrl;
    }

    public String getOtherImg1Url() {
        return otherImg1Url;
    }

    public void setOtherImg1Url(String otherImg1Url) {
        this.otherImg1Url = otherImg1Url;
    }

    public String getOtherImg2Url() {
        return otherImg2Url;
    }

    public void setOtherImg2Url(String otherImg2Url) {
        this.otherImg2Url = otherImg2Url;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "MShopPo{" +
        ", id=" + id +
        ", agentId=" + agentId +
        ", mchId=" + mchId +
        ", name=" + name +
        ", provinceId=" + provinceId +
        ", province=" + province +
        ", cityId=" + cityId +
        ", city=" + city +
        ", regionId=" + regionId +
        ", region=" + region +
        ", address=" + address +
        ", linkMan=" + linkMan +
        ", linkMobile=" + linkMobile +
        ", remark=" + remark +
        ", latitude=" + latitude +
        ", longtitude=" + longtitude +
        ", serviceTime=" + serviceTime +
        ", outterImgUrl=" + outterImgUrl +
        ", innerImgUrl=" + innerImgUrl +
        ", otherImg1Url=" + otherImg1Url +
        ", otherImg2Url=" + otherImg2Url +
        ", status=" + status +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
