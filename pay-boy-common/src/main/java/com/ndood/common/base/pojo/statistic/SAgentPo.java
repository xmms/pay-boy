package com.ndood.common.base.pojo.statistic;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
@TableName("s_agent")
public class SAgentPo extends Model<SAgentPo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 商户ID
     */
    private Integer agentId;

    /**
     * 日期
     */
    private LocalDate day;

    /**
     * 商户名称
     */
    private String agentName;

    /**
     * 收入笔数
     */
    private Long incomeCount;

    /**
     * 收入总金额
     */
    private BigDecimal incomeAmount;

    /**
     * 支出笔数
     */
    private Long outcomeCount;

    /**
     * 支出总金额
     */
    private BigDecimal outcomeAmount;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public LocalDate getDay() {
        return day;
    }

    public void setDay(LocalDate day) {
        this.day = day;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public Long getIncomeCount() {
        return incomeCount;
    }

    public void setIncomeCount(Long incomeCount) {
        this.incomeCount = incomeCount;
    }

    public BigDecimal getIncomeAmount() {
        return incomeAmount;
    }

    public void setIncomeAmount(BigDecimal incomeAmount) {
        this.incomeAmount = incomeAmount;
    }

    public Long getOutcomeCount() {
        return outcomeCount;
    }

    public void setOutcomeCount(Long outcomeCount) {
        this.outcomeCount = outcomeCount;
    }

    public BigDecimal getOutcomeAmount() {
        return outcomeAmount;
    }

    public void setOutcomeAmount(BigDecimal outcomeAmount) {
        this.outcomeAmount = outcomeAmount;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SAgentPo{" +
        ", id=" + id +
        ", agentId=" + agentId +
        ", day=" + day +
        ", agentName=" + agentName +
        ", incomeCount=" + incomeCount +
        ", incomeAmount=" + incomeAmount +
        ", outcomeCount=" + outcomeCount +
        ", outcomeAmount=" + outcomeAmount +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
