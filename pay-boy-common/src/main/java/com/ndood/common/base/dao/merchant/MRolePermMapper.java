package com.ndood.common.base.dao.merchant;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ndood.common.base.pojo.merchant.MRolePermPo;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
public interface MRolePermMapper extends BaseMapper<MRolePermPo> {

}
