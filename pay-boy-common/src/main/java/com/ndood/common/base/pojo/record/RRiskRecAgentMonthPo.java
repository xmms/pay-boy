package com.ndood.common.base.pojo.record;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
@TableName("r_risk_rec_agent_month")
public class RRiskRecAgentMonthPo extends Model<RRiskRecAgentMonthPo> {

    private static final long serialVersionUID = 1L;

    private String recordNo;

    /**
     * 代理商ID
     */
    private Integer agentId;

    /**
     * 月份代码 201801
     */
    private String month;

    /**
     * 本月提现额度
     */
    private BigDecimal settleAmount;

    /**
     * 本月提现次数
     */
    private Integer settleCount;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


    public String getRecordNo() {
        return recordNo;
    }

    public void setRecordNo(String recordNo) {
        this.recordNo = recordNo;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public BigDecimal getSettleAmount() {
        return settleAmount;
    }

    public void setSettleAmount(BigDecimal settleAmount) {
        this.settleAmount = settleAmount;
    }

    public Integer getSettleCount() {
        return settleCount;
    }

    public void setSettleCount(Integer settleCount) {
        this.settleCount = settleCount;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.recordNo;
    }

    @Override
    public String toString() {
        return "RRiskRecAgentMonthPo{" +
        ", recordNo=" + recordNo +
        ", agentId=" + agentId +
        ", month=" + month +
        ", settleAmount=" + settleAmount +
        ", settleCount=" + settleCount +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
