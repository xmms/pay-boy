package com.ndood.common.base.util;

import java.lang.reflect.Method;

public class CodeUtil {
	/**
	 * 1 设置getter setter
	 */
	public static void printPoGetterDto(Class<?> clazz) {
		Method[] methods = clazz.getMethods();
		for (Method method : methods) {
			String name = method.getName();
			if(name.startsWith("get")) {
				System.out.println("po.set"+method.getName().replaceFirst("get", "")+"(dto."+method.getName()+"()"+");");
			}
		}
	}
	
}