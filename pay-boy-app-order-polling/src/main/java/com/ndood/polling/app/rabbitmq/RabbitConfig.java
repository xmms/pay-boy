package com.ndood.polling.app.rabbitmq;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * https://blog.csdn.net/ztx114/article/details/78410727
 * https://github.com/ityouknow/spring-boot-examples/tree/master/spring-boot-rabbitmq/src/main/java/com/neo/rabbit
 */
@Configuration
public class RabbitConfig {
 
	/**
	 * 轮询队列
	 */
    @Bean
    public Queue pollingQueue() {
        return new Queue("polling");
    }
 
}
