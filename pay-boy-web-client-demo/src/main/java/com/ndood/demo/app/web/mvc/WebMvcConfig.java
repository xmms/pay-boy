package com.ndood.demo.app.web.mvc;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.CacheControl;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.ndood.demo.app.properties.DemoProperties;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
	
	@Autowired
	private DemoProperties demoProperties;
	
	/**
	 * 静态资源映射
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		// 生产环境配置
		if (!demoProperties.getIsDevelop()) {
			String location = "classpath:/static/build/";
			// 解决springboot2.0找不到根目录下静态js文件问题
			registry.addResourceHandler("/static/**").addResourceLocations(location).setCacheControl(CacheControl.maxAge(7, TimeUnit.DAYS));
			registry.addResourceHandler("/favicon.ico").addResourceLocations(location + "favicon.ico");
		// 开发环境配置
		} else {
			String location = "classpath:/static/native/";
			registry.addResourceHandler("/static/**").addResourceLocations(location);
			registry.addResourceHandler("/favicon.ico").addResourceLocations(location + "favicon.ico");
		}
	}
	
}

