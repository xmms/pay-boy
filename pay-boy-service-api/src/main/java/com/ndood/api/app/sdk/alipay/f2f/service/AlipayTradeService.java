package com.ndood.api.app.sdk.alipay.f2f.service;

import com.ndood.api.app.sdk.alipay.f2f.model.builder.AlipayTradeCancelRequestBuilder;
import com.ndood.api.app.sdk.alipay.f2f.model.builder.AlipayTradePayRequestBuilder;
import com.ndood.api.app.sdk.alipay.f2f.model.builder.AlipayTradePrecreateRequestBuilder;
import com.ndood.api.app.sdk.alipay.f2f.model.builder.AlipayTradeQueryRequestBuilder;
import com.ndood.api.app.sdk.alipay.f2f.model.builder.AlipayTradeRefundRequestBuilder;
import com.ndood.api.app.sdk.alipay.f2f.model.result.AlipayF2FCancelResult;
import com.ndood.api.app.sdk.alipay.f2f.model.result.AlipayF2FPayResult;
import com.ndood.api.app.sdk.alipay.f2f.model.result.AlipayF2FPrecreateResult;
import com.ndood.api.app.sdk.alipay.f2f.model.result.AlipayF2FQueryResult;
import com.ndood.api.app.sdk.alipay.f2f.model.result.AlipayF2FRefundResult;

/**
 * Created by liuyangkly on 15/7/29.
 */
public interface AlipayTradeService {

    // 当面付2.0流程支付
    public AlipayF2FPayResult tradePay(AlipayTradePayRequestBuilder builder);

    // 当面付2.0消费查询
    public AlipayF2FQueryResult queryTradeResult(AlipayTradeQueryRequestBuilder builder);

    // 当面付2.0消费退款
    public AlipayF2FRefundResult tradeRefund(AlipayTradeRefundRequestBuilder builder);

    // 当面付2.0预下单(生成二维码)
    public AlipayF2FPrecreateResult tradePrecreate(AlipayTradePrecreateRequestBuilder builder);

    // 当面付2.0撤销订单(ndood扩展)
	public AlipayF2FCancelResult cancelTrade(AlipayTradeCancelRequestBuilder builder);
}
