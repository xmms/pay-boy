package com.ndood.api.app.properties;

/**
 * 自定义属性配置类SecurityProperties
 */
import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@ConfigurationProperties(prefix = "ndood.api")
@Data
public class ApiProperties {
	/**
	 * 系统默认域名
	 */
	private String domain = "http://18p88z8050.imwork.net";
	/**
	 * 默认的sessionid名称
	 */
	private String DefaultCookieName = "JSESSIONID";
	/**
	 * 是否开发模式
	 */
	private Boolean isDevelop = false;
	/**
	 * 是否启用数据库配置
	 */
	private Boolean isDbGlobalConfig = false;
	/**
	 * 默认全局配置信息
	 */
	/**
	 * 支付相关配置
	 */
	private PayConfigProperties pay = new PayConfigProperties();
	/**
	 * oss相关配置
	 */
	private AliyunOssProperties oss = new AliyunOssProperties();
	/**
	 * IdWorker相关配置
	 */
	private IdWorkerProperties id = new IdWorkerProperties();
	/**
	 * 对账相关配置
	 */
	private ReconProperties recon = new ReconProperties();
	/**
	 * 轮询相关配置
	 */
	private PollingProperties polling = new PollingProperties();
}