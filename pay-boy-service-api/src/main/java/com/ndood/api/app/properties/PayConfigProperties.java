package com.ndood.api.app.properties;

import lombok.Data;

/**
 * 微信支付相关配置
 */
@Data
public class PayConfigProperties {
	/**
	 * 微信商户ID
	 */
	private String wxMchId;
	/**
	 * 微信APPID
	 */
	private String wxAppId;
	/**
	 * 微信APPSECRET
	 */
	private String wxAppSecret;
	/**
	 * 微信APISECRET
	 */
	private String wxApiSecret;
	/**
	 * 微信证书
	 */
	private String wxCertStream;
	/**
	 * 微信三方支付网关
	 */
	private String wxGateway = "http://api.wxpay.com";
	/**
	 * 支付宝APPID
	 */
	private String alipayAppId = "2016080600181116";
	/**
	 * 
	 */
	private String alipayMchId = "2088102170216031";
	/**
	 * 支付宝商户公钥
	 */
	private String alipayMchRsa2PublicSecret = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAt/b7rI18hpSGuT8k9k4eFi3D9Gl6FMFMEkWmx+/4C3qt/2+WRCPayEOBigaR2/e9RqMaoDjKr9qZ7yNh16+psZra4dU0Cy7MsG4a5zWBVisBCcv1Pesyc/KHYJXccegRlod4QBoTzrMgoDaJFxs8ES8V4a/HuOYta2ccyiQLSfBwY9gLq4H+edzAs3O0l/AodMR88jDAV3yQYzjcThAUAT8jLWih5uFCwb3pArelY5fTJx6YQbrGpzi3rS1HqZnMzNDTv33gytH+AfIN2o/pg4OJDtDMCpEfx1D1QK7GAYQ9LygkIVbpAp+FWHDSjSXcc8Q0dU+mdYjQUmf6/eNZywIDAQAB";
	/**
	 * 支付宝商户私钥
	 */
	private String alipayMchRsa2PrivateSecret = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC39vusjXyGlIa5PyT2Th4WLcP0aXoUwUwSRabH7/gLeq3/b5ZEI9rIQ4GKBpHb971GoxqgOMqv2pnvI2HXr6mxmtrh1TQLLsywbhrnNYFWKwEJy/U96zJz8odgldxx6BGWh3hAGhPOsyCgNokXGzwRLxXhr8e45i1rZxzKJAtJ8HBj2Aurgf553MCzc7SX8Ch0xHzyMMBXfJBjONxOEBQBPyMtaKHm4ULBvekCt6Vjl9MnHphBusanOLetLUepmczM0NO/feDK0f4B8g3aj+mDg4kO0MwKkR/HUPVArsYBhD0vKCQhVukCn4VYcNKNJdxzxDR1T6Z1iNBSZ/r941nLAgMBAAECggEBAJtSX7mXl+z02C7t3mJLIGHvcAQbTiTZ80V2I1OqC0593QzPBRsw/paZUHTeCSakq91I9sESUidZtSBQSlrWCLtYmLeV0CQk9lN8EyDlbvb2xiCkYMJPo0B8I8rrnL1biAYFCQVuPw0E9mXuxZ6JsxR7Icl0JXe6d9y3qXoOJq3ccgtac6Hf7Vby0eqz0ie3gr+zYLFVc//2K6DUFSZ8CjlYd18Iev+1sWhnuFDEWLsyDS0rRykJ/ibrCctnGSCBJa9nP3AZYW2NOzYkXhBJAn8I+w3wo+dbCo1eC9R/0jDoJdkvb089Lw+vBt90LHin0xoFvAHzk69GzFk3PhSeqIkCgYEA4KSZdwo/wgM79Ll+vtC0PwH8qM4M7BMEnOfivCMfzirBWgU+3xKlVgP7DZ+ZecgXLrGdg7jIQMYIbNHTE8fprTwAwznOM5srDZ1ourDXZYL4HCRrpv4t7zgJdustHsIrjgTsVK/oj6GhQvTOpJ7lEh7H7pLvL7+diNhBultNuycCgYEA0aTJosDSr1FS2Xzy+wbh/2jXmnRKEz8daDfvDRvA3uVMNYpWv9NWhH25hsxKL1RL6X7AJJn3uOklOPILxp0sIJ4zxr3D6kejO/mEc8D+hB9JJmJsNJ3fqsD5h+YvNbyYKVKLFpwlQywQs+dSrWplP6JCMZND103Xy0JfsuNTIr0CgYBqaC01QMqQmL2WnyLVOcCXM9a9EtT2IdAGjqRMn4qaOpBZlrCAM7HTO5F4k4e8MrYgbQuCcE2SuPsWqJEGBqS+ZPSrhL8SnMZY7Z3FCK0OrWne0rWXZUDW1kFNbfpyXmEJJXXYa3aKSnI7aABV49n3qlLjw/++K1JwlpcTsdnw8wKBgA2mSAaTvWPyzI0iiIN1mENT5S3GQohsRJLjiWn2E6Gvlj2qKynELc7kjK7NIEtilqq2OPZpG+rrKyRBBilBVH0vL7nR4y6eyW7/OPN8blXV36JjKB5o5QYuGFET2KEMDXArUqj/M6Hi/ZdpCUYHNl5lRyAJMZKUG+e9A5QXHvOVAoGASCIGzsjxraAxNN6nOZltlsuadHe3iQVhV7Z5ZKnmB0qRCmALj9IBweSBai80FFUDJPgvzQYcK8I4PwmldEPxHCgMnoAeVFAtGbz6zenKEWfdoSZnePggFKImqYAn3a4eUv+QZzmL7sPAf98bzLx1wVyXt1YEeCL3S1WMYe5W3VA=";
	/**
	 * 支付宝官方公钥
	 */
	private String alipayRsa2PublicSecret = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsYuB/Y5J2TIcTwgvgzxJXz+bWQO/OAzhvlQ4nT9ynG1z/0K5JGQqE4fBBB26BuRUcnMKuA9YLGUdiBDizXQfS2j1CPXfR5F9t2a/LgIg+URrYt7sr3waQcGiK26MqhUP8qWWFtOG6L3Xd7DyoUAUQPhj3ikKqcFqf+EOKfZdY9Lj0Ohf2fCmhOBSv4SM10nsjHcoEkYdhQ0jklTi4wNDUVHGWn9Yi0/5EGi4br3YREi+74gu4ZuuKKYeiDG4BgR0vU4ShoFBdA+IZzP5QkFGVKp6am45a+5ep4tjSc98OSzMx5EBTv762UiN06R0ILwn0MYyC8GGEPYVcned2z1pCQIDAQAB";
	/**
	 * 支付宝网关
	 */
	private String alipayGateway = "https://openapi.alipaydev.com/gateway.do";
}