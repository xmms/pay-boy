package com.ndood.api.app.properties;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ReconProperties {

	/**
	 * 对账文件下载陆军
	 */
	private String reconZipDir = "/recon/zip";
	
	/**
	 * 对账文件下载陆军
	 */
	private String reconFileDir = "/recon/file";
}
