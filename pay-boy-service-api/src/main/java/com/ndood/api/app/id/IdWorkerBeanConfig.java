package com.ndood.api.app.id;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ndood.api.app.properties.ApiProperties;
 
/**
 * @author : zhangxin
 * @version : V1.0
 * @since : 2018/09/15
 */
@Configuration
public class IdWorkerBeanConfig {
	
	@Autowired
	private ApiProperties apiProperties;
	
    /**
     * 配置全局id生成器
     */
    @Bean
    public SnowflakeIdWorker getIdWorker() {
        return new SnowflakeIdWorker(apiProperties.getId().getWorkspaceId(), apiProperties.getId().getMachineId());
    }
    
}
