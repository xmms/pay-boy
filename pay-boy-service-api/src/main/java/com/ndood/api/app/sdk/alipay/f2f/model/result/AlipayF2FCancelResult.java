package com.ndood.api.app.sdk.alipay.f2f.model.result;

import com.alipay.api.response.AlipayTradeCancelResponse;
import com.ndood.api.app.sdk.alipay.f2f.model.TradeStatus;

/**
 * Created by liuyangkly on 15/8/27.
 */
public class AlipayF2FCancelResult implements Result {
    private TradeStatus tradeStatus;
    private AlipayTradeCancelResponse response;

    public AlipayF2FCancelResult(AlipayTradeCancelResponse response) {
        this.response = response;
    }

    public void setResponse(AlipayTradeCancelResponse response) {
        this.response = response;
    }

    public AlipayTradeCancelResponse getResponse() {
        return response;
    }
    
    public TradeStatus getTradeStatus() {
		return tradeStatus;
	}

	public void setTradeStatus(TradeStatus tradeStatus) {
		this.tradeStatus = tradeStatus;
	}

	@Override
    public boolean isTradeSuccess() {
        return response != null &&
                TradeStatus.SUCCESS.equals(tradeStatus);
    }
    
}
