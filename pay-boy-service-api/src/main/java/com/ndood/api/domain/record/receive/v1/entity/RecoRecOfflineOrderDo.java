package com.ndood.api.domain.record.receive.v1.entity;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

/**
 * 收款-平台-记录-线下订单
 */
@Getter @Setter
public class RecoRecOfflineOrderDo {
    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 外部订单号
     */
    private String outOrderNo;

    /**
     * 代理商ID
     */
    private Integer agentId;

    /**
     * 代理商名称
     */
    private String agentName;

    /**
     * 商户ID
     */
    private Integer mchId;

    /**
     * 商户名称
     */
    private String mchName;

    /**
     * 门店开放ID
     */
    private Integer shopId;

    /**
     * 门店全称 店名-分店名
     */
    private String shopName;

    /**
     * 客户端IP
     */
    private String clientIp;

    /**
     * 设备编号
     */
    private String deviceNo;

    /**
     * 商品标题
     */
    private String subject;

    /**
     * 商品描述信息
     */
    private String body;

    /**
     * 特定渠道发起额外参数
     */
    private String extra;

    /**
     * 失效时间，单位分钟
     */
    private String expireTime;

    /**
     * 支付收银员ID
     */
    private Integer payCashierId;

    /**
     * 支付收银员姓名
     */
    private String payCashierName;

    /**
     * 支付备注
     */
    private String payRemark;

    /**
     * 退款人ID
     */
    private Integer refundCashierId;

    /**
     * 退款人姓名
     */
    private String refundCashierName;

    /**
     * 退款备注
     */
    private String refundRemark;

    private String lastPayTime;

    /**
     * 订单总金额 单位 元
     */
    private BigDecimal totalAmount;

    /**
     * 总实收金额
     */
    private BigDecimal receiptAmount;

    /**
     * 卖家优惠金额
     */
    private BigDecimal freeAmount;

    /**
     * 总退款金额 单位 元
     */
    private BigDecimal refundAmount;

    /**
     * 总实退金额 单位 元
     */
    private BigDecimal receiptRefundAmount;

    /**
     * 总剩余可退金额 单位 元
     */
    private BigDecimal canRefundAmount;

    /**
     * 退款时间
     */
    private String lastRefundTime;
    
    /**
     * 是否人工处理
     */
    private Integer isManual;
    
    /**
     * 三方平台APPID
     */
    private String appid;

    /**
     * 状态： 1 已创建 2 成功 3 失败 4 撤销 5 退款 6 部分退款
     */
    private Integer status;

    /**
     * 扩展参数1
     */
    private String param1;

    /**
     * 扩展参数2
     */
    private String param2;

    /**
     * 扩展参数3
     */
    private String param3;

    /**
     * 扩展参数4
     */
    private String param4;
}
