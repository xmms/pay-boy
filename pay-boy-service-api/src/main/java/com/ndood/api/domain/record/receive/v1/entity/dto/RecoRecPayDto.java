package com.ndood.api.domain.record.receive.v1.entity.dto;

import com.ndood.api.domain.record.receive.v1.entity.RecoRecPayDo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecoRecPayDto extends RecoRecPayDo {

}
