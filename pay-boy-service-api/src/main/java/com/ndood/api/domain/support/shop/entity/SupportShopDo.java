package com.ndood.api.domain.support.shop.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * 支持服务门店领域对象 - 聚合根
 */
@Getter @Setter
public class SupportShopDo {

}
