package com.ndood.api.domain.support.staff.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ndood.api.base.dao.support_staff.ExtendSupportStaffDao;
import com.ndood.api.domain.support.staff.entity.dto.SupportStaffDto;

/**
 * 员工支持服务
 */
@Service
public class SupportStaffService {

	@Autowired
	private ExtendSupportStaffDao extendSupportStaffDao;
	
	/**
	 * 通过员工username获取支付相关信息
	 */
	public SupportStaffDto getStaffFullPayInfo(String username) {
		SupportStaffDto staffPayInfo = extendSupportStaffDao.getStaffFullPayInfo(username);
		return staffPayInfo;
	}

}
