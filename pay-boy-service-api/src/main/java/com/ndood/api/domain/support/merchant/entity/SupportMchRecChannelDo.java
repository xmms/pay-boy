package com.ndood.api.domain.support.merchant.entity;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class SupportMchRecChannelDo {
	/**
	 * 渠道编码
	 */
	private String channelNo;
	/**
	 * 渠道名称
	 */
	private String channelName;
	/**
	 * 支付方式
	 */
	private Integer payWay;
	/**
	 * 支付类型
	 */
	private Integer payType;
	/**
	 * 状态
	 */
	private Integer status;
	/**
	 * 优先级
	 */
	private Integer priority;
}
