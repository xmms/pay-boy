package com.ndood.api.domain.support.merchant.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * 商户直清支付配置
 */
@Getter
@Setter
public class SupportMchRecConfigDo {
    private Integer id;

    /**
     * 微信商户号
     */
    private String wxMchId;

    /**
     * 微信app_id
     */
    private String wxAppId;

    /**
     * 微信app_secret
     */
    private String wxAppSecret;

    private String wxApiSecret;

    private String wxCertStream;

    private String aliAppId;

    private String aliAlipayPublicRsa2Secret;

    private String aliMchPrivateRsa2Secret;
}
