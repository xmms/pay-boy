package com.ndood.api.domain.record.receive.v1.entity;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RecoRecPayDo {
    private Long id;
	/**
	 * 商户ID
	 */
	private Integer mchId;
	/**
	 * 商户名称
	 */
	private String mchName;
	/**
	 * 代理ID
	 */
	private Integer agentId;
	/**
	 * 代理名称
	 */
	private String agentName;
	/**
     * 支付订单ID
     */
    private String payOrderNo;

    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 支付方式： 1 支付宝 2 微信
     */
    private Integer payWay;

    /**
     * 支付类型：1 扫码 2 被扫 3门店码
     */
    private Integer payType;

    private Integer contractId;

    /**
     * 合同类型：1直清 2 二清 3 子商户
     */
    private Integer contractType;

    private String contractNo;

    private String contractName;

    /**
     * 支付产品编码
     */
    private String productNo;

    /**
     * 支付产品名称
     */
    private String productName;

    /**
     * 支付渠道编码
     */
    private Integer channelId;

    /**
     * 渠道编码
     */
    private String channelNo;

    /**
     * 支付渠道名称
     */
    private String channelName;

    /**
     * 总金额
     */
    private BigDecimal totalAmount;

    /**
     * 商家优惠金额
     */
    private BigDecimal freeAmount;

    /**
     * 商家实收金额
     */
    private BigDecimal receiptAmount;

    /**
     * 三方平台优惠金额
     */
    private BigDecimal thirdFreeAmount;

    /**
     * 买家付款金额
     */
    private BigDecimal buyerPayAmount;

    /**
     * 支付时间
     */
    private String payTime;
    
    /**
     * 三方平台APPID
     */
    private String appId;
    
    /**
     * 买家三方ID
     */
    private String buyerUserId;
    
    /**
     * 买家三方账号
     */
    private String buyerLogonId;
    /**
     * 状态：1 创建 2 成功 3 失败
     */
    private Integer status;

    private String param1;

    private String param2;

    private String param3;

    private String param4;

    private String param5;

    private String param6;
}
