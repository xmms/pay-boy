package com.ndood.api.domain.support.merchant.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ndood.api.base.dao.support_merchant.ExtendSupportMchRecContractDao;
import com.ndood.api.domain.support.merchant.entity.dto.SupportMchRecContractDto;

/**
 * 支持能力-商户-收款支付服务
 */
@Service
public class SupportMchRecContractService {
	
	@Autowired
	private ExtendSupportMchRecContractDao extendSupportMchRecContractDao;
	
	/**
	 * 1 根据商户ID查询出商户直清收款配置
	 * FIXME 加spring cache
	 */
	public SupportMchRecContractDto getMerchantReceiveContract(Integer merchantRecContractId){
		return extendSupportMchRecContractDao.extendFindMerchantReceiveContractByContractId(merchantRecContractId);
	}
	
}
