package com.ndood.api.domain.record.receive.v1.entity;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RecoRecRefundDo {
    private Long id;
	/**
	 * 商户ID
	 */
	private Integer mchId;
	/**
	 * 商户名称
	 */
	private String mchName;
	/**
	 * 代理ID
	 */
	private Integer agentId;
	/**
	 * 代理名称
	 */
	private String agentName;
    /**
     * 退款交易订单号
     */
    private String refundOrderNo;

    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 支付方式： 1 支付宝 2 微信
     */
    private Integer payWay;

    /**
     * 支付类型：1 扫码 2 被扫 3门店码
     */
    private Integer payType;

    /**
     * 合同ID
     */
    private Integer contractId;

    /**
     * 合同编码
     */
    private String contractNo;

    /**
     * 合同类型： 1 直清 2 二清 3子商户
     */
    private Integer contractType;

    /**
     * 合同名称
     */
    private String contractName;

    /**
     * 支付产品编码
     */
    private String productNo;

    /**
     * 支付产品名称
     */
    private String productName;

    /**
     * 支付渠道编码
     */
    private Integer channelId;

    /**
     * 渠道编码
     */
    private String channelNo;

    /**
     * 支付渠道名称
     */
    private String channelName;

    private BigDecimal refundAmount;

    private BigDecimal refundFreeAmount;

    private BigDecimal refundReceiptAmount;

    private BigDecimal refundThirdFreeAmount;

    private BigDecimal refundBuyerPayAmount;

    private String refundTime;

    /**
     * 三方平台APPID
     */
    private String appId;

    /**
     * 状态：1 创建 2 成功 3 失败
     */
    private Integer status;

    /**
     * 渠道应用appid
     */
    private String param1;

    /**
     * 跨公众号支付所需的sub_appid，来自商户合同
     */
    private String param2;

    /**
     * 渠道商户号
     */
    private String param3;

    /**
     * 跨公众号支付所需的sub_mch_id，来自商户合同
     */
    private String param4;

    /**
     * 跨公众号支付sub_open_id
     */
    private String param5;

    /**
     * 渠道返回报文
     */
    private String param6;
}
