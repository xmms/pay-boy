package com.ndood.api.domain.support.merchant.entity;

import java.time.LocalDate;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class SupportMerchantDo {
    private Integer id;

    /**
     * 代理商ID
     */
    private Integer agentId;

    /**
     * 商户名称
     */
    private String name;

    /**
     * 手机
     */
    private String mobile;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 企业QQ
     */
    private String qq;

    /**
     * 商户备注
     */
    private String remark;

    /**
     * 经营类目
     */
    private String industryId;

    /**
     * 网站名称
     */
    private String websiteName;

    /**
     * 网站地址
     */
    private String websiteUrl;

    /**
     * 营业执照类型：1 三证合一 2 非三证合一,
     */
    private Integer businessLicenseType;

    /**
     * 营业执照号码
     */
    private String businessLicenseNo;

    /**
     * 营业执照生效时间
     */
    private LocalDate businessLicenseNoStart;

    /**
     * 营业执照失效时间
     */
    private LocalDate businessLicenseNoEnd;

    /**
     * 税务登记号
     */
    private String taxNo;

    /**
     * 税务登记号生效时间
     */
    private LocalDate taxNoStart;

    /**
     * 税务登记号失效时间
     */
    private LocalDate taxNoEnd;

    /**
     * 组织机构代码
     */
    private String orgNo;

    /**
     * 组织机构代码生效时间
     */
    private LocalDate orgNoStart;

    /**
     * 组织机构代码失效时间
     */
    private LocalDate orgNoEnd;

    /**
     * 法人姓名
     */
    private String legalPersonName;

    /**
     * 法人性别：1 男 2 女
     */
    private Integer legalPersonSex;

    /**
     * 法人证件类型
     */
    private Integer legalPersonCertType;

    /**
     * 法人证件号码
     */
    private String legalPersonCertNo;

    private String legalPersonCertRegion;

    /**
     * 法人证件生效时间
     */
    private Date legalPersonCertStart;

    /**
     * 法人证件失效时间
     */
    private Date legalPersonCertEnd;

    private String legalPersonMobile;

    private String legalPersonEmail;

    /**
     * 法人职业
     */
    private Integer legalPersonJob;

    private String legalPersonAddress;

    /**
     * 签约账户类型：1 对私 2 对公
     */
    private Integer accountType;

    /**
     * 账户人身份 1 法人 2 法人亲属
     */
    private Integer accountBossType;

    /**
     * 开户银行
     */
    private String accountBankName;

    /**
     * 开户人姓名
     */
    private String accountName;

    /**
     * 收款账号
     */
    private String accountNo;

    /**
     * 收款人手机
     */
    private String accountMobile;

    /**
     * 清算联行号
     */
    private String accountCleanBankNo;

    /**
     * 营业执照图片
     */
    private String licenceImgUrl;

    /**
     * 开户许可证图片
     */
    private String openingPermitImgUrl;

    /**
     * 身份证正面图片
     */
    private String idImgUrl;

    /**
     * 身份证反面图片
     */
    private String idReverseImgUrl;

    /**
     * 税务登记证正面图片
     */
    private String taxImgUrl;

    /**
     * 组织机构代码图片
     */
    private String orgImgUrl;

    /**
     * 授权文件图片
     */
    private String authorizeImgUrl;

    /**
     * 状态: 0 待审核 1 启用 3 禁用
     */
    private Integer status;

    /**
     * 逻辑删除: 0 未删除 1 删除
     */
    private Integer logicDelete;

    private Date createTime;

    private Date updateTime;
    
    private Integer riskDay;
}
