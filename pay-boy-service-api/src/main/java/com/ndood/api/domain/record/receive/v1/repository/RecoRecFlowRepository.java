package com.ndood.api.domain.record.receive.v1.repository;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ndood.api.domain.record.receive.v1.entity.dto.RecoRecFlowDto;
import com.ndood.common.base.dao.record.RRecFlowMapper;
import com.ndood.common.base.pojo.record.RRecFlowPo;

@Repository
public class RecoRecFlowRepository {
	
	@Autowired
	private RRecFlowMapper recFlowMapper;
	
	public void insertRecFlow(RecoRecFlowDto dto) {
		RRecFlowPo po = new RRecFlowPo();
		po.setFlowNo(dto.getFlowNo());
		po.setSettleAgentRate(dto.getSettleAgentRate());
		po.setSettleAgentFee(dto.getSettleAgentFee());
		po.setSettleProfit(dto.getSettleProfit());
		po.setSettlePlatformProfit(dto.getSettlePlatformProfit());
		po.setSettleAgentProfit(dto.getSettleAgentProfit());
		po.setSettleMchProfit(dto.getSettleMchProfit());
		po.setSettlePlatformRate(dto.getSettlePlatformRate());
		po.setTradeNo(dto.getTradeNo());
		po.setMchId(dto.getMchId());
		po.setTradeType(dto.getTradeType());
		po.setAgentName(dto.getAgentName());
		po.setContractId(dto.getContractId());
		po.setChannelName(dto.getChannelName());
		po.setTradeAmount(dto.getTradeAmount());
		po.setTradeTime(dto.getTradeTime());
		po.setSettleRiskDay(dto.getSettleRiskDay());
		po.setShopName(dto.getShopName());
		po.setSettleRate(dto.getSettleRate());
		po.setContractNo(dto.getContractNo());
		po.setOrderNo(dto.getOrderNo());
		po.setPayType(dto.getPayType());
		po.setAgentId(dto.getAgentId());
		po.setSettleDate(dto.getSettleDate());
		po.setContractName(dto.getContractName());
		po.setSettleType(dto.getSettleType());
		po.setShopId(dto.getShopId());
		po.setContractType(dto.getContractType());
		po.setPayWay(dto.getPayWay());
		po.setSettleStatus(dto.getSettleStatus());
		po.setChannelId(dto.getChannelId());
		po.setChannelNo(dto.getChannelNo());
		po.setMchName(dto.getMchName());
		po.setProductNo(dto.getProductNo());
		po.setProductName(dto.getProductName());

		po.setUpdateTime(new Date());
		po.setCreateTime(new Date());
		recFlowMapper.insert(po);
	}

}
