package com.ndood.api.domain.record.receive.v1.entity;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecoRecFlowDo {
	/**
	 * 流水号
	 */
	private String flowNo;
    /**
     * 交易号
     */
    private String tradeNo;

    /**
     * 交易类型：1 支付 2 退款
     */
    private Integer tradeType;

    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 代理商ID
     */
    private Integer agentId;

    /**
     * 代理商名称
     */
    private String agentName;

    /**
     * 商户ID
     */
    private Integer mchId;

    /**
     * 商户名称
     */
    private String mchName;

    /**
     * 门店开放ID
     */
    private Integer shopId;

    /**
     * 门店全称 店名-分店名
     */
    private String shopName;

    /**
     * 支付方式： 1 支付宝 2 微信
     */
    private Integer payWay;

    /**
     * 支付类型：1 扫码 2 被扫 3门店码
     */
    private Integer payType;

    /**
     * 合同ID
     */
    private Integer contractId;

    /**
     * 合同类型： 1直清 2 二清 3 子商户
     */
    private Integer contractType;

    /**
     * 合同编码
     */
    private String contractNo;

    /**
     * 合同名称
     */
    private String contractName;

    /**
     * 产品编码
     */
    private String productNo;

    /**
     * 产品名称
     */
    private String productName;

    /**
     * 渠道ID
     */
    private Integer channelId;

    /**
     * 渠道编码
     */
    private String channelNo;

    /**
     * 渠道名称
     */
    private String channelName;

    /**
     * 实际交易金额
     */
    private BigDecimal tradeAmount;

    /**
     * 交易时间
     */
    private String tradeTime;
    
    /**
     * 三方平台APPID
     */
    private String appid;

    /**
     * 结算类型：1 费率 2 固定金额
     */
    private Integer settleType;

    /**
     * 商户风险预存期
     */
    private Integer settleRiskDay;

    /**
     * 最早应结算日
     */
    private Date settleDate;

    /**
     * 签约费率
     */
    private BigDecimal settleRate;

    /**
     * 代理商渠道费率
     */
    private BigDecimal settleAgentRate;

    /**
     * 代理商渠道单笔结算金额
     */
    private BigDecimal settleAgentFee;

    /**
     * 平台费率
     */
    private BigDecimal settlePlatformRate;

    /**
     * 平台费用
     */
    private BigDecimal settlePlatformFee;
    
    /**
     * 总服务费  比如支付宝 收0.6%
     */
    private BigDecimal settleProfit;
    
    /**
     * 运营商分润收益 单位 元
     */
    private BigDecimal settlePlatformProfit;

    /**
     * 代理商分润收益 单位 元
     */
    private BigDecimal settleAgentProfit;

    /**
     * 商户收益
     */
    private BigDecimal settleMchProfit;

    /**
     * 结算标记：0 未结算 1 已结算
     */
    private Integer settleStatus;
}
