package com.ndood.api.domain.record.receive.v1.service;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ndood.api.app.tools.IdUtils;
import com.ndood.api.domain.record.receive.v1.entity.dto.RecoRecOfflineOrderDto;
import com.ndood.api.domain.record.receive.v1.entity.dto.RecoRecPayDto;
import com.ndood.api.domain.record.receive.v1.entity.dto.RecoRecRefundDto;
import com.ndood.api.domain.record.receive.v1.repository.RecoRecOfflineOrderRepository;
import com.ndood.api.domain.record.receive.v1.repository.RecoRecPayRepository;
import com.ndood.api.domain.record.receive.v1.repository.RecoRecRefundRepository;
import com.ndood.common.base.constaints.CommonConstaints;

@Service
public class RecoRecMockService {
	
	@Autowired
	private RecoRecOfflineOrderRepository offlineOrderRepository;
	
	@Autowired
	private RecoRecPayRepository payRepository;
	
	@Autowired
	private RecoRecRefundRepository refundRepository;
	
	@Autowired
	private IdUtils idUtils;

	/**
	 * 1 模拟一笔收款撤销的交易
	 * @param appid 
	 */
	public void mockAPlatformOfflineOfficialAlipayPayCancelTrade(String appid) {
		// Step1: 模拟一个订单
		RecoRecOfflineOrderDto order = new RecoRecOfflineOrderDto();
		order.setAgentId(1);
		order.setAgentName("测试代理");
		order.setAppid(appid);
		order.setBody("模拟收款撤销");
		order.setCanRefundAmount(new BigDecimal(0));
		order.setClientIp("127.0.0.1");
		order.setDeviceNo("123456");
		order.setExpireTime("5m");
		order.setExtra(null);
		order.setFreeAmount(new BigDecimal(0));
		order.setIsManual(0);
		order.setLastPayTime("2019-04-17 12:13:13");
		order.setLastRefundTime(null);
		order.setMchId(1);
		order.setMchName("测试商户");
		order.setOrderNo(idUtils.getNextId());
		order.setOutOrderNo(null);
		order.setPayCashierId(1);
		order.setPayCashierName("测试收银员");
		order.setPayRemark("测试收银");
		order.setReceiptAmount(new BigDecimal(1));
		order.setReceiptRefundAmount(new BigDecimal(0));
		order.setRefundAmount(new BigDecimal(0));
		order.setRefundCashierId(null);
		order.setRefundCashierName(null);
		order.setRefundRemark(null);
		order.setShopId(1);
		order.setShopName("测试门店");
		order.setStatus(CommonConstaints.REC_ORDER_STATUS_CANCEL);
		order.setSubject("模拟收款撤销");
		order.setTotalAmount(new BigDecimal(1));
		offlineOrderRepository.insertRecOfflineOrder(order);
		
		// Step2: 模拟一笔收款撤销订单
		RecoRecPayDto pay = new RecoRecPayDto();
		pay.setBuyerLogonId("135****3049");
		pay.setBuyerPayAmount(new BigDecimal(0));
		pay.setBuyerUserId("208803211");
		pay.setChannelId(1);
		pay.setChannelName("支付宝官方当面付扫码通道");
		pay.setChannelNo("CN_GF_ALI_SCAN");
		pay.setContractId(1);
		pay.setContractName("支付宝官方当面付合同");
		pay.setContractNo("C_ALI_F2F");
		pay.setContractType(1);
		pay.setFreeAmount(new BigDecimal(0));
		pay.setMchId(1);
		pay.setMchName("测试商户");
		pay.setAgentId(1);
		pay.setAgentName("测试代理");
		pay.setOrderNo(order.getOrderNo());
		pay.setPayOrderNo(idUtils.getNextId());
		pay.setPayTime(null);
		pay.setPayType(1);
		pay.setPayWay(1);
		pay.setProductName("支付宝扫当面付码支付");
		pay.setProductNo("P_ALI_SCAN");
		pay.setReceiptAmount(new BigDecimal(0));
		pay.setStatus(CommonConstaints.REC_CHANNEL_PAY_STATUS_CANCEL);
		pay.setThirdFreeAmount(new BigDecimal(0));
		pay.setTotalAmount(new BigDecimal(1));
		payRepository.insertRecPay(pay);
	}

	/**
	 * 2 模拟一笔退款失败交易
	 */
	public void mockAPlatformOfflineOfficialAlipayRefundFailedTrade(String appid) {
		// Step1: 模拟一个订单
		RecoRecOfflineOrderDto order = new RecoRecOfflineOrderDto();
		order.setAgentId(1);
		order.setAgentName("测试代理");
		order.setAppid(appid);
		order.setBody("模拟退款失败");
		order.setCanRefundAmount(new BigDecimal(1));
		order.setClientIp("127.0.0.1");
		order.setDeviceNo("123456");
		order.setExpireTime("5m");
		order.setExtra(null);
		order.setFreeAmount(new BigDecimal(0));
		order.setIsManual(0);
		order.setLastPayTime("2019-04-17 12:16:16");
		order.setLastRefundTime(null);
		order.setMchId(1);
		order.setMchName("测试商户");
		order.setOrderNo(idUtils.getNextId());
		order.setOutOrderNo(null);
		order.setPayCashierId(1);
		order.setPayCashierName("测试收银员");
		order.setPayRemark("测试收银");
		order.setReceiptAmount(new BigDecimal(1));
		order.setReceiptRefundAmount(new BigDecimal(0));
		order.setRefundAmount(new BigDecimal(0));
		order.setRefundCashierId(null);
		order.setRefundCashierName(null);
		order.setRefundRemark(null);
		order.setShopId(1);
		order.setShopName("测试门店");
		order.setStatus(CommonConstaints.REC_ORDER_STATUS_SUCCESS);
		order.setSubject("模拟退款失败");
		order.setTotalAmount(new BigDecimal(1));
		offlineOrderRepository.insertRecOfflineOrder(order);
		
		// Step2: 模拟一笔收款成功流水
		RecoRecPayDto pay = new RecoRecPayDto();
		pay.setBuyerLogonId("135****3049");
		pay.setBuyerPayAmount(new BigDecimal(0));
		pay.setBuyerUserId("208803211");
		pay.setChannelId(1);
		pay.setChannelName("支付宝官方当面付扫码通道");
		pay.setChannelNo("CN_GF_ALI_SCAN");
		pay.setContractId(1);
		pay.setContractName("支付宝官方当面付合同");
		pay.setContractNo("C_ALI_F2F");
		pay.setContractType(1);
		pay.setFreeAmount(new BigDecimal(0));
		pay.setMchId(1);
		pay.setMchName("测试商户");
		pay.setAgentId(1);
		pay.setAgentName("测试代理");
		pay.setOrderNo(order.getOrderNo());
		pay.setPayOrderNo(idUtils.getNextId());
		pay.setPayTime("2019-04-17 12:16:19");
		pay.setPayType(1);
		pay.setPayWay(1);
		pay.setProductName("支付宝扫当面付码支付");
		pay.setProductNo("P_ALI_SCAN");
		pay.setReceiptAmount(new BigDecimal(1));
		pay.setStatus(CommonConstaints.REC_CHANNEL_PAY_STATUS_SUCCESS);
		pay.setThirdFreeAmount(new BigDecimal(0));
		pay.setTotalAmount(new BigDecimal(1));
		payRepository.insertRecPay(pay);
	
		// Step3: 模拟一笔退款失败流水
		RecoRecRefundDto refund = new RecoRecRefundDto();
		refund.setAgentId(1);
		refund.setAgentName("测试代理");
		refund.setAppId(appid);
		refund.setChannelId(1);
		refund.setChannelName("支付宝官方当面付扫码通道");
		refund.setChannelNo("CN_GF_ALI_SCAN");
		refund.setContractId(1);
		refund.setContractName("支付宝官方当面付合同");
		refund.setContractNo("C_ALI_F2F");
		refund.setContractType(1);
		refund.setMchId(1);
		refund.setMchName("测试商户");
		refund.setOrderNo(order.getOrderNo());
		refund.setPayType(1);
		refund.setPayWay(1);
		refund.setProductName("支付宝扫当面付码支付");
		refund.setProductNo("P_ALI_SCAN");
		refund.setRefundAmount(new BigDecimal(1));
		refund.setRefundBuyerPayAmount(new BigDecimal(0));
		refund.setRefundFreeAmount(new BigDecimal(0));
		refund.setRefundOrderNo(idUtils.getNextId());
		refund.setRefundReceiptAmount(new BigDecimal(0));
		refund.setRefundThirdFreeAmount(new BigDecimal(0));
		refund.setRefundTime("2019-04-17 12:16:20");
		refund.setStatus(CommonConstaints.REC_CHANNEL_REFUND_STATUS_FAILED);
		refundRepository.insertRecRefund(refund);
	}

}
