package com.ndood.api.domain.record.receive.v1.entity.dto;

import com.ndood.api.domain.record.receive.v1.entity.RecoRecFlowDo;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RecoRecFlowDto extends RecoRecFlowDo{
	
}
