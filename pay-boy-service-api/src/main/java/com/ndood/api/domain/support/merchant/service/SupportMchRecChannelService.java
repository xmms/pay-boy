package com.ndood.api.domain.support.merchant.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ndood.api.base.dao.support_merchant.ExtendSupportMchRecChannelDao;
import com.ndood.api.domain.support.merchant.entity.dto.SupportMchRecChannelDto;

/**
 * 支持能力-商户-收款支付服务
 */
@Service
public class SupportMchRecChannelService {
	
	@Autowired
	private ExtendSupportMchRecChannelDao extendSupportMchRecChannelDao;
	
	/**
	 * 1 根据商户名，产品编码查询出商户下所有可用的支付渠道，按优先级排序。 3服务商 > 2直清 > 1二清； 同一类型看sort字段
	 */
	public List<SupportMchRecChannelDto> getMerchantReceiveChannel(Integer merchantId, String productNo){
		List<SupportMchRecChannelDto> channels = extendSupportMchRecChannelDao
				.extendFindMerchantReceiveChannelByMerchantIdAndProductNo(merchantId, productNo);
		return channels;
	}
	
}
