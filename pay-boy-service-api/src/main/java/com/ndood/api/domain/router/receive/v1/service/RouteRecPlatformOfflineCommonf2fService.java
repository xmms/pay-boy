package com.ndood.api.domain.router.receive.v1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ndood.api.base.constaints.ApiCode;
import com.ndood.api.base.exception.ApiException;
import com.ndood.api.domain.support.merchant.entity.dto.SupportMchRecChannelDto;
import com.ndood.api.domain.support.staff.entity.dto.SupportStaffDto;
import com.ndood.common.base.constaints.CommonConstaints;

/**
 * 平台支付-线下渠道路由-支付宝当面付路由 渠道路由的职责是根据支付类型选择对应的渠道
 */
@Service
public class RouteRecPlatformOfflineCommonf2fService {

	@Autowired
	private RouteRecPlatformOfflineAlif2fService routeRecPlatformOfflineAlif2fService;
	
	public SupportMchRecChannelDto routeBarCode(String amount, String barCode, SupportStaffDto staff)
			throws ApiException {

		// Step1: 根据条形码编码判断支付类型
		if (barCode.startsWith("25") || barCode.startsWith("28") || barCode.startsWith("30")) { // 支付宝
			return routeRecPlatformOfflineAlif2fService.route(CommonConstaints.P_ALI_BAR, amount, staff);

		} else if (barCode.startsWith("10") || barCode.startsWith("11") || barCode.startsWith("12")
				|| barCode.startsWith("13") || barCode.startsWith("14") || barCode.startsWith("15")) { // 微信
			throw new ApiException(ApiCode.ERR_SERVER, "不支持的支付授权码：" + barCode);
		} else if (barCode.startsWith("91")) { // QQ
			throw new ApiException(ApiCode.ERR_SERVER, "不支持的支付授权码：" + barCode);
		} else if (barCode.startsWith("18")) { // 京东
			throw new ApiException(ApiCode.ERR_SERVER, "不支持的支付授权码：" + barCode);
		} else if (barCode.startsWith("51")) { // 翼支付
			throw new ApiException(ApiCode.ERR_SERVER, "不支持的支付授权码：" + barCode);
		} else {
			throw new ApiException(ApiCode.ERR_SERVER, "不支持的支付授权码：" + barCode);
		}

	}

}
