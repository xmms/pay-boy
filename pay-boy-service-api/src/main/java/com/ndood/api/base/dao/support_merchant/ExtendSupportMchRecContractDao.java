package com.ndood.api.base.dao.support_merchant;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.ndood.api.domain.support.merchant.entity.dto.SupportMchRecContractDto;

public interface ExtendSupportMchRecContractDao {

	/**
	 * 1 查询出商户结算合同
	 */
	@Select("SELECT c.`id`,c.`settle_type`,c.`name`,c.`rate`,c.`platform_rate`,c.`platform_fee`,c.`agent_rate`,c.`agent_fee`,c.`end_time`, m.`risk_day` "
			+ "FROM m_rec_contract c "
			+ "JOIN m_merchant m ON m.`id` = c.`mch_id` "
			+ "WHERE c.`id` = #{merchantRecContractId} ")
	SupportMchRecContractDto extendFindMerchantReceiveContractByContractId(@Param("merchantRecContractId") Integer merchantRecContractId);

}
