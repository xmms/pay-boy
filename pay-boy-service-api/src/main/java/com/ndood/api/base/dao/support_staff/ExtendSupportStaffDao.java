package com.ndood.api.base.dao.support_staff;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.ndood.api.domain.support.staff.entity.dto.SupportStaffDto;

public interface ExtendSupportStaffDao {

	/**
	 * 1 根据员工username获取所有支付信息
	 */
	@Select("SELECT s.id, s.agent_id, a.name as agentName, s.mch_id, m.name as merchantName, s.email, s.mobile, s.nick_name, s.shop_id, sp.name as shopName "
			+ " FROM m_staff s "
			+ " LEFT JOIN m_merchant m ON s.mch_id = m.id AND m.status = 1 "
			+ " LEFT JOIN a_agent a ON s.agent_id = a.id AND a.status = 1 "
			+ " LEFT JOIN m_shop sp ON s.shop_id = sp.id AND sp.status = 1"
			+ " WHERE s.mobile = #{username} "
			+ " OR s.email = #{username} ")
	SupportStaffDto getStaffFullPayInfo(@Param("username") String username);

}
