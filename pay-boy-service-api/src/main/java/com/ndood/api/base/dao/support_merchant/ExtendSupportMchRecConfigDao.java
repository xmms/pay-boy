package com.ndood.api.base.dao.support_merchant;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.ndood.api.domain.record.receive.v1.entity.dto.ReconAppDto;
import com.ndood.api.domain.support.merchant.entity.dto.SupportMchRecConfigDto;

public interface ExtendSupportMchRecConfigDao {

	/**
	 * 1 根据商户ID获取商户支付配置
	 */
	@Select("`id`, `agent_id`, `mch_id`, `wx_mch_id`, `wx_app_id`, `wx_app_secret`, `wx_api_secret`, `wx_cert_stream`, `ali_app_id`, `ali_alipay_public_rsa2_secret`, "
			+ "`ali_mch_private_rsa2_secret`, `create_time`, `update_time` "
			+ "FROM m_rec_config WHERE `mch_id`= #{merchantId} LIMIT 1")
	SupportMchRecConfigDto extendFindMerchantReceiveConfigByMerchantId(@Param("merchantId") Integer merchantId);

	/**
	 * 2 查询出官方支付宝app信息
	 */
	@Select("SELECT mch_id, ali_app_id as appId, ali_alipay_public_rsa2_secret as alipayPublicRsa2Secret, ali_mch_private_rsa2_secret as mchPrivateRsa2Secret FROM m_rec_config WHERE ali_app_id IN (${appIds})")
	List<ReconAppDto> extendFindMerchantOfficialAlipayReceiveConfigByAppIds(@Param("appIds") String appIds);
}
