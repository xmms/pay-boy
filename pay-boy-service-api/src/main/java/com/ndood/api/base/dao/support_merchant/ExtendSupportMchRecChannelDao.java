package com.ndood.api.base.dao.support_merchant;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.ndood.api.domain.support.merchant.entity.dto.SupportMchRecChannelDto;

public interface ExtendSupportMchRecChannelDao {

	/**
	 * 1 根据商户ID，渠道编码获取所有支付渠道，根据合同类型和优先级排序 
	 */
	@Select("SELECT c.`id`,c.`agent_id`,c.`mch_id`,c.`contract_id`,c.`contract_no`,c.`contract_type`,c.`contract_name`,c.`product_no`,c.`product_name`,c.`channel_no`,c.`channel_name`,"
			+ "c.`pay_way`,c.`pay_type`,c.`status`,c.`priority`, co.settle_type, co.rate, co.platform_rate, co.platform_fee, co.agent_rate, co.agent_fee "
			+ " FROM m_rec_channel c "
			+ " JOIN m_rec_contract co ON c.`contract_id` = co.id "
			+ " WHERE c.`status` = 1 AND c.`mch_id` = #{merchantId} AND c.`product_no`= #{productNo} "
			+ " ORDER BY c.contract_type DESC, c.`priority` DESC")
	List<SupportMchRecChannelDto> extendFindMerchantReceiveChannelByMerchantIdAndProductNo(@Param("merchantId") Integer merchantId,
			@Param("productNo") String productNo);

}
