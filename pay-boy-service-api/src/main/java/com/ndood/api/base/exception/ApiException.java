package com.ndood.api.base.exception;

import com.ndood.api.base.constaints.ApiCode;

public class ApiException extends Exception {
	private static final long serialVersionUID = 13168926815908268L;

	private ApiCode code;
	
	private String message;

	public ApiCode getCode() {
		return code;
	}

	public void setCode(ApiCode code) {
		this.code = code;
	}

	public ApiException(String message) {
		super(message);
	}

	public ApiException(ApiCode code) {
        super(code.getValue());
        this.code = code;
    }
	
	public ApiException(ApiCode code, String message) {
		super(message);
        this.code = code;
        this.message = message;
    }

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}