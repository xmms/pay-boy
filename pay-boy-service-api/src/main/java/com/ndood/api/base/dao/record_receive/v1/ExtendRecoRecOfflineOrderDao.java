package com.ndood.api.base.dao.record_receive.v1;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.ndood.api.domain.record.receive.v1.entity.dto.RecoRecOfflineOrderDto;

public interface ExtendRecoRecOfflineOrderDao {

	/**
	 * 根据订单号查询出订单信息
	 */
	@Select("SELECT "
			+ "`order_no`, `out_order_no`, `agent_id`, `agent_name`, `mch_id`, `mch_name`, `shop_id`, `shop_name`, `client_ip`, "
			+ "`device_no`, `subject`, `body`, `extra`, `expire_time`, `pay_cashier_id`, `pay_cashier_name`, `pay_remark`, "
			+ "`refund_cashier_id`, `refund_cashier_name`, `refund_remark`, `last_pay_time`, `total_amount`, "
			+ "`receipt_amount`, `free_amount`, `refund_amount`, `receipt_refund_amount`, `can_refund_amount`, "
			+ "`last_refund_time`, `status`, `param1`, `param2`, `param3`, `param4`, `create_time`, `update_time` "
			+ "FROM r_rec_offline_order "
			+ "WHERE `order_no` = #{orderNo}")
	public RecoRecOfflineOrderDto extendFindOfflineOrderByOrderNo(@Param("orderNo") String orderNo);
}
