package com.ndood.merchant.controller.comm;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.api.client.util.Maps;

@Controller
public class IndexController {
	
	@GetMapping("/")
	public String root() {
		return "redirect:/index";
	}
	
	@GetMapping("/index")
	public String toIndex() {
		return "index";
	}
	
	@PostMapping("/test")
	@ResponseBody
	public Map<String,Object> test(@RequestBody Map<String,Object> maps) {
		Map<String,Object> map = Maps.newHashMap();
		map.put("a", maps.get("id"));
		map.put("code", 200);
		return map;
	}
}
