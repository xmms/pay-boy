package com.ndood.merchant.core.exception;

import com.ndood.merchant.core.constaints.MerchantCode;

public class MerchantException extends Exception {
	private static final long serialVersionUID = 13168926815908268L;

	private MerchantCode code;

	public MerchantCode getCode() {
		return code;
	}

	public void setCode(MerchantCode code) {
		this.code = code;
	}

	public MerchantException(String message) {
		super(message);
	}

	public MerchantException(MerchantCode code) {
        super(code.getValue());
        this.code = code;
    }
	
	public MerchantException(MerchantCode code, String message) {
        super(message);
        this.code = code;
    }

}