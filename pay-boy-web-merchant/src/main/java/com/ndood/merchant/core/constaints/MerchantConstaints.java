package com.ndood.merchant.core.constaints;

/**
 * 全局常量
 * @author ndood
 */
public class MerchantConstaints {

	/**
	 * 错误码
	 */
	public static final String SUCCESS = "10000";
	public static final String ERROR = "40400";
	
	/**
	 * 资源类型
	 */
	public static final Integer PERMISSION_TYPE_SYSTEM = 1;
	public static final Integer PERMISSION_TYPE_PAGE = 2;
	public static final Integer PERMISSION_TYPE_BUTTON = 3;
	
	/**
	 * 状态 1 可用 0 禁用
	 */
	public static final Integer STATUS_OK = 1;
	public static final Integer STATUS_FOBIDDEN = 0;
	
	/**
	 * 性别 1 男 2女
	 */
	public static final Integer SEX_MAN = 1;
	public static final Integer SEX_WOMAN = 2;
}
