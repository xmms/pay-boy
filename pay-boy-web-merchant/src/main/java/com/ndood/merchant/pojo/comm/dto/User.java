package com.ndood.merchant.pojo.comm.dto;

import java.util.Date;

import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonView;

/**
 * 创建User
 * @author ndood
 */
public class User {
	
	/**
	 * 添加JsonView视图
	 * @author ndood
	 *
	 */
	public interface UserSimpleView {};
	public interface UserDetailView extends UserSimpleView {};
	
	private String id;
	
	// 自定义validator
	private String username;

	// hibernate validator
	@NotBlank(message = "密码不能为空")
	private String password;
	
	// hibernate validator
	@Past(message = "生日必须是过去的时间")
	private Date birthday;

	// 添加JsonView视图
	@JsonView(UserSimpleView.class)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	// 添加JsonView视图
	@JsonView(UserDetailView.class)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@JsonView(UserSimpleView.class)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

}
