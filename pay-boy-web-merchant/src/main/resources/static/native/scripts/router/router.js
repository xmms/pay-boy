// 4 自定义路由
// https://router.vuejs.org/zh/guide/essentials/history-mode.html
define([
	'require',
	'vue',
	'vue-router',
	'layui',
	'scripts/utils/storage',
],function(require){
	
	//使用 路由
	var layer = require('layui');
	var Vue = require('vue');
	var VueRouter = require('vue-router');
	var storage = require('scripts/utils/storage');
    Vue.use(VueRouter);
    
	//返回路由实例
    var router = new VueRouter({
    	// mode: 'history',
        // 默认路由表
        routes:[],
    });
    
	// 全局路由守卫
    // https://blog.csdn.net/Servenity/article/details/80605833 
	router.beforeEach((to, from, next) => {
		console.log(to.path)
		// 判斷用戶是否登陸
		var accessToken = storage.getStore('access-token'); // 是否登录
		var isLogin = accessToken ? true : false;
		
		// to: Route: 即将要进入的目标 路由对象
		// from: Route: 当前导航正要离开的路由
		// next: Function: 一定要调用该方法来 resolve 这个钩子。执行效果依赖 next 方法的调用参数。
		const route = ['/content'];
		// 未登录状态；当路由到route指定页时，跳转至login
		if (route.indexOf(to.path) > -1) {  
			if (!isLogin) {
				router.push({ path:'/login',});
			}
		}
		// 已登录状态；当路由到login时，跳转至home 
		if (to.path === '/login') {
			if (isLogin) {
				router.push({ path:'/'});;
			}
		}
		next();
	});
	router.afterEach(function (transition) {
		layer.closeAll();
	})
	
	return router;
});