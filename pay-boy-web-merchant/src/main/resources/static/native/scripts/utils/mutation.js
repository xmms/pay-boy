// 监听dom变化
// https://www.cnblogs.com/jiangxiaobo/p/5370940.html
define(function() {
	// Firefox和Chrome早期版本中带有前缀
	var MutationObserver = window.MutationObserver
			|| window.WebKitMutationObserver || window.MozMutationObserver
	// 选择目标节点
	var target = document.querySelector('#app');
	// 创建观察者对象
	var observer = new MutationObserver(function(mutations) {
		mutations.forEach(function(mutation) {
			if(mutation.addedNodes.length){
				App.init();
				Layout.init();
				QuickNav.init();
			}
		});
	});
	// 配置观察选项:
	var config = {
		attributes : true,// 检测属性变动
		childList : true,// 检测子节点变动
		characterData : true
	// 节点内容或节点文本的变动。
	}
	// 传入目标节点和观察选项
	observer.observe(target, config);
	// /停止观察
	// observer.disconnect();
})