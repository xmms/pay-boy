define(function(){
	return {
		/**
		 * 写cookie
		 */
		setCookie: function (name,value,days){
		    var d = new Date;
		    d.setTime(d.getTime() + 24*60*60*1000*days);
		    window.document.cookie = name + "=" + value + ";path=/;expires=" + d.toGMTString();
		},
		/**
		 * 读cookie
		 */
		getCookie: function getCookie(name){
		    var v = window.document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
		    return v ? v[2] : null;
		},
		/**
		 * 移除cookie
		 */
		deleteCookie: function (name) {
		    this.set(name, '', -1);
		}
	}
})