define(function(){
	var vApp = {
		namespaced: true,
		state:{
			test: {a:222}
		},
		getters:{},
		actions:{},
		mutations: {
			updateTest (state,test) {
				state.test = test;
			}
		}
	}
	return vApp;
})