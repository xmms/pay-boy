// 3 自定义缓存空间
// https://vuex.vuejs.org/zh/guide/modules.html
define([
	'vue',
	'vuex',
	'scripts/vuex/modules/vApp'
],function(Vue,Vuex,vApp){
	
	var Vue = require('vue');
	var Vuex = require('vuex');
	Vue.use(Vuex);

	var vApp = require('scripts/vuex/modules/vApp');
	
    return new Vuex.Store({
    	modules: {
    		vApp : vApp,
    	},
	})
    
});