// https://blog.csdn.net/aitangyong/article/details/40708025
define([
	'require',
	'jquery',
	'bootstrap',
	'bootstrap-datetimepicker',
	'bootstrap-datetimepicker-zh-CN',
	'text!./loginPage.html',
	'css!./loginPage.css',
	'es6!./class'
],function(require){
	
	var $ = require('jquery');
	var Template = require('text!./loginPage.html');
	var App = {
		data: function(){
			return {};
		},
		template: Template,
		mounted: function(obj){
			var that = this;
			require(['domReady!'],function(){
				// 1 初始化日期控件
				$('#log_start_date').datetimepicker({
					language: 'zh-CN',
					format: 'yyyy-mm-dd hh:ii:00',
					autoclose: true
				});
			});
			
		},
        methods: {},
	};
	return App

});