package com.ndood.ddd.starter;

import java.io.FileNotFoundException;

import com.ndood.ddd.entity.DDDDomain;
import com.ndood.ddd.generator.DDDGenerator;
import com.ndood.ddd.parser.DDDParser;

public class DDDStarter {

	public static void main(String[] args) throws FileNotFoundException {
		// Step1: 获取配置文件内容到内存
		DDDDomain config = DDDParser.parseDomain();
		
		// Step2: 生成后端代码
		DDDGenerator.generatorBackend(config);
		
		// Step3: 生成前端代码
		DDDGenerator.generatorFrontend(config);
	}
}
