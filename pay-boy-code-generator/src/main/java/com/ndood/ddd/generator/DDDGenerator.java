package com.ndood.ddd.generator;

import java.io.File;
import java.util.List;

import com.ndood.ddd.entity.DDDAggregate;
import com.ndood.ddd.entity.DDDDomain;
import com.ndood.ddd.parser.ClientTemplate;
import com.ndood.ddd.parser.FileUtil;
import com.ndood.ddd.parser.TemplateUtil;

public class DDDGenerator {
	public static final String BACKEND_CODE_PATH = "D:\\ddd\\backend";
	public static final String FRONTEND_CODE_PATH = "D:\\ddd\\frontend";
	
	private static final String backendDomainTemplatePath = new File("").getAbsolutePath() + "\\src\\main\\resources\\ddd\\backend";
	private static final String frontendDomainTemplatePath = new File("").getAbsolutePath() + "\\src\\main\\resources\\ddd\\frontend";
	
	/**
	 * 1 生成后端代码
	 */
	public static void generatorBackend(DDDDomain domain) {
		// 获取工程级模板
		List<ClientTemplate> list = TemplateUtil.getTemplateList(backendDomainTemplatePath);
		
		for (ClientTemplate t : list) {
			
			if (FileUtil.isTemplatFile(t.getAllPath())) {

				if (t.getFileName().indexOf("[")>=0 && t.getFileName().indexOf("]")>=0){

					for(DDDAggregate aggregate: domain.getAggregates()) {
						
						// 替换聚合级模板部分
						String content = BackendGeneratorUtil.createContent(t.getFileName(), t.getAllPath(), domain, aggregate);

						// 替换文件路径
						String outPath = BackendGeneratorUtil.createOutPath(BACKEND_CODE_PATH + t.getPath(), t.getFileName(), domain, aggregate);
						
						// 保存文件
						FileUtil.saveContent(outPath, content);
					}
						
				} else{
					// 获取文件内容
					String content = BackendGeneratorUtil.createSingleContent(t.getFileName(), t.getAllPath(), domain);
					
					// 不用循环的文件 application.yml(TFILE)
					String outPath = BackendGeneratorUtil
							.createOutPath(BACKEND_CODE_PATH + t.getPath(), t.getFileName(), domain);
					// 保存文件
					FileUtil.saveContent(outPath, content);
				}
			}
			
		}
	}
	
	/**
	 * 2 生成前端代码
	 */
	public static void generatorFrontend(DDDDomain domain) {
		// 获取工程级模板
		List<ClientTemplate> list = TemplateUtil.getTemplateList(frontendDomainTemplatePath);
		
		for (ClientTemplate t : list) {
			
			if (FileUtil.isTemplatFile(t.getAllPath())) {

				if (t.getAllPath().contains("[aggregate]")){

					for(DDDAggregate aggregate: domain.getAggregates()) {
						
						// 替换聚合级模板部分
						String content = FrontendGeneratorUtil.createContent(t.getFileName(), t.getAllPath(), domain, aggregate);

						// 替换文件路径
						String outPath = FrontendGeneratorUtil.createOutPath(FRONTEND_CODE_PATH + t.getPath(), t.getFileName(), domain, aggregate);
						
						// 保存文件
						FileUtil.saveContent(outPath, content);
					}
						
				} else {
					// 获取文件内容
					String content = FrontendGeneratorUtil.createSingleContent(t.getFileName(), t.getAllPath(), domain);
					
					// 不用循环的文件 application.yml(TFILE)
					String outPath = FrontendGeneratorUtil
							.createOutPath(FRONTEND_CODE_PATH + t.getPath(), t.getFileName(), domain);
					// 保存文件
					FileUtil.saveContent(outPath, content);
				}
			}
			
		}
	}

}
