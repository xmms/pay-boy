package com.ndood.code.xml;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.ndood.code.entity.filter.ColumnFilter;
import com.ndood.code.entity.filter.TableFilter;

/**
 * tableMapXml阅读工具
 * 将tableMap加载到内存
 */
public class DataFilterMapXml {

	/**
	 * 从tableMap.xml中读取属性，并返回Map
	 */
	@SuppressWarnings({ "unchecked"})
	public static Map<String,TableFilter> readTableMap(String xmlPath){
		Map<String,TableFilter> filterMap = new HashMap<String,TableFilter>();
		
		try {
			SAXReader reader = new SAXReader();
			File file = new File(xmlPath + "\\dataFilterMap.xml");
			Document doc = reader.read(file);

			Element dbe = doc.getRootElement();
			List<Element> elist = dbe.elements();
			for (Element e : elist) {
				String name = e.attributeValue("name");
				String subpackage = e.attributeValue("subpackage");
				String title = e.attributeValue("title");
				
				TableFilter po = new TableFilter();
				po.setName(name);
				po.setTitle(title);
				po.setSubpackage(subpackage);

				List<Element> sdbeList = e.elements();
				HashMap<String,ColumnFilter> columnsMap = getColumnsMap(sdbeList);
				po.setColumnsMap(columnsMap);
				filterMap.put(name, po);
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		return filterMap;
	}
	
	/**
	 * 读取子map
	 */
	private static HashMap<String, ColumnFilter> getColumnsMap(List<Element> sdbeList) {

		HashMap<String, ColumnFilter> filterMap = new HashMap<String, ColumnFilter>();
		for (Element e : sdbeList) {
			String name = e.attributeValue("name");
			String componentType = e.attributeValue("componentType");
			String search = e.attributeValue("search");
			String hideColumn = e.attributeValue("hideColumn");
			ColumnFilter po = new ColumnFilter();
			po.setName(name);
			po.setComponentType(componentType);
			po.setSearch(search);
			po.setHideColumn(hideColumn);
			filterMap.put(name, po);
		}
		return filterMap;
		
	}

	public static void main(String[] args) {
		String filePath = "C:\\Users\\Administrator\\git\\pay-boy-taste\\垂直架构版\\pay-boy-taste\\pay-boy-code-generator\\src\\main\\resources";
		readTableMap(filePath);
	}
	
}
