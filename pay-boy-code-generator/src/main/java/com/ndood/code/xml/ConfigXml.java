package com.ndood.code.xml;

import java.util.HashMap;
import java.util.Map;

public class ConfigXml {
	
	public static Map<String, Map<String, String>> readConfig() {

		// mysql
		Map<String, String> mysqlMap = new HashMap<String, String>();
		mysqlMap.put("databaseTYPE", "MYSQL");
		mysqlMap.put("driverName", "com.mysql.jdbc.Driver");
		mysqlMap.put("url", "jdbc:mysql://[ip]:[port]/[db]?characterEncoding=UTF8");
		mysqlMap.put("dialect", "org.hibernate.dialect.MySQLInnoDBDialect");
		mysqlMap.put("generator", "<![CDATA[<generator class=\"native\"></generator>]]>");

		// oracle
		Map<String, String> oracleMap = new HashMap<String, String>();
		oracleMap.put("databaseTYPE", "ORACLE");
		oracleMap.put("driverName", "oracle.jdbc.driver.OracleDriver");
		oracleMap.put("url", "jdbc:oracle:thin:@[ip]:[port]:ORCL");
		oracleMap.put("dialect", "org.hibernate.dialect.Oracle10gDialect");
		oracleMap.put("generator",
				"<![CDATA[<generator class=\"org.hibernate.id.SequenceGenerator\"><param name=\"sequence\">[table]_seq</param></generator>]]>");

		Map<String, Map<String, String>> map = new HashMap<String, Map<String, String>>();
		map.put("MYSQL", mysqlMap);
		map.put("ORACLE", oracleMap);
		return map;
	}

}
