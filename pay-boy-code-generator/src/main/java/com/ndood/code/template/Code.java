package com.ndood.code.template;

import java.util.List;
import java.util.Map;

import com.ndood.code.entity.Table;
import com.ndood.code.entity.Templet;
import com.ndood.code.xml.DatabaseXml;

/**
 * 代码相关操作类
 * @author Administrator
 */
public class Code {

	/**
	 * 代码生成
	 */
	public static void create(Map<String, String> pathMap, Map<String, String> publicMap) {
		
		// Step1：读取pathMap到变量
		String projectTempletPath = pathMap.get("projectTempletPath");// 框架模板所在目录
		String tablleTempletPath = pathMap.get("tablleTempletPath");// 表级模板所在目录
		String columnTempletPath = pathMap.get("columnTempletPath");// 列级模板所在目录
		String xmlPath = pathMap.get("xmlPath");// 数据库信息文件../../db.xml
		String codePath = pathMap.get("codePath");// 代码输出目录

		// Step2：读取数据库全局属性：databaseTYPE,dialect,generator,driverName,url
		Map<String, String> propertyMap = DatabaseXml.readProperty(xmlPath);
		publicMap.putAll(propertyMap);

		// Step3：加载数据表
		List<Table> tableList = DatabaseXml.readDatabaseXml(xmlPath);// 得到表的列表

		// Step4：加载表级模板数据到Map
		Map<String, String> tableTempletMap = ClientTempletUtil.getTempletList(tablleTempletPath);// 表级模板MAP
		
		// Step5：加载列级模板数据到Map
		Map<String, String> columnTempletMap = ClientTempletUtil.getTempletList(columnTempletPath);// 列级模板MAP
		
		// Step6: 加载工程级模板路径数据到 List
		List<Templet> list = TempletUtil.getTempletList(projectTempletPath);

		for (Templet t : list) {
			System.out.println("处理模板：" + t.getPath() + " --- " + t.getFileName());

			// 如果是模板类文件，用模板生成多个表对应的文件
			if (FileUtil.isTemplatFile(t.getAllPath())) {

				// 6.1 读取模板文件内容
				String content = FileUtil.getContent(t.getAllPath());

				// 6.2 【关键】替换表级模板部分 title
				content = ClientTempletUtil.createContentForTable(content, tableTempletMap, tableList);

				// 6.3 如果文件名包含表替换符号，则使用每个表数据进行模板生成
				if (t.getFileName().indexOf("[table]") >= 0 || t.getFileName().indexOf("[Table]") >= 0
						|| t.getFileName().indexOf("[table2]") >= 0 || t.getFileName().indexOf("[Table2]") >= 0) {

					for (Table table : tableList) {
						
						// Step a：确定输出的文件名
						String outFile = t.getFileName().replace("[table]", table.getName());
						outFile = outFile.replace("[Table]", Utils.firstUpperCase(table.getName()));
						outFile = outFile.replace("[Table2]", Utils.firstUpperCase(table.getName2()));
						outFile = outFile.replace("[table2]", table.getName2());
						
						outFile = outFile.replace("[prefixSubpackage4]", table.getPrefixSubpackageHump());
						outFile = outFile.replace("[PrefixSubpackage4]", Utils.firstUpperCase(table.getPrefixSubpackageHump()));

						// Step b：【关键】替换列级模板部分
						String outContent = content;
						outContent = ClientTempletUtil.createContent(outContent, columnTempletMap, table);
						
						// Step c：全局替换
						System.out.println("全局替换符嵌套替换符处理********" + table.getName());
						outContent = ClientTempletUtil.createContent(outContent, publicMap);
						
						outContent = outContent.replace("[table]", table.getName());
						outContent = outContent.replace("[Table]", Utils.firstUpperCase(table.getName()));
						outContent = outContent.replace("[table2]", table.getName2());
						outContent = outContent.replace("[Table2]", Utils.firstUpperCase(table.getName2()));
						
						outContent = outContent.replace("[simple]", Utils.getSimpleSubpackage(table.getSubpackage()));
						outContent = outContent.replace("[Simple]", Utils.getSimpleSubpackage(table.getSubpackage()));
						
						outContent = outContent.replace("[subpackage]", table.getSubpackage());
						outContent = outContent.replace("[Subpackage]", Utils.firstUpperCase(table.getSubpackage()));
						
						outContent = outContent.replace("[subpackage2]", table.getSubpackagePath());
						outContent = outContent.replace("[Subpackage2]", Utils.firstUpperCase(table.getSubpackagePath()));
						
						outContent = outContent.replace("[subpackage3]", table.getPrefixSubpackageDot());
						outContent = outContent.replace("[Subpackage3]", Utils.firstUpperCase(table.getPrefixSubpackageDot()));
						
						outContent = outContent.replace("[subpackage4]", table.getPrefixSubpackageHump());
						outContent = outContent.replace("[Subpackage4]", Utils.firstUpperCase(table.getPrefixSubpackageHump()));
						
						outContent = outContent.replace("[prefixSubpackage]", table.getPrefixSubpackage());
						outContent = outContent.replace("[PrefixSubpackage]", Utils.firstUpperCase(table.getPrefixSubpackage()));
						
						outContent = outContent.replace("[prefixSubpackage2]", table.getPrefixSubpackagePath());
						outContent = outContent.replace("[PrefixSubpackage2]", Utils.firstUpperCase(table.getPrefixSubpackagePath()));
						
						outContent = outContent.replace("[prefixSubpackage3]", table.getPrefixSubpackageDot());
						outContent = outContent.replace("[PrefixSubpackage3]", Utils.firstUpperCase(table.getPrefixSubpackageDot()));
						
						outContent = outContent.replace("[prefixSubpackage4]", table.getPrefixSubpackageHump());
						outContent = outContent.replace("[PrefixSubpackage4]", Utils.firstUpperCase(table.getPrefixSubpackageHump()));

						// Step d：输出文件
						String subPath = table.getPrefixSubpackagePath();
						// 对于前端的html模板，需要具体到模块的叶子路径
						if(t.getFileName().contains(".html")) {
							subPath = table.getSubpackagePath();
						}
						
						String outPath = ClientTempletUtil.createContent(codePath + "/" + t.getPath() + subPath + "/" + outFile,
								publicMap);
						outPath = outPath.replace("(TFILE)", "");
						FileUtil.setContent(outPath, outContent);
					}
					
				} else{
				
					// 不用循环的文件 application.yml(TFILE)
					String outPath = ClientTempletUtil
							.createContent(codePath + "/" + t.getPath() + "/" + t.getFileName(), publicMap);
					// 在新的文件中去掉模板文件标记***********(TFILE)
					outPath = outPath.replace("(TFILE)", "");

					content = ClientTempletUtil.createContent(content, publicMap);
					FileUtil.setContent(outPath, content);// 写入文件
					
				}
				
			} else {
				
				// 非模板文件直接拷贝
				String newPath = ClientTempletUtil.createContent(codePath + "/" + t.getPath() + "/" + t.getFileName(),
						publicMap);

				FileUtil.copyFile(t.getAllPath(), newPath);
			
			}
		
		}
		System.out.println("代码成功生成!");
	}

}
