package com.ndood.code.template;

import org.apache.commons.lang.StringUtils;

/**
 * 工具集
 * 
 * @author Administrator
 *
 */
public class Utils {

	/**
	 * 首字母大写
	 */
	public static String firstUpperCase(String str) {
		if (StringUtils.isBlank(str)) {
			return str;
		}
		String s0 = str.substring(0, 1).toUpperCase();
		return s0 + str.substring(1);
	}

	/**
	 * 去掉表名前缀
	 */
	public static String getTableName2(String tableName) {

		tableName = tableName.toLowerCase();// 全部转换为小写
		int index = tableName.indexOf("_");// 下划线的位置
		if (index == -1) {
			return tableName;
		}
		String name = tableName.substring(index + 1);// 从下划线开始截取

		return getColumnName2(name);// 去掉下划线

	}

	/**
	 * 列名下划线处理
	 */
	@Deprecated
	public static String getColumnName2Backup(String name) {

		while (true) {

			int i = name.indexOf("_");// 取下划线
			System.out.println("i=" + i);
			if (i == -1) {
				break;// 跳出
			}
			String n = name.substring(i + 1, i + 2).toUpperCase();// 取出需要转换的字母进行转换

			name = name.substring(0, i) + n + name.substring(i + 2);

		}
		return name;
	}

	/**
	 * UnderlineToHump
	 */
	public static String getColumnName2(String para) {
		return underlineToHump(para);
	}

	public static String underlineToHump(String para) {
		StringBuilder result = new StringBuilder();
		String a[] = para.split("_");
		for (String s : a) {
			if (result.length() == 0) {
				result.append(s.toLowerCase());
			} else {
				result.append(s.substring(0, 1).toUpperCase());
				result.append(s.substring(1).toLowerCase());
			}
		}
		return result.toString();
	}

	public static String humpToUnderline(String para) {
		StringBuilder sb = new StringBuilder(para);
		int temp = 0;// 定位
		for (int i = 0; i < para.length(); i++) {
			if (Character.isUpperCase(para.charAt(i))) {
				sb.insert(i + temp, "_");
				temp += 1;
			}
		}
		return sb.toString().toUpperCase();
	}

	/**
	 * 包名缩写 operator.pay_config.app -> operator_pa
	 */
	public static String getSimpleSubpackage(String subpackage) {
		if (StringUtils.isBlank(subpackage)) {
			return "";
		}
		if (!subpackage.contains(".")) {
			return subpackage;
		}
		StringBuilder sb = new StringBuilder();
		String a[] = subpackage.trim().split("\\.");
		for (int i = 0; i < a.length; i++) {
			if (i == 0) {
				sb.append(a[i]).append("_");
			} else if (i == a.length - 1) {
				sb.append(a[i].substring(0, 1).toLowerCase());
			} else {
				sb.append(a[i].substring(0, 1).toLowerCase());
			}
		}
		return sb.toString();
	}

	/**
	 * operator.pay_config.app -> /operator/pay_config/app
	 */
	public static String getSubpackagePath(String subpackage) {
		if (StringUtils.isBlank(subpackage)) {
			return "";
		}
		if (!subpackage.contains(".")) {
			return "/" + subpackage;
		}
		StringBuilder sb = new StringBuilder();
		String a[] = subpackage.trim().split("\\.");
		for (int i = 0; i < a.length; i++) {
			sb.append("/").append(a[i]);
		}
		return sb.toString();
	}

	/**
	 * operator.pay_config.app -> .operator.pay_config.app
	 */
	public static String getSubpackageDot(String subpackage) {
		if (StringUtils.isBlank(subpackage)) {
			return "";
		}
		return "." + subpackage;
	}
	
	/**
	 * operator.pay_config.app -> operatorPayConfig
	 */
	public static String getSubpackageHump(String subpackage) {
		return toHump(subpackage);
	}

	/**
	 * operator.pay_config.app -> operator.pay_config
	 */
	public static String getPrefixSubpackage(String subpackage) {
		if (StringUtils.isBlank(subpackage)) {
			return "";
		}
		if (!subpackage.contains(".")) {
			return "";
		}
		String temp = subpackage.substring(0, subpackage.lastIndexOf('.'));
		return temp;
	}

	/**
	 * operator.pay_config.app -> /operator/pay_config
	 */
	public static String getPrefixSubpackagePath(String subpackage) {
		if (StringUtils.isBlank(subpackage)) {
			return "";
		}
		if (!subpackage.contains(".")) {
			return "/" + subpackage;
		}
		StringBuilder sb = new StringBuilder();
		String temp = subpackage.substring(0, subpackage.lastIndexOf('.'));
		String a[] = temp.trim().split("\\.");
		for (int i = 0; i < a.length; i++) {
			sb.append("/").append(a[i]);
		}
		return sb.toString();
	}

	/**
	 * operator.pay_config.app -> .operator.pay_config
	 */
	public static String getPrefixSubpackageDot(String subpackage) {
		if (StringUtils.isBlank(subpackage)) {
			return "";
		}
		if (!subpackage.contains(".")) {
			return "";
		}
		String temp = subpackage.substring(0, subpackage.lastIndexOf('.'));
		return "." + temp;
	}

	/**
	 * 驼峰命名法则
	 */
	public static String toHump(String str) {
	 	if (StringUtils.isBlank(str)) {
			return "";
		}
		if (!str.contains(".")&&!str.contains("_")) {
			return str;
		}
		String temp = str.replaceAll("\\.", "_");
		return underlineToHump(temp);
	}

	/**
	 * operator.pay_config.app -> operatorPayConfig
	 */
	public static String getPrefixSubpackageHump(String subpackage) {
		if (StringUtils.isBlank(subpackage)) {
			return "";
		}
		if (!subpackage.contains(".")) {
			return "";
		}
		String temp = subpackage.substring(0, subpackage.lastIndexOf('.'));
		return toHump(temp);
	}

}
