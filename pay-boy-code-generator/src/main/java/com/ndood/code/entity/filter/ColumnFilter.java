package com.ndood.code.entity.filter;

/**
 * 列属性过滤器，用来决定视图的渲染行为
 * 
 * @author ndood
 */
public class ColumnFilter {
	/**
	 * 列名称 user_id
	 */
	private String name;
	/**
	 * 视图组件类型
	 */
	private String componentType = "text";
	/**
	 * 是否是搜索字段
	 */
	private String search = "false";
	/**
	 * 是否隐藏数据列
	 */
	private String hideColumn = "false";

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getComponentType() {
		return componentType;
	}

	public void setComponentType(String componentType) {
		this.componentType = componentType;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public String getHideColumn() {
		return hideColumn;
	}

	public void setHideColumn(String hideColumn) {
		this.hideColumn = hideColumn;
	}

}
