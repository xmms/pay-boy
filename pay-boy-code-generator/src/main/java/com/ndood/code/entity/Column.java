package com.ndood.code.entity;

import org.apache.commons.lang.StringUtils;

/**
 * 列对象
 * 
 * @author Administrator
 *
 */
public class Column {

	/**
	 * 定义一些常用的组件类型
	 */
	public static final String COMPONENT_TYPE_TEXT = "text";
	public static final String COMPONENT_TYPE_NUMBER = "number";
	public static final String COMPONENT_TYPE_RADIO = "radio";
	public static final String COMPONENT_TYPE_SELECT = "select";
	public static final String COMPONENT_TYPE_DATETIME = "datetime";
	
	private String columnName;// 列名称
	private String columnName2;// 列名称(处理后的列名称)
	private String columnType;// 列类型
	
	/**
	 * 组件类型：text radio datetime
	 */
	private String componentType = Column.COMPONENT_TYPE_TEXT;
	/**
	 * 是否是搜索，如果为搜索状态，将会生成搜索框
	 */
	private Boolean search = false;
	/**
	 * 是否隐藏数据列，如果隐藏数据列，datatable的列将不会展示
	 */
	private Boolean hideColumn = false;

	public String getComponentType() {
		return componentType;
	}

	public void setComponentType(String componentType) {
		if(StringUtils.isBlank(componentType)) {
			return;
		}
		this.componentType = componentType;
	}

	public Boolean getSearch() {
		return search;
	}

	public void setSearch(Boolean search) {
		this.search = search;
	}

	public Boolean getHideColumn() {
		return hideColumn;
	}

	public void setHideColumn(Boolean hideColumn) {
		this.hideColumn = hideColumn;
	}

	public String getColumnName2() {
		return columnName2;
	}

	public void setColumnName2(String columnName2) {
		this.columnName2 = columnName2;
	}

	private String columnDbType;// 列数据库类型

	private String columnComment;// 列备注D
	private String columnKey;// 是否是主键

	private int decimal_digits;// DECIMAL_DIGITS;//小数位数
	private int colums_size;// COLUMN_SIZE 字段长度

	public int getColums_size() {
		return colums_size;
	}

	public void setColums_size(int colums_size) {
		this.colums_size = colums_size;
	}

	public int getDecimal_digits() {
		return decimal_digits;
	}

	public void setDecimal_digits(int decimal_digits) {
		this.decimal_digits = decimal_digits;
	}

	public String getColumnDbType() {
		return columnDbType;
	}

	public void setColumnDbType(String columnDbType) {
		this.columnDbType = columnDbType;
	}

	public String getColumnKey() {
		return columnKey;
	}

	public void setColumnKey(String columnKey) {
		this.columnKey = columnKey;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getColumnType() {
		return columnType;
	}

	public void setColumnType(String columnType) {
		this.columnType = columnType;
	}

	public String getColumnComment() {
		return columnComment;
	}

	public void setColumnComment(String columnComment) {
		this.columnComment = columnComment;
	}

}
