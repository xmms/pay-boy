package [package].dao[prefixSubpackage3];

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import [package].pojo[prefixSubpackage3].[Table2];
/**
 * [comment]数据访问接口
 * @author Administrator
 *
 */
public interface [Table2]Dao extends JpaRepository<[Table2],String>,JpaSpecificationExecutor<[Table2]>{
	
}
