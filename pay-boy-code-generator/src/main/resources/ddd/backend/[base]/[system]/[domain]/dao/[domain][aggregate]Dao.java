package ${domain.base}.${domain.system}.${domain.name}.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;

import ${domain.base}.${domain.system}.${domain.name}.entity.dto.${domain.largeCamelName}${aggregate.largeCamelName}Dto;
<#if (aggregate.statisticFields?size>0)>
import ${domain.base}.${domain.system}.${domain.name}.entity.dto.${domain.largeCamelName}${aggregate.largeCamelName}TotalDto;
</#if>
import ${domain.base}.${domain.system}.${domain.name}.entity.query.${domain.largeCamelName}${aggregate.largeCamelName}Query;

public interface ${domain.largeCamelName}${aggregate.largeCamelName}Dao {

	/**
	 * 1 分页查询
	 */
	@SelectProvider(type = ${aggregate.largeCamelName}Provider.class, method = "page${aggregate.largeCamelName}List")
	List<${domain.largeCamelName}${aggregate.largeCamelName}Dto> page${aggregate.largeCamelName}List(${domain.largeCamelName}${aggregate.largeCamelName}Query query);

	/**
	 * 2 查询统计
	 */
	@SelectProvider(type = ${aggregate.largeCamelName}Provider.class, method = "count${aggregate.largeCamelName}List")
<#if (aggregate.statisticFields?size>0)>	
	${domain.largeCamelName}${aggregate.largeCamelName}TotalDto count${aggregate.largeCamelName}List(${domain.largeCamelName}${aggregate.largeCamelName}Query query);
<#else>
	Long count${aggregate.largeCamelName}List(${domain.largeCamelName}${aggregate.largeCamelName}Query query);
</#if>

	class ${aggregate.largeCamelName}Provider {
		
		public String page${aggregate.largeCamelName}List(${domain.largeCamelName}${aggregate.largeCamelName}Query query) {
			StringBuilder sql = new StringBuilder("SELECT `id`, <#list aggregate.fields as field>`${field.name}`, <#if field_index % 8 == 0 && field_index !=0 >"
					+ </#if></#list>`gmt_created`, `gmt_updated` "
					+ "FROM ${aggregate.tableName} "
					+ "WHERE 1 = 1 ") ;
<#if (aggregate.searchFields?size>0)>
			addConditions(sql, query);
</#if>
			sql.append(" ORDER BY `gmt_created` DESC ");
			sql.append(" LIMIT <#noparse>#{</#noparse>offset<#noparse>}</#noparse>,<#noparse>#{</#noparse>pageSize<#noparse>}</#noparse> ");
			return sql.toString();
		}

		public String count${aggregate.largeCamelName}List(${domain.largeCamelName}${aggregate.largeCamelName}Query query) {
<#if (aggregate.statisticFields?size>0)>				
			StringBuilder sql = new StringBuilder("SELECT COUNT(0) as total<#list aggregate.statisticFields as field>, SUM(`${field.name}`) as total${field.largeCamelName}</#list> FROM ${aggregate.tableName} WHERE 1 = 1 ");
<#else>
			StringBuilder sql = new StringBuilder("SELECT COUNT(0) FROM ${aggregate.tableName} WHERE 1 = 1 ") ;
</#if>

<#if (aggregate.searchFields?size>0)>
			addConditions(sql, query);
</#if>
			return sql.toString();
		}
		
<#if (aggregate.searchFields?size>0)>
		private void addConditions(StringBuilder sql, ${domain.largeCamelName}${aggregate.largeCamelName}Query query) {
<#list aggregate.searchFields as field>

<#if field.name=='start_date' || field.name=='start_time'>
			if(query.get${field.largeCamelName}() != null)) {
				sql.append(" AND `${field.name}` >= <#noparse>#{</#noparse>${field.camelName}<#noparse>}</#noparse> ");
			}
<#elseif field.name=='end_date' || field.name=='end_time'>
			if(query.get${field.largeCamelName}() != null)) {
				sql.append(" AND `${field.name}` < <#noparse>#{</#noparse>${field.camelName}<#noparse>}</#noparse> ");
			}
<#elseif field.type!='string'>
			if(query.get${field.largeCamelName}() != null)) {
				sql.append(" AND `${field.name}` = <#noparse>#{</#noparse>${field.camelName}<#noparse>}</#noparse> ");
			}
<#elseif field.name?contains('name')>
			if(!StringUtils.isBlank(query.get${field.largeCamelName}())) {
				sql.append(" AND `${field.name}` like concat('%',<#noparse>#{</#noparse>${field.camelName}<#noparse>}</#noparse>,'%') ");
			}
<#else>
			if(!StringUtils.isBlank(query.get${field.largeCamelName}())) {
				sql.append(" AND `${field.name}` = <#noparse>#{</#noparse>${field.camelName}<#noparse>}</#noparse> ");
			}
</#if>
</#list>
		}
</#if>
	}

}