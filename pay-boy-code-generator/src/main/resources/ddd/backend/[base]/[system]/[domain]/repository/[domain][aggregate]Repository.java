package ${domain.base}.${domain.system}.${domain.name}.repository;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import ${domain.base}.base.dao.${domain.system}.${aggregate.largeCamelTableName}Dao;
import ${domain.base}.base.pojo.${domain.system}.${aggregate.largeCamelTableName}Po;

@Repository
public class ${domain.largeCamelName}${aggregate.largeCamelName}Repository {

	@Autowired
	private ${aggregate.largeCamelTableName}Dao ${aggregate.camelTableName}Dao;
	
	/**
	 * 1 添加一条<#if aggregate.desc??>${aggregate.desc}<#else>记录</#if>
	 */
	public void add${aggregate.largeCamelName}(${domain.largeCamelName}${aggregate.largeCamelName}Dto dto) {
		${aggregate.largeCamelTableName}Po po = new ${aggregate.largeCamelTableName}Po();
<#list aggregate.fields as field>
<#if field.name != 'id'>
		po.set${field.largeCamelName}(dto.get${field.largeCamelName}());
</#if>
</#list>
		po.setGmtUpdated(new Date());
		po.setGmtCreated(new Date());
		${aggregate.camelTableName}Dao.insert(po);
	}
	
	/**
	 * 2 根据ID更新<#if aggregate.desc??>${aggregate.desc}<#else>记录</#if>
	 */
	public void updateMonthDataById(${domain.largeCamelName}${aggregate.largeCamelName}Dto dto) {
		${aggregate.largeCamelTableName}Po po = new ${aggregate.largeCamelTableName}Po();
<#list aggregate.fields as field>
		po.set${field.largeCamelName}(dto.get${field.largeCamelName}());
</#list>
		po.setGmtUpdated(new Date());
		${aggregate.camelTableName}Dao.updateById(po);
	}
	
}