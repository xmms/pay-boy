package ${domain.base}.${domain.system}.${domain.name}.entity.query;

import ${domain.base}.${domain.system}.${domain.name}.entity.aggregate.${domain.largeCamelName}${aggregate.largeCamelName}Do;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ${domain.largeCamelName}${aggregate.largeCamelName}Query extends ${domain.largeCamelName}${aggregate.largeCamelName}Do {
	
<#list aggregate.searchFields as field>
<#if field.desc??>
	/**
     * ${field.desc}
     */
</#if>
    private ${field.largeType} ${field.camelName};
    
</#list>
	private Integer pageNo = 1;
	private Integer pageSize = 20;

	public Integer getOffset() {
		return (pageNo - 1) * pageSize;
	}
	
}