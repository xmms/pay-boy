<#if (aggregate.statisticFields?size>0)>
package ${domain.base}.${domain.system}.${domain.name}.entity.dto;

import java.math.BigDecimal;
import java.util.List;

import ${domain.base}.support.DataTableDto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ${domain.largeCamelName}${aggregate.largeCamelName}TotalDto {

	private Long total;
	
<#list aggregate.statisticFields as field>
<#if field.desc??>
	/**
	 * ${field.desc}
	 */
</#if>
	private ${field.largeType} total${field.largeCamelName};
</#list>

}
</#if>