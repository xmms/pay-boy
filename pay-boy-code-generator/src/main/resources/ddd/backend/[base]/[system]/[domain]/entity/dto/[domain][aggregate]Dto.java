package ${domain.base}.${domain.system}.${domain.name}.entity.dto;

import ${domain.base}.${domain.system}.${domain.name}.entity.aggregate.${domain.largeCamelName}${aggregate.largeCamelName}Do;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ${domain.largeCamelName}${aggregate.largeCamelName}Dto extends ${domain.largeCamelName}${aggregate.largeCamelName}Do {

}