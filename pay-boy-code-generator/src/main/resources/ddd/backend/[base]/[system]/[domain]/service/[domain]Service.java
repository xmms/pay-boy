package ${domain.base}.${domain.system}.${domain.name}.service;

import java.util.List;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ${domain.base}.${domain.system}.${domain.name}.entity.query.${domain.largeCamelName}${aggregate.largeCamelName}Query;
import ${domain.base}.${domain.system}.${domain.name}.entity.dto.${domain.largeCamelName}${aggregate.largeCamelName}Dto;
<#if (aggregate.statisticFields?size>0)>
import ${domain.base}.${domain.system}.${domain.name}.entity.dto.${domain.largeCamelName}${aggregate.largeCamelName}TotalDto;
import ${domain.base}.${domain.system}.${domain.name}.entity.dto.${domain.largeCamelName}${aggregate.largeCamelName}DataTableDto;
<#else>
import ${domain.base}.support.DataTableDto;
</#if>
import ${domain.base}.${domain.system}.${domain.name}.dao.${domain.largeCamelName}${aggregate.largeCamelName}Dao;

@Service
@Slf4j
public class ${domain.largeCamelName}Service {
	
	@Autowired
	private ${domain.largeCamelName}${aggregate.largeCamelName}Dao ${aggregate.camelName}Dao;

	@Autowired
	private ${domain.largeCamelName}${aggregate.largeCamelName}Repository ${aggregate.camelName}Repository;
	
	/**
	 * 1 查询出<#if aggregate.desc??>${aggregate.desc}<#else>数据</#if>列表
	 */
<#if (aggregate.statisticFields?size>0)>	
	public ${domain.largeCamelName}TotalDto query${aggregate.largeCamelName}List(${domain.largeCamelName}${aggregate.largeCamelName}Query query) {
		
		// Step1: 查询出<#if aggregate.desc??>${aggregate.desc}<#else>数据</#if>列表
		List<${domain.largeCamelName}${aggregate.largeCamelName}Dto> list = ${aggregate.camelName}Dao.page${aggregate.largeCamelName}List(query);
		
		// Step2: 查询出<#if aggregate.desc??>${aggregate.desc}<#else>数据</#if>统计
		${domain.largeCamelName}${aggregate.largeCamelName}TotalDto total = ${aggregate.camelName}Dao.count${aggregate.largeCamelName}List(query);
		
		// Step3: 返回结果
		return new ${domain.largeCamelName}DataTableDto(list,total);
	
	}
<#else>
	public DataTableDto query${aggregate.largeCamelName}List(${domain.largeCamelName}${aggregate.largeCamelName}Query query) {
		
		// Step1: 查询出<#if aggregate.desc??>${aggregate.desc}<#else>数据</#if>列表
		List<${domain.largeCamelName}${aggregate.largeCamelName}Dto> list = ${aggregate.camelName}Dao.page${aggregate.largeCamelName}List(query);
		
		// Step2: 查询出<#if aggregate.desc??>${aggregate.desc}<#else>数据</#if>统计
		Long total = ${aggregate.camelName}Dao.count${aggregate.largeCamelName}List(query);
		
		// Step3: 返回结果
		return new DataTableDto(list,total);
	
	}
</#if>
	
}