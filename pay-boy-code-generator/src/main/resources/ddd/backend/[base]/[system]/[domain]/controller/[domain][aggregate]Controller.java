package ${domain.base}.${domain.system}.${domain.name}.contoller;

package com.taiyi.web.admin.domain.d.trade;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ${domain.base}.support.AdminResultVo;
import ${domain.base}.support.AssertUtil;

import ${domain.base}.${domain.system}.${domain.name}.entity.query.${domain.largeCamelName}${aggregate.largeCamelName}Query;
import ${domain.base}.${domain.system}.${domain.name}.entity.dto.${domain.largeCamelName}${aggregate.largeCamelName}Dto;
<#if (aggregate.statisticFields?size>0)>
import ${domain.base}.${domain.system}.${domain.name}.entity.dto.${domain.largeCamelName}${aggregate.largeCamelName}TotalDto;
import ${domain.base}.${domain.system}.${domain.name}.entity.dto.${domain.largeCamelName}${aggregate.largeCamelName}DataTableDto;
<#else>
import ${domain.base}.support.DataTableDto;
</#if>
import ${domain.base}.${domain.system}.${domain.name}.service.${domain.largeCamelName}Service;

@RestController
@RequestMapping("/${domain.system}/${domain.name}/${aggregate.name}")
public class ${domain.largeCamelName}${aggregate.largeCamelName}Controller {
	
	@Autowired
	private ${domain.largeCamelName}Service ${domain.camelName}Service;
	
	/**
	 * 1 查询出<#if aggregate.desc??>${aggregate.desc}<#else>数据</#if>列表
	 */
	@RequestMapping("/query_list")
	public AdminResultVo query${aggregate.largeCamelName}List(@RequestBody ${domain.largeCamelName}${aggregate.largeCamelName}Query query) {
		
<#if (aggregate.searchFields?size>0)>
		// Step1: 校验请求参数
</#if>
<#list aggregate.searchFields as field>
		AssertUtil.notNull(query.get${field.largeCamelName}(), "<#if field.desc??>${field.desc}<#else>请求参数</#if>不存在！");
</#list>

<#if (aggregate.searchFields?size>0)>
		// Step2: 获取<#if aggregate.desc??>${aggregate.desc}<#else>数据</#if>列表
</#if>
<#if (aggregate.statisticFields?size>0)>
		${domain.largeCamelName}${aggregate.largeCamelName}DataTableDto page = ${domain.camelName}Service.query${aggregate.largeCamelName}List(query);
<#else>
		DataTableDto page = ${domain.camelName}Service.query${aggregate.largeCamelName}List(query);
</#if>
		return AdminResultVo.ok().setData(page).setMsg("操作成功");
	
	}
	
}
