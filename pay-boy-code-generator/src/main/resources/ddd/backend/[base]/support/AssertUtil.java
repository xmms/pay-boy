package ${domain.base}.support;

import java.util.Collection;

import org.apache.commons.lang.StringUtils;

public class AssertUtil {
	/**
	 * 判断对象不能为空
	 */
	public static void notNull(Object obj, String msg) {
		if (obj == null) {
			throw new IllegalArgumentException(msg);
		}
		if(obj instanceof String) {
			if(StringUtils.isBlank((String)obj)) {
				throw new IllegalArgumentException(msg);
			}
		}
	}

	/**
	 * 判断集合不能为空
	 */
	public static void notEmpty(Collection<?> collection, String msg) {
		if(collection == null) {
			throw new IllegalArgumentException(msg);
		}
		if(collection.isEmpty()) {
			throw new IllegalArgumentException(msg);
		}
	}
}
