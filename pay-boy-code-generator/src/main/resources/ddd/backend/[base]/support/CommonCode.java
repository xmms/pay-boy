package ${domain.base}.support;

public enum CommonCode {

	SUCCESS(10000, "请求成功"),
	
	ERR_SERVER(10500, "(*^__^*)系统开小差了,请稍后重试"),
	
	ERR_AUTHENTICATE(10400, "登录失败");
	
    private Integer code;
    private String value;
    
    private CommonCode(Integer code, String value) {
        this.code = code;
        this.value = value;
    }

    public Integer getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
    
	public static CommonCode getEnum(Integer code) {
		for (CommonCode rs : CommonCode.values()) {
			if (code==rs.getCode()) {
				return rs;
			}
		}
		return CommonCode.ERR_SERVER;
	}
    
}
