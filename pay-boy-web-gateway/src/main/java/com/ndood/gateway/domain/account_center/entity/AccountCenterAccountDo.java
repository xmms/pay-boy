package com.ndood.gateway.domain.account_center.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableId;

import lombok.Getter;
import lombok.Setter;

/**
 * 收银员账户领域对象
 */
@Getter
@Setter
public class AccountCenterAccountDo implements Serializable{
	private static final long serialVersionUID = 6672017425783738408L;

	@TableId
	private Integer id;

    /**
     * 是否收银员
     */
    private Integer isSales;
	
    private String email;

    /**
     * 邮箱状态：0 未激活 1 已激活
     */
    private Integer emailStatus;

    private String mobile;

    private String nickName;

    private String password;

    /**
     * 账户交易密码
     */
    private String tradePassword;

    /**
     * 最后一次登录时间
     */
    private String lastLoginTime;
    
    /**
     * 状态：0未启用 1启用
     */
    private Integer status;

}
