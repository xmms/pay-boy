package com.ndood.gateway.domain.account_center.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ndood.gateway.base.dao.account_center.ExtendAccountCenterAccountDao;
import com.ndood.gateway.domain.account_center.entity.dto.AccountCenterAccountDto;

/**
 * 账户中心领域服务
 */
@Service
public class AccountCenterService {

	@Autowired
	private ExtendAccountCenterAccountDao extendAccountCenterAccountDao;

	public AccountCenterAccountDto getAccountByEmail(String username) {
		return extendAccountCenterAccountDao.extendFindAccountByEmail(username);
	}

	public AccountCenterAccountDto getAccountByMobile(String username) {
		return extendAccountCenterAccountDao.extendFindAccountByMobile(username);
	}

	public AccountCenterAccountDto getAccountById(Integer id) {
		return extendAccountCenterAccountDao.extendFindAccountById(id);
	}
	
}
