package com.ndood.gateway.domain.account_center.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ndood.gateway.base.pojo.GatewayResultVo;
import com.ndood.gateway.domain.account_center.entity.dto.AccountCenterAccountDto;

@RestController
@RequestMapping("/account_center")
public class AccountCenterController {
	
	/**
	 * 1 获取账户信息
	 */
	@RequestMapping("/account_info")
	public GatewayResultVo getAccountInfo() {
		AccountCenterAccountDto dto = new AccountCenterAccountDto();
		dto.setId(1);
		dto.setNickName("asdfasfdasdf");
		return GatewayResultVo.ok().setData(dto).setMsg("获取登录用户信息成功");
	}
	
}
