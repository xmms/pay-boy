package com.ndood.gateway.domain.receive.v1.controller;

import java.io.PrintWriter;
import java.util.SortedMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.ndood.api.base.constaints.ApiCode;
import com.ndood.api.base.exception.ApiException;
import com.ndood.api.domain.record.receive.v1.entity.dto.RecoRecOfflineOrderDto;
import com.ndood.api.domain.router.receive.v1.service.RouteRecPlatformOfflineAlif2fService;
import com.ndood.api.domain.support.merchant.entity.dto.SupportMchRecChannelDto;
import com.ndood.api.domain.support.staff.entity.dto.SupportStaffDto;
import com.ndood.api.domain.support.staff.service.SupportStaffService;
import com.ndood.common.base.constaints.CommonConstaints;
import com.ndood.gateway.base.pojo.GatewayResultVo;
import com.ndood.gateway.base.utils.HttpUtil;
import com.ndood.gateway.domain.receive.v1.service.RecPlatformOfflineOfficialAlif2fService;

import lombok.extern.slf4j.Slf4j;

/**
 * 收款功能v1-平台收款-支付宝当面付
 */
@RestController
@RequestMapping("/receive/v1/platform/offline/alif2f")
@Slf4j
public class RecPlatformOfflineAlif2fController {

	@Autowired
	private RouteRecPlatformOfflineAlif2fService routeRecPlatformAlif2fService;
	
	@Autowired
	private RecPlatformOfflineOfficialAlif2fService recPlatformOfficialAlif2fService;
	
	@Autowired
	private SupportStaffService supportStaffService;
	
	/**
	 * 1 扫码支付创建预支付订单
	 */
	@RequestMapping("/pre_create")
	public GatewayResultVo preCreate(String amount, String payRemark, String username, String deviceNo, HttpServletRequest request) throws ApiException {

		// Step1: 参数校验
		Assert.notNull(username, "收银员账号不能为空！");
		Assert.notNull(amount, "金额不能为空！");
		Assert.notNull(deviceNo, "设备编码不能为空");
		
		String ip = HttpUtil.getIpAddress(request);

		// Step1: 获取支付信息，包含商户信息，支付信息，门店信息
		SupportStaffDto staff = supportStaffService.getStaffFullPayInfo(username);
		Assert.notNull(staff, "员工信息不存在!");
		
		// Step2: 渠道路由
		SupportMchRecChannelDto channel = routeRecPlatformAlif2fService.route(CommonConstaints.P_ALI_SCAN, amount, staff);

		// Step3: 路由跳转
		if(CommonConstaints.CN_GF_ALI_SCAN.equals(channel.getChannelNo())){
			RecoRecOfflineOrderDto order = recPlatformOfficialAlif2fService.preCreate(amount, payRemark, deviceNo, ip, staff, channel);
			return GatewayResultVo.ok().setData(order);
		}

		throw new ApiException(ApiCode.ERR_SERVER, "不支持的支付渠道->" + channel.getChannelNo());

	}

	/**
	 * 2 扫码支付接收异步通知并完成订单
	 */
	@RequestMapping("/notify/{channelNo}/{contractType}/{merchantId}")
	public void notifyOrder(HttpServletRequest req, HttpServletResponse resp, @PathVariable String channelNo, @PathVariable Integer contractType, 
			@PathVariable Integer merchantId) {
		try {
			
			// Step1: 参数校验
			Assert.notNull(channelNo, "渠道编码不能为空");
			Assert.notNull(contractType, "合同类型不能为空");
			Assert.notNull(merchantId, "商户ID不能为空");
			SortedMap<String, Object> params = HttpUtil.getParameterSortedMap(req);
			log.info(new Gson().toJson(params));
			
			// Step2: 支付路由
			if(CommonConstaints.CN_GF_ALI_SCAN.equals(channelNo)) {
				recPlatformOfficialAlif2fService.preCreateNotify(channelNo, contractType, merchantId, params);
				// Step3: 返回结果
				PrintWriter out = resp.getWriter();
				out.write("success");
				out.flush();
			}

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

	}

}
