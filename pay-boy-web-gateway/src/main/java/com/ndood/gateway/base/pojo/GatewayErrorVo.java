package com.ndood.gateway.base.pojo;

import java.io.Serializable;

public class GatewayErrorVo  implements Serializable{
	private static final long serialVersionUID = -7285807415842181061L;

	private Integer code;
	private String msg;

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public GatewayErrorVo(Integer code, String msg) {
		super();
		this.code = code;
		this.msg = msg;
	}

	public GatewayErrorVo() {
		super();
	}
	
}