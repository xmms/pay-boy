package com.ndood.gateway.app.security.handler;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ndood.api.base.constaints.ApiCode;
import com.ndood.gateway.base.pojo.GatewayResultVo;

import lombok.extern.slf4j.Slf4j;

@Component("unAuthorizedAccessHandler")
@Slf4j
public class PaymentUnAuthorizedAccessHandler  implements AccessDeniedHandler{

	@Autowired
	private ObjectMapper objectMapper;
	
	@Override
	public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException exception)
			throws IOException, ServletException {

		// 根据配置进行处理
		// response.setStatus(HttpStatus.FORBIDDEN.value());
		response.setStatus(HttpStatus.OK.value());
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Credentials", "true");
		response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
		response.setHeader("Access-Control-Max-Age", "3600");
		response.setHeader("Access-Control-Allow-Headers",
				"x-requested-with, authorization, Content-Type, Authorization, credential, X-XSRF-TOKEN");
		response.setContentType("application/json;charset=UTF-8");
		
		String json = objectMapper.writeValueAsString(
				GatewayResultVo.error().setCode(ApiCode.ERR_TOKEN.getCode()).setMsg(exception.getMessage()));

		StringBuilder sb = new StringBuilder("\n");
		sb.append("DDDDDDDDDDDDDDD没有权限访问该资源DDDDDDDDDDDDDDD").append("\n");
		sb.append(JSON.toJSON(getHeadersInfo(request))).append("\n");
		sb.append(JSON.toJSON(request.getParameterMap())).append("\n");
		sb.append("返回结果:").append(json).append("\n");
		sb.append("DDDDDDDDDDDDDDD没有权限访问该资源DDDDDDDDDDDDDDD").append("\n\n");
		log.error(sb.toString(), exception);
		
		response.getWriter().write(json);
		response.getWriter().flush();
	}
	
	/**
	 * 获取请求头信息
	 */
	private Map<String, String> getHeadersInfo(HttpServletRequest request) {
        Map<String, String> map = new HashMap<String, String>();
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String key = (String) headerNames.nextElement();
            String value = request.getHeader(key);
            map.put(key, value);
        }
        return map;
    }
	
}
