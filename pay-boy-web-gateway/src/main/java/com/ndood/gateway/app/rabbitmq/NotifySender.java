package com.ndood.gateway.app.rabbitmq;

import java.util.Date;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class NotifySender {

	@Autowired
	private AmqpTemplate rabbitTemplate;

	public void send() {
		String context = "notify " + new Date();
		log.info("Sender : " + context);
		this.rabbitTemplate.convertAndSend("notify", context);
	}
}
