package com.ndood.gateway.app.rabbitmq;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class PollingSender {

	@Autowired
	private AmqpTemplate rabbitTemplate;

	public void send(String msg) {
		log.info("Sender : " + msg);
		this.rabbitTemplate.convertAndSend("polling", msg);
	}
}
