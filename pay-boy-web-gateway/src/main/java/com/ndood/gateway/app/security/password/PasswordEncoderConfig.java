package com.ndood.gateway.app.security.password;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * 密码配置类
 */
@Configuration
public class PasswordEncoderConfig {
	
	@Bean
	public PasswordEncoder customPasswordEncoder() {

		return new PasswordEncoder() {
			@Override
			public String encode(CharSequence rawPassword) {
				return BCrypt.hashpw(rawPassword.toString(), BCrypt.gensalt(4));
			}

			@Override
			public boolean matches(CharSequence encodedPassword, String rawPassword) {
				return BCrypt.checkpw(encodedPassword.toString(), rawPassword);
			}
		};
		
	}
	
	public static void main(String[] args) {
		String secret = BCrypt.hashpw("123456", BCrypt.gensalt(4));
		System.out.println(secret);
		boolean test = BCrypt.checkpw("123456", "$2a$04$ray2ErDrDrPGpzXyHuqw5uHvCnn8Nv/IPr8nQL2os718PQ0zC.gt2");
		System.out.println(test);
	}
}
