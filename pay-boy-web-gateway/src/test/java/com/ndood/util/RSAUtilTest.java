package com.ndood.util;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.ndood.gateway.base.utils.AesUtil;
import com.ndood.gateway.base.utils.RSAUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RSAUtilTest {

	public static final String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDN3F0n9qwMQsOzbJxlJwMApXNM"
			+ "R+N+LMpBxcDyKeiBalVAFzrU4q1HsTJIisJOBbL0WHlp2X/eeBFHJstVQCk+qddj"
			+ "R1gKXiJfpU9Q/WnsvQPE4MBK64GG8T1S3Ri57EkR9TE5DwE0zzl+z95Emi26j1Nm" + "Pc3I9kYqfr/dCoj9mwIDAQAB";

	public static final String privateKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAM3cXSf2rAxCw7Ns"
			+ "nGUnAwClc0xH434sykHFwPIp6IFqVUAXOtTirUexMkiKwk4FsvRYeWnZf954EUcm"
			+ "y1VAKT6p12NHWApeIl+lT1D9aey9A8TgwErrgYbxPVLdGLnsSRH1MTkPATTPOX7P"
			+ "3kSaLbqPU2Y9zcj2Rip+v90KiP2bAgMBAAECgYBO/kxQ1XrXiZcG9ppcxkeEq/g+"
			+ "QSeudwl1i6iqRCKP5nmoCkHtBr5vUDN3WqeMwOsWkAym7Wr/txsKLny/zcsFLFBT"
			+ "8CNIKfU53SjVxES4yX5fROsHd5zJFTMWprcGgjOKMmih8pPT3nB8wwcSWuvQYuTY"
			+ "3IIr830hCq9tgHvSoQJBAPBwBvR3MtjTRV47kuSkZGd+NlsAPHxUXEIZGK/MyTVn"
			+ "lVzNoDbVpStTAFqPm5WDlBf4GdDHvCcGqJ8fI0KRNDUCQQDbL2kF99hSjx1ZFpAn"
			+ "kWbMwV12nr6OuyWIicglQgh7VDAa8iaDf1X1icj6lQIsI9NU0H16eIGdhO1+3/XX"
			+ "rASPAkARGocEIO6XCgBnQamjZiZWTl4jfxLObVnawdpFtzWg/OtdHKuG+w+y00a1"
			+ "Kn4Q1rlUMyvy9CJoTEr2dsqVU6r5AkA25VLqeb6mPs3c6DfGkTYsBioAcZXMbbbi"
			+ "0Y9dNYKmUNmThh57RMMkshOyHuviXj6puWYT7GaaKxbIdhM+pwilAkEA3ODIk6wn"
			+ "Flz5IcO7auXJCtiujXceU3t57fUPTRyDwJ7RiNFNrHnNZqih1ZUShDVbwHFbOKO2" + "56/Xt2X3oPX+dQ==";

	@Test
	public void signTest() {
		String text = "ndood";
		String sign = RSAUtil.sign(text, privateKey);
		System.out.println(sign);

		System.out.println(RSAUtil.verify(text, sign, publicKey));
	}

	@Test
	public void rsaRequestTest() {
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("openid", "222222");
			map.put("timestamp", "123123123");
			String sign = RSAUtil.sign(map, privateKey);
			System.out.println(sign);
			map.put("sign", sign);
			String json = null;
			json = new ObjectMapper().writeValueAsString(map);
			System.out.println(json);

			String aesKey = AesUtil.randomAesKey();
			String encryptData = null;
			encryptData = AesUtil.encrypt(json, aesKey);
			String encryptKey = null;
			encryptKey = RSAUtil.encrypt(publicKey, aesKey);

			Map<String, String> requestMap = Maps.newHashMap();
			requestMap.put("encryptAesKey", encryptKey);
			requestMap.put("encryptData", encryptData);
			System.out.println(new Gson().toJson(requestMap));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
