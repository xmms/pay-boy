package com.ndood.reconcilation;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayDataDataserviceBillDownloadurlQueryRequest;
import com.alipay.api.response.AlipayDataDataserviceBillDownloadurlQueryResponse;

public class AlipayReconciliationTest {
	// 初始化实例请求对象
	public static String APP_ID = "2016080600181116";
	public static String APP_PRIVATE_KEY = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC39vusjXyGlIa5PyT2Th4WLcP0aXoUwUwSRabH7/gLeq3/b5ZEI9rIQ4GKBpHb971GoxqgOMqv2pnvI2HXr6mxmtrh1TQLLsywbhrnNYFWKwEJy/U96zJz8odgldxx6BGWh3hAGhPOsyCgNokXGzwRLxXhr8e45i1rZxzKJAtJ8HBj2Aurgf553MCzc7SX8Ch0xHzyMMBXfJBjONxOEBQBPyMtaKHm4ULBvekCt6Vjl9MnHphBusanOLetLUepmczM0NO/feDK0f4B8g3aj+mDg4kO0MwKkR/HUPVArsYBhD0vKCQhVukCn4VYcNKNJdxzxDR1T6Z1iNBSZ/r941nLAgMBAAECggEBAJtSX7mXl+z02C7t3mJLIGHvcAQbTiTZ80V2I1OqC0593QzPBRsw/paZUHTeCSakq91I9sESUidZtSBQSlrWCLtYmLeV0CQk9lN8EyDlbvb2xiCkYMJPo0B8I8rrnL1biAYFCQVuPw0E9mXuxZ6JsxR7Icl0JXe6d9y3qXoOJq3ccgtac6Hf7Vby0eqz0ie3gr+zYLFVc//2K6DUFSZ8CjlYd18Iev+1sWhnuFDEWLsyDS0rRykJ/ibrCctnGSCBJa9nP3AZYW2NOzYkXhBJAn8I+w3wo+dbCo1eC9R/0jDoJdkvb089Lw+vBt90LHin0xoFvAHzk69GzFk3PhSeqIkCgYEA4KSZdwo/wgM79Ll+vtC0PwH8qM4M7BMEnOfivCMfzirBWgU+3xKlVgP7DZ+ZecgXLrGdg7jIQMYIbNHTE8fprTwAwznOM5srDZ1ourDXZYL4HCRrpv4t7zgJdustHsIrjgTsVK/oj6GhQvTOpJ7lEh7H7pLvL7+diNhBultNuycCgYEA0aTJosDSr1FS2Xzy+wbh/2jXmnRKEz8daDfvDRvA3uVMNYpWv9NWhH25hsxKL1RL6X7AJJn3uOklOPILxp0sIJ4zxr3D6kejO/mEc8D+hB9JJmJsNJ3fqsD5h+YvNbyYKVKLFpwlQywQs+dSrWplP6JCMZND103Xy0JfsuNTIr0CgYBqaC01QMqQmL2WnyLVOcCXM9a9EtT2IdAGjqRMn4qaOpBZlrCAM7HTO5F4k4e8MrYgbQuCcE2SuPsWqJEGBqS+ZPSrhL8SnMZY7Z3FCK0OrWne0rWXZUDW1kFNbfpyXmEJJXXYa3aKSnI7aABV49n3qlLjw/++K1JwlpcTsdnw8wKBgA2mSAaTvWPyzI0iiIN1mENT5S3GQohsRJLjiWn2E6Gvlj2qKynELc7kjK7NIEtilqq2OPZpG+rrKyRBBilBVH0vL7nR4y6eyW7/OPN8blXV36JjKB5o5QYuGFET2KEMDXArUqj/M6Hi/ZdpCUYHNl5lRyAJMZKUG+e9A5QXHvOVAoGASCIGzsjxraAxNN6nOZltlsuadHe3iQVhV7Z5ZKnmB0qRCmALj9IBweSBai80FFUDJPgvzQYcK8I4PwmldEPxHCgMnoAeVFAtGbz6zenKEWfdoSZnePggFKImqYAn3a4eUv+QZzmL7sPAf98bzLx1wVyXt1YEeCL3S1WMYe5W3VA=";
	public static String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsYuB/Y5J2TIcTwgvgzxJXz+bWQO/OAzhvlQ4nT9ynG1z/0K5JGQqE4fBBB26BuRUcnMKuA9YLGUdiBDizXQfS2j1CPXfR5F9t2a/LgIg+URrYt7sr3waQcGiK26MqhUP8qWWFtOG6L3Xd7DyoUAUQPhj3ikKqcFqf+EOKfZdY9Lj0Ohf2fCmhOBSv4SM10nsjHcoEkYdhQ0jklTi4wNDUVHGWn9Yi0/5EGi4br3YREi+74gu4ZuuKKYeiDG4BgR0vU4ShoFBdA+IZzP5QkFGVKp6am45a+5ep4tjSc98OSzMx5EBTv762UiN06R0ILwn0MYyC8GGEPYVcned2z1pCQIDAQAB";
	public static AlipayClient alipayClient = new DefaultAlipayClient("https://openapi.alipaydev.com/gateway.do",APP_ID, APP_PRIVATE_KEY, "json", "UTF-8", ALIPAY_PUBLIC_KEY, "RSA2");

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		bill_download_url("2019-03-20");// 下载对账单接口
	}

	public static void bill_download_url(String date) {

		AlipayDataDataserviceBillDownloadurlQueryRequest request = new AlipayDataDataserviceBillDownloadurlQueryRequest();
		request.setBizContent("{\"bill_type\":\"trade\",\"bill_date\":\""+date+"\"}");
		AlipayDataDataserviceBillDownloadurlQueryResponse response = null;
		try {
			response = alipayClient.execute(request);
			System.out.println(response.getBillDownloadUrl());

		} catch (AlipayApiException e) {
			e.printStackTrace();
		}
		if (response.isSuccess()) {
			// 将接口返回的对账单下载地址传入urlStr
			String urlStr = response.getBillDownloadUrl();
			// 指定希望保存的文件路径
			String filePath = "D:\\fund_bill_20190327.csv.zip";
			URL url = null;
			HttpURLConnection httpUrlConnection = null;
			InputStream fis = null;
			FileOutputStream fos = null;
			try {
				url = new URL(urlStr);
				httpUrlConnection = (HttpURLConnection) url.openConnection();
				httpUrlConnection.setConnectTimeout(5 * 1000);
				httpUrlConnection.setDoInput(true);
				httpUrlConnection.setDoOutput(true);
				httpUrlConnection.setUseCaches(false);
				httpUrlConnection.setRequestMethod("GET");
				httpUrlConnection.setRequestProperty("CHARSET", "UTF-8");
				httpUrlConnection.connect();
				fis = httpUrlConnection.getInputStream();
				byte[] temp = new byte[1024];
				int b;
				fos = new FileOutputStream(new File(filePath));
				while ((b = fis.read(temp)) != -1) {
					fos.write(temp, 0, b);
					fos.flush();
				}
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (fis != null)
						fis.close();
					if (fos != null)
						fos.close();
					if (httpUrlConnection != null)
						httpUrlConnection.disconnect();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}