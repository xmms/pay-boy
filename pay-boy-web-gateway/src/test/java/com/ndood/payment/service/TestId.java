package com.ndood.payment.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ndood.api.app.id.SnowflakeIdWorker;
import com.ndood.common.base.util.DateUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestId {
	
	@Autowired
	private SnowflakeIdWorker idWorker;
	
	@Test
	public void testId() {
		for(int i=0;i<100;i++) {
			long nextId = idWorker.nextId();
			System.out.println(DateUtil.getNatureDateTimeSequence()+nextId);
		}
	}
		
}
